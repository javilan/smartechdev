package com.berdico.webservices.endpoint;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.berdico.webservices.cloudservice.CustomerRequest;
import com.berdico.webservices.cloudservice.CustomerResponse;
import com.berdico.webservices.cloudservice.ItemMasterRequest;
import com.berdico.webservices.cloudservice.ItemMasterResponse;
import com.berdico.webservices.cloudservice.RfcDetailsResponse;
import com.berdico.webservices.cloudservice.RfcRequest;
import com.berdico.webservices.customer.Customer;
import com.berdico.webservices.items.ItemMaster;
import com.berdico.webservices.rfc.Rfc;
import com.berdico.webservices.services.CloudSOAPService;

@Endpoint
public class CloudSOAPEndpoint {
	
	private static final String TARGET_NAMESPACE = "http://com/berdico/webservices/cloudservice";
	
	@Autowired
	CloudSOAPService cloudSOAPService;

	public void setCloudSOAPService(CloudSOAPService cloudSOAPService) {
		this.cloudSOAPService = cloudSOAPService;
	}
	
	@PayloadRoot(localPart = "ItemMasterRequest", namespace = TARGET_NAMESPACE)
	public @ResponsePayload ItemMasterResponse getItemDetails(@RequestPayload ItemMasterRequest request)
	{
		ItemMasterResponse response = new ItemMasterResponse();
		List<ItemMaster> list = cloudSOAPService.getItems(request.getItemRequest());
		response.setItemResponse(list);
		return response;
	}

	@PayloadRoot(localPart = "CustomerRequest", namespace = TARGET_NAMESPACE)
	public @ResponsePayload CustomerResponse insertCustomer(@RequestPayload CustomerRequest request)
	{
		CustomerResponse response = new CustomerResponse();
		Customer c = cloudSOAPService.createCustomer(request.getCustomerRequest());
		response.setCustomerResponse(c);
		return response;
	}
	
	@PayloadRoot(localPart = "RfcRequest", namespace = TARGET_NAMESPACE)
	public @ResponsePayload RfcDetailsResponse getRfcDetails(@RequestPayload RfcRequest request)
	{
		RfcDetailsResponse response = new RfcDetailsResponse();
		Rfc r = cloudSOAPService.getRfc(request.getRfc());
		response.setRfcDetails(r);
		return response;
	}
}
