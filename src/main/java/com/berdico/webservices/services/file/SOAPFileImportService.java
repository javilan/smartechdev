package com.berdico.webservices.services.file;

import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tdo.integration.services.HTTPRequestService;
import com.tdo.integration.util.AppConstants;
import com.tdo.integration.util.AppStringUtils;
import com.tdo.integration.ws.PayloadProducer;

@Service("SOAPFileImportService")
public class SOAPFileImportService {
	
	@Autowired  
	HTTPRequestService HTTPRequestService; 
	
	public String uploadARBulkFile(String content, String fileName) {
		
		try {
			
			String response = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_BULKIMPORT_URL + "?invoke=", 
                                             PayloadProducer.getARBulkFileImportSOAPXmlContent(content, fileName),
                                             AppConstants.CLOUD_CREDENTIALS);
			
			if(response != null) {
				return response;
			}else {
				return null;
			}
			
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}

	}
	
	
	public String uploadInvBulkFile(String content, String fileName) {
		
		try {
			
			String response = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_BULKIMPORT_URL + "?invoke=", 
                                             PayloadProducer.getInvBulkFileImportSOAPXmlContent(content, fileName),
                                             AppConstants.CLOUD_CREDENTIALS);
			
			if(response != null) {
				return response;
			}else {
				return null;
			}
			
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}

	}
	
	
	public boolean uploadGLBulkFile(String content, String fileName) {
		
		try {
			
			String response = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_BULKIMPORT_URL + "?invoke=", 
                                             PayloadProducer.getGLBulkFileImportSOAPXmlContent(content, fileName),
                                             AppConstants.CLOUD_CREDENTIALS);
			
			if(response != null) {
				return true;
			}else {
				return false;
			}
			
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}

	}
	
	public String loadARLockboxFile(String content, String fileName, String docname, String date) {
		
		try {
			
			String response = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_BULKIMPORT_URL + "?invoke=", 
                                             PayloadProducer.getARLockboxSOAPXMLContentStep1(content, fileName, docname),
                                             AppConstants.CLOUD_CREDENTIALS);
			
			
			if(response != null) {
				response = AppStringUtils.getEnvResponse(response);
				JSONObject xmlJSONObj = XML.toJSONObject(response, true);
				JsonElement jelement = new JsonParser().parse(xmlJSONObj.toString());
				JsonObject jobject = jelement.getAsJsonObject();
				JsonElement value = jobject.get("env:Envelope").getAsJsonObject().get("env:Body").getAsJsonObject().get("ns0:uploadFileToUcmResponse").getAsJsonObject();
				
				String step1Id = value.getAsJsonObject().get("result").toString();
				step1Id = AppStringUtils.getContentFromEnv(step1Id);
				
				if("".equals(step1Id)) {
					return null;
				}
				
				String nil = "{\"xsi:nil\":\"true\"}";
				 if(step1Id.contains(nil)){
					 return null;
				 }else {
					 step1Id = step1Id.replaceAll("\"", "");				 
				 }


				String response2 = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_BULKIMPORT_URL + "?invoke=", 
	                    PayloadProducer.getARLockboxSOAPXMLContentStep2(step1Id),
	                    AppConstants.CLOUD_CREDENTIALS);
				if(response2 != null) {
					
					response2 = AppStringUtils.getEnvResponse(response2);
					JSONObject xmlJSONObj2 = XML.toJSONObject(response2, true);
					JsonElement jelement2 = new JsonParser().parse(xmlJSONObj2.toString());
					JsonObject jobject2 = jelement2.getAsJsonObject();
					JsonElement value2 = jobject2.get("env:Envelope").getAsJsonObject().get("env:Body").getAsJsonObject().get("ns0:submitESSJobRequestResponse").getAsJsonObject();
					
					String step2Id = value2.getAsJsonObject().get("result").toString();
					step2Id = AppStringUtils.getContentFromEnv(step2Id);
					
					if("".equals(step2Id)) {
						return null;
					}
					
					String ni2 = "{\"xsi:nil\":\"true\"}";
					 if(step2Id.contains(ni2)){
						 return null;
					 }else {
						 step2Id = step2Id.replaceAll("\"", "");				 
					 }
					
				
					String response3 = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_BULKIMPORT_URL + "?invoke=", 
		                    PayloadProducer.getARLockboxSOAPXMLContentStep3(step2Id, docname, date),
		                    AppConstants.CLOUD_CREDENTIALS);
					
					if(response3 != null) {
						response3 = AppStringUtils.getEnvResponse(response3);
						JSONObject xmlJSONObj3 = XML.toJSONObject(response3, true);
						JsonElement jelement3 = new JsonParser().parse(xmlJSONObj3.toString());
						JsonObject jobject3 = jelement3.getAsJsonObject();
						JsonElement value3 = jobject3.get("env:Envelope").getAsJsonObject().get("env:Body").getAsJsonObject().get("ns0:submitESSJobRequestResponse").getAsJsonObject();
						
						String step3Id = value3.getAsJsonObject().get("result").toString();
						step3Id = AppStringUtils.getContentFromEnv(step3Id);
						
						if("".equals(step3Id)) {
							return null;
						}
						
						String ni3 = "{\"xsi:nil\":\"true\"}";
						 if(step3Id.contains(ni3)){
							 return null;
						 }else {
							 step3Id = step3Id.replaceAll("\"", "");				 
						 }
						
						return response;
					}
				}
			}
						
			
						
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
		return null;

	}
	
}

