package com.berdico.webservices.services;

import java.util.List;

import com.berdico.webservices.customer.Customer;
import com.berdico.webservices.customer.CustomerDetailsRequest;
import com.berdico.webservices.items.ItemDetailsRequest;
import com.berdico.webservices.items.ItemMaster;
import com.berdico.webservices.rfc.Rfc;

public interface CloudSOAPService {
	
	public Rfc getRfc(String rfc);
	public List<ItemMaster> getItems(ItemDetailsRequest request);
	public Customer createCustomer(CustomerDetailsRequest request);

}
