package com.berdico.webservices.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.berdico.webservices.customer.Customer;
import com.berdico.webservices.customer.CustomerDetailsRequest;
import com.berdico.webservices.items.Generos;
import com.berdico.webservices.items.ItemDetailsRequest;
import com.berdico.webservices.items.ItemMaster;
import com.berdico.webservices.items.TipoMica;
import com.berdico.webservices.items.TiposMicas;
import com.berdico.webservices.rfc.Rfc;
import com.tdo.integration.dto.CustomerDTO;
import com.tdo.integration.dto.ItemMasterKauffmanDTO;
import com.tdo.integration.ws.SOAPCustomerService;
import com.tdo.integration.ws.SOAPItemMasterService;

@Service
public class CloudSOAPServiceImpl implements CloudSOAPService {

	@Autowired  
	SOAPCustomerService sOAPCustomerService;
	
	@Autowired 
	SOAPItemMasterService sOAPItemMasterService;
	
	@Override
	public Rfc getRfc(String rfc) {
		
		Rfc r = new Rfc();
		r.setExiste("false");
		if(!"".equals(rfc)) {
			if(sOAPCustomerService.getRfcString(rfc) != null) {
				r.setExiste("true");
			}
		}

		return r;
	}

	@Override
	public List<ItemMaster> getItems(ItemDetailsRequest request) {
		
		List<ItemMasterKauffmanDTO> list = sOAPItemMasterService.getItems(request.getDateStart(), request.getDateEnd(), request.getStart(), 
				                                                          request.getSize(), request.getType(), request.getEntidad(), request.getItemType());
		
		List<ItemMaster> resList = new ArrayList<ItemMaster>();
		if(list != null) {
			if(list.size() > 0) {
				for(ItemMasterKauffmanDTO o : list) {
					ItemMaster itm = new ItemMaster();
					itm.setItemIDoracle(o.getSku());
					itm.setDescripcionespañol(o.getOms300());
					itm.setDescripcioningles(o.getOms301());
					itm.setModelo(o.getU_modelodela()); //*****
					itm.setMaterial(o.getU_material());
					itm.setColor(o.getU_colorprove());
					itm.setTipoarmazon(""); //******
					itm.setMedida(o.getU_medidaaro());
					itm.setTamaño(o.getU_forma());
					
					List<Generos> generos = itm.getGeneros();
					Generos genero = new Generos();
					genero.setNombregenero(o.getU_paragenero());
					generos.add(genero);
					
					List<TiposMicas> tiposMica = itm.getTiposmicas();
					TiposMicas tipoMica = new TiposMicas();
					TipoMica tm = new TipoMica();
					tm.setMica(o.getU_clasedemic());
					tipoMica.setTipomica(tm);
					tiposMica.add(tipoMica);
					
					resList.add(itm);
				}
			}
		}

		return resList;
	}

	@Override
	public Customer createCustomer(CustomerDetailsRequest request) {
		
		Customer response = new Customer();
		List<CustomerDTO> customerList = new ArrayList<CustomerDTO>();
		CustomerDTO c = new CustomerDTO();
		
		c.setAccount(request.getAccount());
		c.setAddress1(request.getAddress1());
		c.setCity(request.getCity());
		c.setCountry(request.getCountry());
		c.setName(request.getName());
		c.setPostalCode(request.getPostalCode());
		c.setProfileClassName(request.getProfileClassName());
		c.setRfc(request.getRfc());
		c.setSite(request.getSite());
		c.setState(request.getState());
		customerList.add(c);
		
		List<CustomerDTO> res = sOAPCustomerService.insertCustomers(customerList);
		
		if(res != null) {
			if(res.size() > 0) {
				CustomerDTO o = res.get(0);
				response.setAccount(o.getAccount());
				response.setAccountNumber(o.getAccountNumber());
				response.setAddress1(o.getAddress1());
				response.setCity(o.getCity());
				response.setCountry(o.getCountry());
				response.setMsg(o.getMsg());
				response.setName(o.getName());
				response.setPartyNumber(o.getPartyNumber());
				response.setPostalCode(o.getPostalCode());
				response.setProfileClassName(o.getProfileClassName());
				response.setRfc(o.getRfc());
				response.setSite(o.getSite());
				response.setSiteNumber(o.getSiteNumber());
				response.setState(o.getState());
			}
		}
		
		return response;
		
	}

}
