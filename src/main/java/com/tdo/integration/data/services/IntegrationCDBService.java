package com.tdo.integration.data.services;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tdo.integration.dao.IntegrationTDODataDao;
import com.tdo.integration.data.model.tdo.Udc;
import com.tdo.integration.dto.CreditDTO;
import com.tdo.integration.dto.SalesDTO;
import com.tdo.integration.pojo.BaseResponse;

@Service("salesService")
public class IntegrationCDBService {
	
	@Autowired
	IntegrationTDODataDao integrationTDODataDao;
	
	public BaseResponse insertSales(SalesDTO sales) {
		BaseResponse response = new BaseResponse();
		boolean result = integrationTDODataDao.saveSales(sales);
		
		if(result) {
			response.setStatus("OK");
			response.setContent("Las ventas han sido registradas correctamente.");
		}else {
			response.setStatus("ERROR");
			response.setContent("Ha ocurrido un error al registrar las ventas.");
		}
		return response;	
	}

	public List<CreditDTO> getCustomerCreditByName(String value) {
		return integrationTDODataDao.getCustomerCreditByName(value);
	}
	
	public List<CreditDTO> updateCustomerCredit(List<CreditDTO> lstCreditDTO) {
		
		BigDecimal absoluteCreditLimit;
		BigDecimal localCreditAmount;
		BigDecimal localDebitAmount;
		BigDecimal localCreditAmountUsed;
        BigDecimal absoluteCreditAmount;
        BigDecimal absoluteDebitAmount;        	           
        BigDecimal absoluteAvailableCredit;        
		
		integrationTDODataDao.saveOrUpdateCredit(lstCreditDTO);
		
		if(lstCreditDTO != null && lstCreditDTO.size() > 0) {
			for(CreditDTO creditDTO : lstCreditDTO) {
								
				System.out.println("PROCESO DE LIMITE CREDITO: [SUCURSAL " + creditDTO.getSucursal() + "]" );
				System.out.println("[CLIENTE ID: " + creditDTO.getCliente().getClienteId() + " ] Limite Credito Inicial: " + creditDTO.getCliente().getClienteLimite());
				creditDTO.getCliente().setClienteLimite(new BigDecimal(0));
				System.out.println("[CLIENTE ID: " + creditDTO.getCliente().getClienteId() + " ] Limite Credito Limpio: " + creditDTO.getCliente().getClienteLimite());
				
				
				absoluteCreditLimit = new BigDecimal(0);
				localCreditAmount = new BigDecimal(0);
				localDebitAmount = new BigDecimal(0);
				localCreditAmountUsed = new BigDecimal(0);
				absoluteCreditAmount = new BigDecimal(0);
				absoluteDebitAmount = new BigDecimal(0);				
				absoluteAvailableCredit = new BigDecimal(0);
				
				absoluteCreditLimit = creditDTO.getCliente().getClienteLimiteAbsoluto() != null ? creditDTO.getCliente().getClienteLimiteAbsoluto() : new BigDecimal(0);
				System.out.println("[CLIENTE ID: " + creditDTO.getCliente().getClienteId() + " ] Limite Credito Global: " + absoluteCreditLimit);
				
				localCreditAmount = integrationTDODataDao.getCustomerCreditAmount(creditDTO.getSucursal(), creditDTO.getCliente());
				System.out.println("[CLIENTE ID: " + creditDTO.getCliente().getClienteId() + " ] Credito Sucursal: " + localCreditAmount);
				
				localDebitAmount = integrationTDODataDao.getCustomerDebitAmount(creditDTO.getSucursal(), creditDTO.getCliente());
				System.out.println("[CLIENTE ID: " + creditDTO.getCliente().getClienteId() + " ] Abonos Sucursal: " + localDebitAmount);
								
	            absoluteCreditAmount = integrationTDODataDao.getCustomerCreditAmount("", creditDTO.getCliente());
	            System.out.println("[CLIENTE ID: " + creditDTO.getCliente().getClienteId() + " ] Credito Global: " + absoluteCreditAmount);
	            
	            absoluteDebitAmount = integrationTDODataDao.getCustomerDebitAmount("", creditDTO.getCliente());
	            System.out.println("[CLIENTE ID: " + creditDTO.getCliente().getClienteId() + " ] Abono Global: " + absoluteDebitAmount);
	            
				localCreditAmountUsed = localCreditAmount.subtract(localDebitAmount);
				System.out.println("[CLIENTE ID: " + creditDTO.getCliente().getClienteId() + " ] Credito Utilizado Sucursal: " + localCreditAmountUsed);
	            
	            absoluteAvailableCredit = absoluteCreditLimit.subtract(absoluteCreditAmount.subtract(absoluteDebitAmount));
	            System.out.println("[CLIENTE ID: " + creditDTO.getCliente().getClienteId() + " ] Credito Disponible Global: " + absoluteAvailableCredit);
	            
	            creditDTO.getCliente().setClienteLimite(localCreditAmountUsed.add(absoluteAvailableCredit));
	            System.out.println("[CLIENTE ID: " + creditDTO.getCliente().getClienteId() + " ] Nuevo Limite de Credito: " + localCreditAmountUsed.add(absoluteAvailableCredit));
			}
		}
		
		return lstCreditDTO;
	}

	public BaseResponse updateCustomerCreditLimit(CreditDTO creditDTO) {
		BaseResponse response = new BaseResponse();		
        BigDecimal absoluteCreditAmount;
        BigDecimal absoluteDebitAmount;        	           
        BigDecimal absoluteCreditAmountUsed;
        
		absoluteCreditAmount = new BigDecimal(0);
		absoluteDebitAmount = new BigDecimal(0);				
		absoluteCreditAmountUsed = new BigDecimal(0);
		
        absoluteCreditAmount = integrationTDODataDao.getCustomerCreditAmount("", creditDTO.getCliente());        
        absoluteDebitAmount = integrationTDODataDao.getCustomerDebitAmount("", creditDTO.getCliente());
        absoluteCreditAmountUsed = absoluteCreditAmount.subtract(absoluteDebitAmount);

        if(creditDTO.getCliente().getClienteLimiteAbsoluto().compareTo(absoluteCreditAmountUsed) > 0) {
        	boolean result = integrationTDODataDao.updateCreditLimit(creditDTO);
        	
    		if(result) {
    			response.setStatus("OK");
    			response.setContent("El límite de crédito se ha actualizado correctamente.");
    		}else {
    			response.setStatus("ERROR");
    			response.setContent("Ha ocurrido un error al actualizar el límite de crédito. Intentelo nuevamente.");
    		}
        } else {
			response.setStatus("ERROR");
			response.setContent("El nuevo límite de crédito no puede ser menor al crédito utilizado actualmente por el cliente.");        	
        }
		
		return response;	
	}
	
	public BaseResponse saveOrUpdateUdc(Udc udc) throws Exception {
		BaseResponse response = new BaseResponse();

		boolean result=false;
		if(udc!=null) {
			result = integrationTDODataDao.saveOrUpdateUdc(udc);
		}

		if(result) {
			response.setStatus("OK");
			response.setCode(200);
			response.setContent("se ha actualizado correctamente la UDC");
		}else {
			response.setStatus("ERROR");
			response.setContent("Ha ocurrido un error al registrar la UDC");
		}
		return response;	
		
	}
	
	public List<Udc> getUdcByUdcKey(String udcKey) {
		
		return integrationTDODataDao.getUdcByUdcKey(udcKey);
	}
}
