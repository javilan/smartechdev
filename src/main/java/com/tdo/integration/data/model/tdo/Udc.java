package com.tdo.integration.data.model.tdo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "udc")
public class Udc {
	
	private static final long serialVersionUID = 1L;
	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String udckey;
	private String udcValue;
	private String udcSystem;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getUdckey() {
		return udckey;
	}
	public void setUdckey(String udckey) {
		this.udckey = udckey;
	}
	public String getUdcValue() {
		return udcValue;
	}
	public void setUdcValue(String udcValue) {
		this.udcValue = udcValue;
	}
	public String getUdcSystem() {
		return udcSystem;
	}
	public void setUdcSystem(String udcSystem) {
		this.udcSystem = udcSystem;
	}
	
	

}
