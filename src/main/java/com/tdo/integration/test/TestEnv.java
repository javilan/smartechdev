package com.tdo.integration.test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonParser;
import com.google.gson.JsonObject;


public class TestEnv {

	public static void main(String[] args) {
		
		String text = "{\"xmlns\":\"http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/types/\",\"content\":\"52759\"}";
		
		 JSONObject jsonObject = new JSONObject(text);
		 System.out.println(jsonObject.get("content"));
		
		
		String str = "------=_Part_2928_1544699196.1570918152135 " + 
				"Content-Type: application/xop+xml;charset=UTF-8;type=\"text/xml\" " + 
				"Content-Transfer-Encoding: 8bit " + 
				"Content-ID: <f8d9def2-93d8-45c6-be02-c31d67d3fcd3> " + 
				" " + 
				"<?xml version=\"1.0\" encoding=\"UTF-8\" ?> " + 
				"<env:Envelope xmlns:env=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wsa=\"http://www.w3.org/2005/08/addressing\"><env:Header><wsa:Action>http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService//ErpIntegrationService/uploadFileToUcmResponse</wsa:Action><wsa:MessageID>urn:uuid:01e5f1f0-23fb-445f-8c70-4ad0cd614908</wsa:MessageID></env:Header><env:Body><ns0:uploadFileToUcmResponse xmlns:ns0=\"http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/types/\"><result xmlns=\"http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/types/\">52758</result></ns0:uploadFileToUcmResponse></env:Body></env:Envelope> " + 
				"------=_Part_2928_1544699196.1570918152135-- ";
		
		System.out.println(getEnvResponse(str));

	}
	
	public static String getEnvResponse(String str) {
		try {
				String wordToFind = "env:Envelope";
				Pattern word = Pattern.compile(wordToFind);
				Matcher match = word.matcher(str);
		
				int start = 0;
				int end = 0;
				while (match.find()) {
				     if(start == 0) start = match.start() - 1;
				     end = match.end() + 1;
				}
				 String result = str.substring(start, end);
				 return result;
	
		}catch(Exception e) {
			e.printStackTrace();
			return "";
		}
	}
	
	private String getContentFromEnv(String str) {
		try {
			JSONObject jsonObject = new JSONObject(str);
			return (String) jsonObject.get("content");
		}catch(Exception e) {
			e.printStackTrace();
			return "";
		}
	}
}
