package com.tdo.integration.test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class TestTimeZone {

	public static void main(String[] args) {
		
		try {
			String lv_dateFormateInLocalTimeZone="";//Will hold the final converted date
			Date lv_localDate = null;
			String lv_localTimeZone ="";
			SimpleDateFormat lv_formatter;
			SimpleDateFormat lv_parser;
			 
			String v_localTimeZone="America/Mexico_City";
			 
			//create a new Date object using the UTC timezone
			lv_parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			lv_parser.setTimeZone(TimeZone.getTimeZone("UTC"));
			lv_localDate = lv_parser.parse("2019-08-14T15:34:39");
			 
			//Set output format - // prints "2007/10/25  18:35:07 EDT(-0400)"
			lv_formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss z'('Z')'");
			System.out.println("convertUTCtoLocalTime  "+ "The Date in the UTC time zone(UTC) " + lv_formatter.format(lv_localDate));
			 
			//Convert the UTC date to Local timezone
			lv_formatter.setTimeZone(TimeZone.getTimeZone(lv_localTimeZone));
			lv_dateFormateInLocalTimeZone = lv_formatter.format(lv_localDate);
			System.out.println("convertUTCtoLocalTime: "+": "+"The Date in the LocalTime Zone time zone " + lv_formatter.format(lv_localDate));
		}catch(Exception e) {
			e.printStackTrace();
		}
		

	}

}
