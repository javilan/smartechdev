package com.tdo.integration.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Serial {

	@JsonProperty("SerialNumber")
	private String SerialNumber;

	public String getSerialNumber() {
		return SerialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		SerialNumber = serialNumber;
	}
	
	
}
