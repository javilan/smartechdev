package com.tdo.integration.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect
public class InventoryTransactionSerial {
	
	@JsonProperty("INV_SERIAL_INTERFACE_NUM")
	private String INV_SERIAL_INTERFACE_NUM;
	@JsonProperty("SOURCE_CODE")
	private String SOURCE_CODE;
	@JsonProperty("SOURCE_LINE_ID")
	private String SOURCE_LINE_ID;
	@JsonProperty("FM_SERIAL_NUMBER")
	private String FM_SERIAL_NUMBER;
	@JsonProperty("TO_SERIAL_NUMBER")
	private String TO_SERIAL_NUMBER;
	@JsonProperty("ERROR_CODE")
	private String ERROR_CODE;
	@JsonProperty("PROCESS_FLAG")
	private String PROCESS_FLAG;
	@JsonProperty("PARENT_SERIAL_NUMBER")
	private String PARENT_SERIAL_NUMBER;
	@JsonProperty("SERIAL_ATTRIBUTE_CATEGORY")
	private String SERIAL_ATTRIBUTE_CATEGORY;
	@JsonProperty("TERRITORY_CODE")
	private String TERRITORY_CODE;
	@JsonProperty("ORIGINATION_DATE")
	private String ORIGINATION_DATE;
	@JsonProperty("VENDOR_SERIAL_NUMBER")
	private String VENDOR_SERIAL_NUMBER;
	@JsonProperty("VENDOR_LOT_NUMBER")
	private String VENDOR_LOT_NUMBER;
	@JsonProperty("TIME_SINCE_NEW")
	private String TIME_SINCE_NEW;
	@JsonProperty("CYCLES_SINCE_NEW")
	private String CYCLES_SINCE_NEW;
	@JsonProperty("TIME_SINCE_OVERHAUL")
	private String TIME_SINCE_OVERHAUL;
	@JsonProperty("CYCLES_SINCE_OVERHAUL")
	private String CYCLES_SINCE_OVERHAUL;
	@JsonProperty("TIME_SINCE_REPAIR")
	private String TIME_SINCE_REPAIR;
	@JsonProperty("CYCLES_SINCE_REPAIR")
	private String CYCLES_SINCE_REPAIR;
	@JsonProperty("TIME_SINCE_VISIT")
	private String TIME_SINCE_VISIT;
	@JsonProperty("CYCLES_SINCE_VISIT")
	private String CYCLES_SINCE_VISIT;
	@JsonProperty("TIME_SINCE_MARK")
	private String TIME_SINCE_MARK;
	@JsonProperty("CYCLES_SINCE_MARK")
	private String CYCLES_SINCE_MARK;
	@JsonProperty("NUMBER_OF_REPAIRS")
	private String NUMBER_OF_REPAIRS;
	@JsonProperty("STATUS_NAME")
	private String STATUS_NAME;
	@JsonProperty("STATUS_CODE")
	private String STATUS_CODE;
	@JsonProperty("PRODUCT_CODE")
	private String PRODUCT_CODE;
	@JsonProperty("PRODUCT_TRANSACTION_ID")
	private String PRODUCT_TRANSACTION_ID;
	@JsonProperty("PARENT_OBJECT_TYPE")
	private String PARENT_OBJECT_TYPE;
	@JsonProperty("PARENT_OBJECT_NUMBER")
	private String PARENT_OBJECT_NUMBER;
	@JsonProperty("PARENT_OBJECT_TYPE2")
	private String PARENT_OBJECT_TYPE2;
	@JsonProperty("PARENT_OBJECT_NUMBER2")
	private String PARENT_OBJECT_NUMBER2;
	@JsonProperty("C_ATTRIBUTE1")
	private String C_ATTRIBUTE1;
	@JsonProperty("C_ATTRIBUTE2")
	private String C_ATTRIBUTE2;
	@JsonProperty("C_ATTRIBUTE3")
	private String C_ATTRIBUTE3;
	@JsonProperty("C_ATTRIBUTE4")
	private String C_ATTRIBUTE4;
	@JsonProperty("C_ATTRIBUTE5")
	private String C_ATTRIBUTE5;
	@JsonProperty("C_ATTRIBUTE6")
	private String C_ATTRIBUTE6;
	@JsonProperty("C_ATTRIBUTE7")
	private String C_ATTRIBUTE7;
	@JsonProperty("C_ATTRIBUTE8")
	private String C_ATTRIBUTE8;
	@JsonProperty("C_ATTRIBUTE9")
	private String C_ATTRIBUTE9;
	@JsonProperty("C_ATTRIBUTE10")
	private String C_ATTRIBUTE10;
	@JsonProperty("C_ATTRIBUTE11")
	private String C_ATTRIBUTE11;
	@JsonProperty("C_ATTRIBUTE12")
	private String C_ATTRIBUTE12;
	@JsonProperty("C_ATTRIBUTE13")
	private String C_ATTRIBUTE13;
	@JsonProperty("C_ATTRIBUTE14")
	private String C_ATTRIBUTE14;
	@JsonProperty("C_ATTRIBUTE15")
	private String C_ATTRIBUTE15;
	@JsonProperty("C_ATTRIBUTE16")
	private String C_ATTRIBUTE16;
	@JsonProperty("C_ATTRIBUTE17")
	private String C_ATTRIBUTE17;
	@JsonProperty("C_ATTRIBUTE18")
	private String C_ATTRIBUTE18;
	@JsonProperty("C_ATTRIBUTE19")
	private String C_ATTRIBUTE19;
	@JsonProperty("C_ATTRIBUTE20")
	private String C_ATTRIBUTE20;
	@JsonProperty("D_ATTRIBUTE1")
	private String D_ATTRIBUTE1;
	@JsonProperty("D_ATTRIBUTE2")
	private String D_ATTRIBUTE2;
	@JsonProperty("D_ATTRIBUTE3")
	private String D_ATTRIBUTE3;
	@JsonProperty("D_ATTRIBUTE4")
	private String D_ATTRIBUTE4;
	@JsonProperty("D_ATTRIBUTE5")
	private String D_ATTRIBUTE5;
	@JsonProperty("D_ATTRIBUTE6")
	private String D_ATTRIBUTE6;
	@JsonProperty("D_ATTRIBUTE7")
	private String D_ATTRIBUTE7;
	@JsonProperty("D_ATTRIBUTE8")
	private String D_ATTRIBUTE8;
	@JsonProperty("D_ATTRIBUTE9")
	private String D_ATTRIBUTE9;
	@JsonProperty("D_ATTRIBUTE10")
	private String D_ATTRIBUTE10;
	@JsonProperty("N_ATTRIBUTE1")
	private String N_ATTRIBUTE1;
	@JsonProperty("N_ATTRIBUTE2")
	private String N_ATTRIBUTE2;
	@JsonProperty("N_ATTRIBUTE3")
	private String N_ATTRIBUTE3;
	@JsonProperty("N_ATTRIBUTE4")
	private String N_ATTRIBUTE4;
	@JsonProperty("N_ATTRIBUTE5")
	private String N_ATTRIBUTE5;
	@JsonProperty("N_ATTRIBUTE6")
	private String N_ATTRIBUTE6;
	@JsonProperty("N_ATTRIBUTE7")
	private String N_ATTRIBUTE7;
	@JsonProperty("N_ATTRIBUTE8")
	private String N_ATTRIBUTE8;
	@JsonProperty("N_ATTRIBUTE9")
	private String N_ATTRIBUTE9;
	@JsonProperty("N_ATTRIBUTE10")
	private String N_ATTRIBUTE10;
	@JsonProperty("T_ATTRIBUTE1")
	private String T_ATTRIBUTE1;
	@JsonProperty("T_ATTRIBUTE2")
	private String T_ATTRIBUTE2;
	@JsonProperty("T_ATTRIBUTE3")
	private String T_ATTRIBUTE3;
	@JsonProperty("T_ATTRIBUTE4")
	private String T_ATTRIBUTE4;
	@JsonProperty("T_ATTRIBUTE5")
	private String T_ATTRIBUTE5;
	@JsonProperty("ATTRIBUTE_CATEGORY")
	private String ATTRIBUTE_CATEGORY;
	@JsonProperty("ATTRIBUTE1")
	private String ATTRIBUTE1;
	@JsonProperty("ATTRIBUTE2")
	private String ATTRIBUTE2;
	@JsonProperty("ATTRIBUTE3")
	private String ATTRIBUTE3;
	@JsonProperty("ATTRIBUTE4")
	private String ATTRIBUTE4;
	@JsonProperty("ATTRIBUTE5")
	private String ATTRIBUTE5;
	@JsonProperty("ATTRIBUTE6")
	private String ATTRIBUTE6;
	@JsonProperty("ATTRIBUTE7")
	private String ATTRIBUTE7;
	@JsonProperty("ATTRIBUTE8")
	private String ATTRIBUTE8;
	@JsonProperty("ATTRIBUTE9")
	private String ATTRIBUTE9;
	@JsonProperty("ATTRIBUTE10")
	private String ATTRIBUTE10;
	@JsonProperty("ATTRIBUTE11")
	private String ATTRIBUTE11;
	@JsonProperty("ATTRIBUTE12")
	private String ATTRIBUTE12;
	@JsonProperty("ATTRIBUTE13")
	private String ATTRIBUTE13;
	@JsonProperty("ATTRIBUTE14")
	private String ATTRIBUTE14;
	@JsonProperty("ATTRIBUTE15")
	private String ATTRIBUTE15;
	@JsonProperty("ATTRIBUTE16")
	private String ATTRIBUTE16;
	@JsonProperty("ATTRIBUTE17")
	private String ATTRIBUTE17;
	@JsonProperty("ATTRIBUTE18")
	private String ATTRIBUTE18;
	@JsonProperty("ATTRIBUTE19")
	private String ATTRIBUTE19;
	@JsonProperty("ATTRIBUTE20")
	private String ATTRIBUTE20;
	@JsonProperty("ATTRIBUTE_NUMBER1")
	private String ATTRIBUTE_NUMBER1;
	@JsonProperty("ATTRIBUTE_NUMBER2")
	private String ATTRIBUTE_NUMBER2;
	@JsonProperty("ATTRIBUTE_NUMBER3")
	private String ATTRIBUTE_NUMBER3;
	@JsonProperty("ATTRIBUTE_NUMBER4")
	private String ATTRIBUTE_NUMBER4;
	@JsonProperty("ATTRIBUTE_NUMBER5")
	private String ATTRIBUTE_NUMBER5;
	@JsonProperty("ATTRIBUTE_NUMBER6")
	private String ATTRIBUTE_NUMBER6;
	@JsonProperty("ATTRIBUTE_NUMBER7")
	private String ATTRIBUTE_NUMBER7;
	@JsonProperty("ATTRIBUTE_NUMBER8")
	private String ATTRIBUTE_NUMBER8;
	@JsonProperty("ATTRIBUTE_NUMBER9")
	private String ATTRIBUTE_NUMBER9;
	@JsonProperty("ATTRIBUTE_NUMBER10")
	private String ATTRIBUTE_NUMBER10;
	@JsonProperty("ATTRIBUTE_DATE1")
	private String ATTRIBUTE_DATE1;
	@JsonProperty("ATTRIBUTE_DATE2")
	private String ATTRIBUTE_DATE2;
	@JsonProperty("ATTRIBUTE_DATE3")
	private String ATTRIBUTE_DATE3;
	@JsonProperty("ATTRIBUTE_DATE4")
	private String ATTRIBUTE_DATE4;
	@JsonProperty("ATTRIBUTE_DATE5")
	private String ATTRIBUTE_DATE5;
	@JsonProperty("ATTRIBUTE_TIMESTAMP1")
	private String ATTRIBUTE_TIMESTAMP1;
	@JsonProperty("ATTRIBUTE_TIMESTAMP2")
	private String ATTRIBUTE_TIMESTAMP2;
	@JsonProperty("ATTRIBUTE_TIMESTAMP3")
	private String ATTRIBUTE_TIMESTAMP3;
	@JsonProperty("ATTRIBUTE_TIMESTAMP4")
	private String ATTRIBUTE_TIMESTAMP4;
	@JsonProperty("ATTRIBUTE_TIMESTAMP5")
	private String ATTRIBUTE_TIMESTAMP5;
	@JsonProperty("END")
	private String END = "";
	
	
	public String getEND() {
		return END;
	}
	public void setEND(String eND) {
		END = eND;
	}
	public String getINV_SERIAL_INTERFACE_NUM() {
		return INV_SERIAL_INTERFACE_NUM;
	}
	public void setINV_SERIAL_INTERFACE_NUM(String iNV_SERIAL_INTERFACE_NUM) {
		INV_SERIAL_INTERFACE_NUM = iNV_SERIAL_INTERFACE_NUM;
	}
	public String getSOURCE_CODE() {
		return SOURCE_CODE;
	}
	public void setSOURCE_CODE(String sOURCE_CODE) {
		SOURCE_CODE = sOURCE_CODE;
	}
	public String getSOURCE_LINE_ID() {
		return SOURCE_LINE_ID;
	}
	public void setSOURCE_LINE_ID(String sOURCE_LINE_ID) {
		SOURCE_LINE_ID = sOURCE_LINE_ID;
	}
	public String getFM_SERIAL_NUMBER() {
		return FM_SERIAL_NUMBER;
	}
	public void setFM_SERIAL_NUMBER(String fM_SERIAL_NUMBER) {
		FM_SERIAL_NUMBER = fM_SERIAL_NUMBER;
	}
	public String getTO_SERIAL_NUMBER() {
		return TO_SERIAL_NUMBER;
	}
	public void setTO_SERIAL_NUMBER(String tO_SERIAL_NUMBER) {
		TO_SERIAL_NUMBER = tO_SERIAL_NUMBER;
	}
	public String getERROR_CODE() {
		return ERROR_CODE;
	}
	public void setERROR_CODE(String eRROR_CODE) {
		ERROR_CODE = eRROR_CODE;
	}
	public String getPROCESS_FLAG() {
		return PROCESS_FLAG;
	}
	public void setPROCESS_FLAG(String pROCESS_FLAG) {
		PROCESS_FLAG = pROCESS_FLAG;
	}
	public String getPARENT_SERIAL_NUMBER() {
		return PARENT_SERIAL_NUMBER;
	}
	public void setPARENT_SERIAL_NUMBER(String pARENT_SERIAL_NUMBER) {
		PARENT_SERIAL_NUMBER = pARENT_SERIAL_NUMBER;
	}
	public String getSERIAL_ATTRIBUTE_CATEGORY() {
		return SERIAL_ATTRIBUTE_CATEGORY;
	}
	public void setSERIAL_ATTRIBUTE_CATEGORY(String sERIAL_ATTRIBUTE_CATEGORY) {
		SERIAL_ATTRIBUTE_CATEGORY = sERIAL_ATTRIBUTE_CATEGORY;
	}
	public String getTERRITORY_CODE() {
		return TERRITORY_CODE;
	}
	public void setTERRITORY_CODE(String tERRITORY_CODE) {
		TERRITORY_CODE = tERRITORY_CODE;
	}
	public String getORIGINATION_DATE() {
		return ORIGINATION_DATE;
	}
	public void setORIGINATION_DATE(String oRIGINATION_DATE) {
		ORIGINATION_DATE = oRIGINATION_DATE;
	}
	public String getVENDOR_SERIAL_NUMBER() {
		return VENDOR_SERIAL_NUMBER;
	}
	public void setVENDOR_SERIAL_NUMBER(String vENDOR_SERIAL_NUMBER) {
		VENDOR_SERIAL_NUMBER = vENDOR_SERIAL_NUMBER;
	}
	public String getVENDOR_LOT_NUMBER() {
		return VENDOR_LOT_NUMBER;
	}
	public void setVENDOR_LOT_NUMBER(String vENDOR_LOT_NUMBER) {
		VENDOR_LOT_NUMBER = vENDOR_LOT_NUMBER;
	}
	public String getTIME_SINCE_NEW() {
		return TIME_SINCE_NEW;
	}
	public void setTIME_SINCE_NEW(String tIME_SINCE_NEW) {
		TIME_SINCE_NEW = tIME_SINCE_NEW;
	}
	public String getCYCLES_SINCE_NEW() {
		return CYCLES_SINCE_NEW;
	}
	public void setCYCLES_SINCE_NEW(String cYCLES_SINCE_NEW) {
		CYCLES_SINCE_NEW = cYCLES_SINCE_NEW;
	}
	public String getTIME_SINCE_OVERHAUL() {
		return TIME_SINCE_OVERHAUL;
	}
	public void setTIME_SINCE_OVERHAUL(String tIME_SINCE_OVERHAUL) {
		TIME_SINCE_OVERHAUL = tIME_SINCE_OVERHAUL;
	}
	public String getCYCLES_SINCE_OVERHAUL() {
		return CYCLES_SINCE_OVERHAUL;
	}
	public void setCYCLES_SINCE_OVERHAUL(String cYCLES_SINCE_OVERHAUL) {
		CYCLES_SINCE_OVERHAUL = cYCLES_SINCE_OVERHAUL;
	}
	public String getTIME_SINCE_REPAIR() {
		return TIME_SINCE_REPAIR;
	}
	public void setTIME_SINCE_REPAIR(String tIME_SINCE_REPAIR) {
		TIME_SINCE_REPAIR = tIME_SINCE_REPAIR;
	}
	public String getCYCLES_SINCE_REPAIR() {
		return CYCLES_SINCE_REPAIR;
	}
	public void setCYCLES_SINCE_REPAIR(String cYCLES_SINCE_REPAIR) {
		CYCLES_SINCE_REPAIR = cYCLES_SINCE_REPAIR;
	}
	public String getTIME_SINCE_VISIT() {
		return TIME_SINCE_VISIT;
	}
	public void setTIME_SINCE_VISIT(String tIME_SINCE_VISIT) {
		TIME_SINCE_VISIT = tIME_SINCE_VISIT;
	}
	public String getCYCLES_SINCE_VISIT() {
		return CYCLES_SINCE_VISIT;
	}
	public void setCYCLES_SINCE_VISIT(String cYCLES_SINCE_VISIT) {
		CYCLES_SINCE_VISIT = cYCLES_SINCE_VISIT;
	}
	public String getTIME_SINCE_MARK() {
		return TIME_SINCE_MARK;
	}
	public void setTIME_SINCE_MARK(String tIME_SINCE_MARK) {
		TIME_SINCE_MARK = tIME_SINCE_MARK;
	}
	public String getCYCLES_SINCE_MARK() {
		return CYCLES_SINCE_MARK;
	}
	public void setCYCLES_SINCE_MARK(String cYCLES_SINCE_MARK) {
		CYCLES_SINCE_MARK = cYCLES_SINCE_MARK;
	}
	public String getNUMBER_OF_REPAIRS() {
		return NUMBER_OF_REPAIRS;
	}
	public void setNUMBER_OF_REPAIRS(String nUMBER_OF_REPAIRS) {
		NUMBER_OF_REPAIRS = nUMBER_OF_REPAIRS;
	}
	public String getSTATUS_NAME() {
		return STATUS_NAME;
	}
	public void setSTATUS_NAME(String sTATUS_NAME) {
		STATUS_NAME = sTATUS_NAME;
	}
	public String getSTATUS_CODE() {
		return STATUS_CODE;
	}
	public void setSTATUS_CODE(String sTATUS_CODE) {
		STATUS_CODE = sTATUS_CODE;
	}
	public String getPRODUCT_CODE() {
		return PRODUCT_CODE;
	}
	public void setPRODUCT_CODE(String pRODUCT_CODE) {
		PRODUCT_CODE = pRODUCT_CODE;
	}
	public String getPRODUCT_TRANSACTION_ID() {
		return PRODUCT_TRANSACTION_ID;
	}
	public void setPRODUCT_TRANSACTION_ID(String pRODUCT_TRANSACTION_ID) {
		PRODUCT_TRANSACTION_ID = pRODUCT_TRANSACTION_ID;
	}
	public String getPARENT_OBJECT_TYPE() {
		return PARENT_OBJECT_TYPE;
	}
	public void setPARENT_OBJECT_TYPE(String pARENT_OBJECT_TYPE) {
		PARENT_OBJECT_TYPE = pARENT_OBJECT_TYPE;
	}
	public String getPARENT_OBJECT_NUMBER() {
		return PARENT_OBJECT_NUMBER;
	}
	public void setPARENT_OBJECT_NUMBER(String pARENT_OBJECT_NUMBER) {
		PARENT_OBJECT_NUMBER = pARENT_OBJECT_NUMBER;
	}
	public String getPARENT_OBJECT_TYPE2() {
		return PARENT_OBJECT_TYPE2;
	}
	public void setPARENT_OBJECT_TYPE2(String pARENT_OBJECT_TYPE2) {
		PARENT_OBJECT_TYPE2 = pARENT_OBJECT_TYPE2;
	}
	public String getPARENT_OBJECT_NUMBER2() {
		return PARENT_OBJECT_NUMBER2;
	}
	public void setPARENT_OBJECT_NUMBER2(String pARENT_OBJECT_NUMBER2) {
		PARENT_OBJECT_NUMBER2 = pARENT_OBJECT_NUMBER2;
	}
	public String getC_ATTRIBUTE1() {
		return C_ATTRIBUTE1;
	}
	public void setC_ATTRIBUTE1(String c_ATTRIBUTE1) {
		C_ATTRIBUTE1 = c_ATTRIBUTE1;
	}
	public String getC_ATTRIBUTE2() {
		return C_ATTRIBUTE2;
	}
	public void setC_ATTRIBUTE2(String c_ATTRIBUTE2) {
		C_ATTRIBUTE2 = c_ATTRIBUTE2;
	}
	public String getC_ATTRIBUTE3() {
		return C_ATTRIBUTE3;
	}
	public void setC_ATTRIBUTE3(String c_ATTRIBUTE3) {
		C_ATTRIBUTE3 = c_ATTRIBUTE3;
	}
	public String getC_ATTRIBUTE4() {
		return C_ATTRIBUTE4;
	}
	public void setC_ATTRIBUTE4(String c_ATTRIBUTE4) {
		C_ATTRIBUTE4 = c_ATTRIBUTE4;
	}
	public String getC_ATTRIBUTE5() {
		return C_ATTRIBUTE5;
	}
	public void setC_ATTRIBUTE5(String c_ATTRIBUTE5) {
		C_ATTRIBUTE5 = c_ATTRIBUTE5;
	}
	public String getC_ATTRIBUTE6() {
		return C_ATTRIBUTE6;
	}
	public void setC_ATTRIBUTE6(String c_ATTRIBUTE6) {
		C_ATTRIBUTE6 = c_ATTRIBUTE6;
	}
	public String getC_ATTRIBUTE7() {
		return C_ATTRIBUTE7;
	}
	public void setC_ATTRIBUTE7(String c_ATTRIBUTE7) {
		C_ATTRIBUTE7 = c_ATTRIBUTE7;
	}
	public String getC_ATTRIBUTE8() {
		return C_ATTRIBUTE8;
	}
	public void setC_ATTRIBUTE8(String c_ATTRIBUTE8) {
		C_ATTRIBUTE8 = c_ATTRIBUTE8;
	}
	public String getC_ATTRIBUTE9() {
		return C_ATTRIBUTE9;
	}
	public void setC_ATTRIBUTE9(String c_ATTRIBUTE9) {
		C_ATTRIBUTE9 = c_ATTRIBUTE9;
	}
	public String getC_ATTRIBUTE10() {
		return C_ATTRIBUTE10;
	}
	public void setC_ATTRIBUTE10(String c_ATTRIBUTE10) {
		C_ATTRIBUTE10 = c_ATTRIBUTE10;
	}
	public String getC_ATTRIBUTE11() {
		return C_ATTRIBUTE11;
	}
	public void setC_ATTRIBUTE11(String c_ATTRIBUTE11) {
		C_ATTRIBUTE11 = c_ATTRIBUTE11;
	}
	public String getC_ATTRIBUTE12() {
		return C_ATTRIBUTE12;
	}
	public void setC_ATTRIBUTE12(String c_ATTRIBUTE12) {
		C_ATTRIBUTE12 = c_ATTRIBUTE12;
	}
	public String getC_ATTRIBUTE13() {
		return C_ATTRIBUTE13;
	}
	public void setC_ATTRIBUTE13(String c_ATTRIBUTE13) {
		C_ATTRIBUTE13 = c_ATTRIBUTE13;
	}
	public String getC_ATTRIBUTE14() {
		return C_ATTRIBUTE14;
	}
	public void setC_ATTRIBUTE14(String c_ATTRIBUTE14) {
		C_ATTRIBUTE14 = c_ATTRIBUTE14;
	}
	public String getC_ATTRIBUTE15() {
		return C_ATTRIBUTE15;
	}
	public void setC_ATTRIBUTE15(String c_ATTRIBUTE15) {
		C_ATTRIBUTE15 = c_ATTRIBUTE15;
	}
	public String getC_ATTRIBUTE16() {
		return C_ATTRIBUTE16;
	}
	public void setC_ATTRIBUTE16(String c_ATTRIBUTE16) {
		C_ATTRIBUTE16 = c_ATTRIBUTE16;
	}
	public String getC_ATTRIBUTE17() {
		return C_ATTRIBUTE17;
	}
	public void setC_ATTRIBUTE17(String c_ATTRIBUTE17) {
		C_ATTRIBUTE17 = c_ATTRIBUTE17;
	}
	public String getC_ATTRIBUTE18() {
		return C_ATTRIBUTE18;
	}
	public void setC_ATTRIBUTE18(String c_ATTRIBUTE18) {
		C_ATTRIBUTE18 = c_ATTRIBUTE18;
	}
	public String getC_ATTRIBUTE19() {
		return C_ATTRIBUTE19;
	}
	public void setC_ATTRIBUTE19(String c_ATTRIBUTE19) {
		C_ATTRIBUTE19 = c_ATTRIBUTE19;
	}
	public String getC_ATTRIBUTE20() {
		return C_ATTRIBUTE20;
	}
	public void setC_ATTRIBUTE20(String c_ATTRIBUTE20) {
		C_ATTRIBUTE20 = c_ATTRIBUTE20;
	}
	public String getD_ATTRIBUTE1() {
		return D_ATTRIBUTE1;
	}
	public void setD_ATTRIBUTE1(String d_ATTRIBUTE1) {
		D_ATTRIBUTE1 = d_ATTRIBUTE1;
	}
	public String getD_ATTRIBUTE2() {
		return D_ATTRIBUTE2;
	}
	public void setD_ATTRIBUTE2(String d_ATTRIBUTE2) {
		D_ATTRIBUTE2 = d_ATTRIBUTE2;
	}
	public String getD_ATTRIBUTE3() {
		return D_ATTRIBUTE3;
	}
	public void setD_ATTRIBUTE3(String d_ATTRIBUTE3) {
		D_ATTRIBUTE3 = d_ATTRIBUTE3;
	}
	public String getD_ATTRIBUTE4() {
		return D_ATTRIBUTE4;
	}
	public void setD_ATTRIBUTE4(String d_ATTRIBUTE4) {
		D_ATTRIBUTE4 = d_ATTRIBUTE4;
	}
	public String getD_ATTRIBUTE5() {
		return D_ATTRIBUTE5;
	}
	public void setD_ATTRIBUTE5(String d_ATTRIBUTE5) {
		D_ATTRIBUTE5 = d_ATTRIBUTE5;
	}
	public String getD_ATTRIBUTE6() {
		return D_ATTRIBUTE6;
	}
	public void setD_ATTRIBUTE6(String d_ATTRIBUTE6) {
		D_ATTRIBUTE6 = d_ATTRIBUTE6;
	}
	public String getD_ATTRIBUTE7() {
		return D_ATTRIBUTE7;
	}
	public void setD_ATTRIBUTE7(String d_ATTRIBUTE7) {
		D_ATTRIBUTE7 = d_ATTRIBUTE7;
	}
	public String getD_ATTRIBUTE8() {
		return D_ATTRIBUTE8;
	}
	public void setD_ATTRIBUTE8(String d_ATTRIBUTE8) {
		D_ATTRIBUTE8 = d_ATTRIBUTE8;
	}
	public String getD_ATTRIBUTE9() {
		return D_ATTRIBUTE9;
	}
	public void setD_ATTRIBUTE9(String d_ATTRIBUTE9) {
		D_ATTRIBUTE9 = d_ATTRIBUTE9;
	}
	public String getD_ATTRIBUTE10() {
		return D_ATTRIBUTE10;
	}
	public void setD_ATTRIBUTE10(String d_ATTRIBUTE10) {
		D_ATTRIBUTE10 = d_ATTRIBUTE10;
	}
	public String getN_ATTRIBUTE1() {
		return N_ATTRIBUTE1;
	}
	public void setN_ATTRIBUTE1(String n_ATTRIBUTE1) {
		N_ATTRIBUTE1 = n_ATTRIBUTE1;
	}
	public String getN_ATTRIBUTE2() {
		return N_ATTRIBUTE2;
	}
	public void setN_ATTRIBUTE2(String n_ATTRIBUTE2) {
		N_ATTRIBUTE2 = n_ATTRIBUTE2;
	}
	public String getN_ATTRIBUTE3() {
		return N_ATTRIBUTE3;
	}
	public void setN_ATTRIBUTE3(String n_ATTRIBUTE3) {
		N_ATTRIBUTE3 = n_ATTRIBUTE3;
	}
	public String getN_ATTRIBUTE4() {
		return N_ATTRIBUTE4;
	}
	public void setN_ATTRIBUTE4(String n_ATTRIBUTE4) {
		N_ATTRIBUTE4 = n_ATTRIBUTE4;
	}
	public String getN_ATTRIBUTE5() {
		return N_ATTRIBUTE5;
	}
	public void setN_ATTRIBUTE5(String n_ATTRIBUTE5) {
		N_ATTRIBUTE5 = n_ATTRIBUTE5;
	}
	public String getN_ATTRIBUTE6() {
		return N_ATTRIBUTE6;
	}
	public void setN_ATTRIBUTE6(String n_ATTRIBUTE6) {
		N_ATTRIBUTE6 = n_ATTRIBUTE6;
	}
	public String getN_ATTRIBUTE7() {
		return N_ATTRIBUTE7;
	}
	public void setN_ATTRIBUTE7(String n_ATTRIBUTE7) {
		N_ATTRIBUTE7 = n_ATTRIBUTE7;
	}
	public String getN_ATTRIBUTE8() {
		return N_ATTRIBUTE8;
	}
	public void setN_ATTRIBUTE8(String n_ATTRIBUTE8) {
		N_ATTRIBUTE8 = n_ATTRIBUTE8;
	}
	public String getN_ATTRIBUTE9() {
		return N_ATTRIBUTE9;
	}
	public void setN_ATTRIBUTE9(String n_ATTRIBUTE9) {
		N_ATTRIBUTE9 = n_ATTRIBUTE9;
	}
	public String getN_ATTRIBUTE10() {
		return N_ATTRIBUTE10;
	}
	public void setN_ATTRIBUTE10(String n_ATTRIBUTE10) {
		N_ATTRIBUTE10 = n_ATTRIBUTE10;
	}
	public String getT_ATTRIBUTE1() {
		return T_ATTRIBUTE1;
	}
	public void setT_ATTRIBUTE1(String t_ATTRIBUTE1) {
		T_ATTRIBUTE1 = t_ATTRIBUTE1;
	}
	public String getT_ATTRIBUTE2() {
		return T_ATTRIBUTE2;
	}
	public void setT_ATTRIBUTE2(String t_ATTRIBUTE2) {
		T_ATTRIBUTE2 = t_ATTRIBUTE2;
	}
	public String getT_ATTRIBUTE3() {
		return T_ATTRIBUTE3;
	}
	public void setT_ATTRIBUTE3(String t_ATTRIBUTE3) {
		T_ATTRIBUTE3 = t_ATTRIBUTE3;
	}
	public String getT_ATTRIBUTE4() {
		return T_ATTRIBUTE4;
	}
	public void setT_ATTRIBUTE4(String t_ATTRIBUTE4) {
		T_ATTRIBUTE4 = t_ATTRIBUTE4;
	}
	public String getT_ATTRIBUTE5() {
		return T_ATTRIBUTE5;
	}
	public void setT_ATTRIBUTE5(String t_ATTRIBUTE5) {
		T_ATTRIBUTE5 = t_ATTRIBUTE5;
	}
	public String getATTRIBUTE_CATEGORY() {
		return ATTRIBUTE_CATEGORY;
	}
	public void setATTRIBUTE_CATEGORY(String aTTRIBUTE_CATEGORY) {
		ATTRIBUTE_CATEGORY = aTTRIBUTE_CATEGORY;
	}
	public String getATTRIBUTE1() {
		return ATTRIBUTE1;
	}
	public void setATTRIBUTE1(String aTTRIBUTE1) {
		ATTRIBUTE1 = aTTRIBUTE1;
	}
	public String getATTRIBUTE2() {
		return ATTRIBUTE2;
	}
	public void setATTRIBUTE2(String aTTRIBUTE2) {
		ATTRIBUTE2 = aTTRIBUTE2;
	}
	public String getATTRIBUTE3() {
		return ATTRIBUTE3;
	}
	public void setATTRIBUTE3(String aTTRIBUTE3) {
		ATTRIBUTE3 = aTTRIBUTE3;
	}
	public String getATTRIBUTE4() {
		return ATTRIBUTE4;
	}
	public void setATTRIBUTE4(String aTTRIBUTE4) {
		ATTRIBUTE4 = aTTRIBUTE4;
	}
	public String getATTRIBUTE5() {
		return ATTRIBUTE5;
	}
	public void setATTRIBUTE5(String aTTRIBUTE5) {
		ATTRIBUTE5 = aTTRIBUTE5;
	}
	public String getATTRIBUTE6() {
		return ATTRIBUTE6;
	}
	public void setATTRIBUTE6(String aTTRIBUTE6) {
		ATTRIBUTE6 = aTTRIBUTE6;
	}
	public String getATTRIBUTE7() {
		return ATTRIBUTE7;
	}
	public void setATTRIBUTE7(String aTTRIBUTE7) {
		ATTRIBUTE7 = aTTRIBUTE7;
	}
	public String getATTRIBUTE8() {
		return ATTRIBUTE8;
	}
	public void setATTRIBUTE8(String aTTRIBUTE8) {
		ATTRIBUTE8 = aTTRIBUTE8;
	}
	public String getATTRIBUTE9() {
		return ATTRIBUTE9;
	}
	public void setATTRIBUTE9(String aTTRIBUTE9) {
		ATTRIBUTE9 = aTTRIBUTE9;
	}
	public String getATTRIBUTE10() {
		return ATTRIBUTE10;
	}
	public void setATTRIBUTE10(String aTTRIBUTE10) {
		ATTRIBUTE10 = aTTRIBUTE10;
	}
	public String getATTRIBUTE11() {
		return ATTRIBUTE11;
	}
	public void setATTRIBUTE11(String aTTRIBUTE11) {
		ATTRIBUTE11 = aTTRIBUTE11;
	}
	public String getATTRIBUTE12() {
		return ATTRIBUTE12;
	}
	public void setATTRIBUTE12(String aTTRIBUTE12) {
		ATTRIBUTE12 = aTTRIBUTE12;
	}
	public String getATTRIBUTE13() {
		return ATTRIBUTE13;
	}
	public void setATTRIBUTE13(String aTTRIBUTE13) {
		ATTRIBUTE13 = aTTRIBUTE13;
	}
	public String getATTRIBUTE14() {
		return ATTRIBUTE14;
	}
	public void setATTRIBUTE14(String aTTRIBUTE14) {
		ATTRIBUTE14 = aTTRIBUTE14;
	}
	public String getATTRIBUTE15() {
		return ATTRIBUTE15;
	}
	public void setATTRIBUTE15(String aTTRIBUTE15) {
		ATTRIBUTE15 = aTTRIBUTE15;
	}
	public String getATTRIBUTE16() {
		return ATTRIBUTE16;
	}
	public void setATTRIBUTE16(String aTTRIBUTE16) {
		ATTRIBUTE16 = aTTRIBUTE16;
	}
	public String getATTRIBUTE17() {
		return ATTRIBUTE17;
	}
	public void setATTRIBUTE17(String aTTRIBUTE17) {
		ATTRIBUTE17 = aTTRIBUTE17;
	}
	public String getATTRIBUTE18() {
		return ATTRIBUTE18;
	}
	public void setATTRIBUTE18(String aTTRIBUTE18) {
		ATTRIBUTE18 = aTTRIBUTE18;
	}
	public String getATTRIBUTE19() {
		return ATTRIBUTE19;
	}
	public void setATTRIBUTE19(String aTTRIBUTE19) {
		ATTRIBUTE19 = aTTRIBUTE19;
	}
	public String getATTRIBUTE20() {
		return ATTRIBUTE20;
	}
	public void setATTRIBUTE20(String aTTRIBUTE20) {
		ATTRIBUTE20 = aTTRIBUTE20;
	}
	public String getATTRIBUTE_NUMBER1() {
		return ATTRIBUTE_NUMBER1;
	}
	public void setATTRIBUTE_NUMBER1(String aTTRIBUTE_NUMBER1) {
		ATTRIBUTE_NUMBER1 = aTTRIBUTE_NUMBER1;
	}
	public String getATTRIBUTE_NUMBER2() {
		return ATTRIBUTE_NUMBER2;
	}
	public void setATTRIBUTE_NUMBER2(String aTTRIBUTE_NUMBER2) {
		ATTRIBUTE_NUMBER2 = aTTRIBUTE_NUMBER2;
	}
	public String getATTRIBUTE_NUMBER3() {
		return ATTRIBUTE_NUMBER3;
	}
	public void setATTRIBUTE_NUMBER3(String aTTRIBUTE_NUMBER3) {
		ATTRIBUTE_NUMBER3 = aTTRIBUTE_NUMBER3;
	}
	public String getATTRIBUTE_NUMBER4() {
		return ATTRIBUTE_NUMBER4;
	}
	public void setATTRIBUTE_NUMBER4(String aTTRIBUTE_NUMBER4) {
		ATTRIBUTE_NUMBER4 = aTTRIBUTE_NUMBER4;
	}
	public String getATTRIBUTE_NUMBER5() {
		return ATTRIBUTE_NUMBER5;
	}
	public void setATTRIBUTE_NUMBER5(String aTTRIBUTE_NUMBER5) {
		ATTRIBUTE_NUMBER5 = aTTRIBUTE_NUMBER5;
	}
	public String getATTRIBUTE_NUMBER6() {
		return ATTRIBUTE_NUMBER6;
	}
	public void setATTRIBUTE_NUMBER6(String aTTRIBUTE_NUMBER6) {
		ATTRIBUTE_NUMBER6 = aTTRIBUTE_NUMBER6;
	}
	public String getATTRIBUTE_NUMBER7() {
		return ATTRIBUTE_NUMBER7;
	}
	public void setATTRIBUTE_NUMBER7(String aTTRIBUTE_NUMBER7) {
		ATTRIBUTE_NUMBER7 = aTTRIBUTE_NUMBER7;
	}
	public String getATTRIBUTE_NUMBER8() {
		return ATTRIBUTE_NUMBER8;
	}
	public void setATTRIBUTE_NUMBER8(String aTTRIBUTE_NUMBER8) {
		ATTRIBUTE_NUMBER8 = aTTRIBUTE_NUMBER8;
	}
	public String getATTRIBUTE_NUMBER9() {
		return ATTRIBUTE_NUMBER9;
	}
	public void setATTRIBUTE_NUMBER9(String aTTRIBUTE_NUMBER9) {
		ATTRIBUTE_NUMBER9 = aTTRIBUTE_NUMBER9;
	}
	public String getATTRIBUTE_NUMBER10() {
		return ATTRIBUTE_NUMBER10;
	}
	public void setATTRIBUTE_NUMBER10(String aTTRIBUTE_NUMBER10) {
		ATTRIBUTE_NUMBER10 = aTTRIBUTE_NUMBER10;
	}
	public String getATTRIBUTE_DATE1() {
		return ATTRIBUTE_DATE1;
	}
	public void setATTRIBUTE_DATE1(String aTTRIBUTE_DATE1) {
		ATTRIBUTE_DATE1 = aTTRIBUTE_DATE1;
	}
	public String getATTRIBUTE_DATE2() {
		return ATTRIBUTE_DATE2;
	}
	public void setATTRIBUTE_DATE2(String aTTRIBUTE_DATE2) {
		ATTRIBUTE_DATE2 = aTTRIBUTE_DATE2;
	}
	public String getATTRIBUTE_DATE3() {
		return ATTRIBUTE_DATE3;
	}
	public void setATTRIBUTE_DATE3(String aTTRIBUTE_DATE3) {
		ATTRIBUTE_DATE3 = aTTRIBUTE_DATE3;
	}
	public String getATTRIBUTE_DATE4() {
		return ATTRIBUTE_DATE4;
	}
	public void setATTRIBUTE_DATE4(String aTTRIBUTE_DATE4) {
		ATTRIBUTE_DATE4 = aTTRIBUTE_DATE4;
	}
	public String getATTRIBUTE_DATE5() {
		return ATTRIBUTE_DATE5;
	}
	public void setATTRIBUTE_DATE5(String aTTRIBUTE_DATE5) {
		ATTRIBUTE_DATE5 = aTTRIBUTE_DATE5;
	}
	public String getATTRIBUTE_TIMESTAMP1() {
		return ATTRIBUTE_TIMESTAMP1;
	}
	public void setATTRIBUTE_TIMESTAMP1(String aTTRIBUTE_TIMESTAMP1) {
		ATTRIBUTE_TIMESTAMP1 = aTTRIBUTE_TIMESTAMP1;
	}
	public String getATTRIBUTE_TIMESTAMP2() {
		return ATTRIBUTE_TIMESTAMP2;
	}
	public void setATTRIBUTE_TIMESTAMP2(String aTTRIBUTE_TIMESTAMP2) {
		ATTRIBUTE_TIMESTAMP2 = aTTRIBUTE_TIMESTAMP2;
	}
	public String getATTRIBUTE_TIMESTAMP3() {
		return ATTRIBUTE_TIMESTAMP3;
	}
	public void setATTRIBUTE_TIMESTAMP3(String aTTRIBUTE_TIMESTAMP3) {
		ATTRIBUTE_TIMESTAMP3 = aTTRIBUTE_TIMESTAMP3;
	}
	public String getATTRIBUTE_TIMESTAMP4() {
		return ATTRIBUTE_TIMESTAMP4;
	}
	public void setATTRIBUTE_TIMESTAMP4(String aTTRIBUTE_TIMESTAMP4) {
		ATTRIBUTE_TIMESTAMP4 = aTTRIBUTE_TIMESTAMP4;
	}
	public String getATTRIBUTE_TIMESTAMP5() {
		return ATTRIBUTE_TIMESTAMP5;
	}
	public void setATTRIBUTE_TIMESTAMP5(String aTTRIBUTE_TIMESTAMP5) {
		ATTRIBUTE_TIMESTAMP5 = aTTRIBUTE_TIMESTAMP5;
	}

	
	

}

