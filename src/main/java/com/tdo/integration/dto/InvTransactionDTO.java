package com.tdo.integration.dto;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;


public class InvTransactionDTO {
	
	@JsonProperty("InventoryOrganization")
	private String InventoryOrganization;
	
	@JsonProperty("TransactionDate")
	private String TransactionDate;
	
	@JsonProperty("TransactionType")
	private String TransactionType;
	
	@JsonProperty("VentaIdPOS")
	private String VentaIdPOS;
	
	@JsonProperty("Lines")
	private List<TransactionLines> Lines;

	public String getInventoryOrganization() {
		return InventoryOrganization;
	}

	public void setInventoryOrganization(String inventoryOrganization) {
		InventoryOrganization = inventoryOrganization;
	}

	public String getTransactionDate() {
		return TransactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		TransactionDate = transactionDate;
	}

	public String getTransactionType() {
		return TransactionType;
	}

	public void setTransactionType(String transactionType) {
		TransactionType = transactionType;
	}

	public String getVentaIdPOS() {
		return VentaIdPOS;
	}

	public void setVentaIdPOS(String ventaIdPOS) {
		VentaIdPOS = ventaIdPOS;
	}

	public List<TransactionLines> getLines() {
		return Lines;
	}

	public void setLines(List<TransactionLines> lines) {
		Lines = lines;
	}
	
	
}
