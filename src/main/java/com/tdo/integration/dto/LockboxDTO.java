package com.tdo.integration.dto;

public class LockboxDTO {

	private String _recordType = "";
	private String _itemNumber = "";
	private String _remittanceAmount = "";
	private String _receiptNumber = "";
	private String _receiptDate = "";
	private String _currency = "";
	private String _conversionRate = "";
	private String _customerAccountNumber = "";
	private String _customerSite = "";
	private String _receiptMethod = "";
	private String _lockboxNumber = "";
	private String _transactionReference1 = "";
	private String _appliedAmount1 = "";
	
	public String get_recordType() {
		return _recordType;
	}
	public void set_recordType(String _recordType) {
		this._recordType = _recordType;
	}
	public String get_itemNumber() {
		return _itemNumber;
	}
	public void set_itemNumber(String _itemNumber) {
		this._itemNumber = _itemNumber;
	}
	public String get_remittanceAmount() {
		return _remittanceAmount;
	}
	public void set_remittanceAmount(String _remittanceAmount) {
		this._remittanceAmount = _remittanceAmount;
	}
	public String get_receiptNumber() {
		return _receiptNumber;
	}
	public void set_receiptNumber(String _receiptNumber) {
		this._receiptNumber = _receiptNumber;
	}
	public String get_receiptDate() {
		return _receiptDate;
	}
	public void set_receiptDate(String _receiptDate) {
		this._receiptDate = _receiptDate;
	}
	public String get_currency() {
		return _currency;
	}
	public void set_currency(String _currency) {
		this._currency = _currency;
	}
	public String get_customerAccountNumber() {
		return _customerAccountNumber;
	}
	public void set_customerAccountNumber(String _customerAccountNumber) {
		this._customerAccountNumber = _customerAccountNumber;
	}
	public String get_customerSite() {
		return _customerSite;
	}
	public void set_customerSite(String _customerSite) {
		this._customerSite = _customerSite;
	}
	public String get_receiptMethod() {
		return _receiptMethod;
	}
	public void set_receiptMethod(String _receiptMethod) {
		this._receiptMethod = _receiptMethod;
	}
	public String get_lockboxNumber() {
		return _lockboxNumber;
	}
	public void set_lockboxNumber(String _lockboxNumber) {
		this._lockboxNumber = _lockboxNumber;
	}
	public String get_transactionReference1() {
		return _transactionReference1;
	}
	public void set_transactionReference1(String _transactionReference1) {
		this._transactionReference1 = _transactionReference1;
	}
	public String get_appliedAmount1() {
		return _appliedAmount1;
	}
	public void set_appliedAmount1(String _appliedAmount1) {
		this._appliedAmount1 = _appliedAmount1;
	}	
	public String get_conversionRate() {
		return _conversionRate;
	}
	public void set_conversionRate(String _conversionRate) {
		this._conversionRate = _conversionRate;
	}
	
	@Override
	public String toString() {
		return "LockboxDTO [_recordType=" + _recordType + ", _itemNumber=" + _itemNumber + ", _remittanceAmount="
				+ _remittanceAmount + ", _receiptNumber=" + _receiptNumber + ", _receiptDate=" + _receiptDate
				+ ", _currency=" + _currency + ", _conversionRate=" + _conversionRate + ", _customerAccountNumber="
				+ _customerAccountNumber + ", _customerSite=" + _customerSite + ", _receiptMethod=" + _receiptMethod
				+ ", _lockboxNumber=" + _lockboxNumber + ", _transactionReference1=" + _transactionReference1
				+ ", _appliedAmount1=" + _appliedAmount1 + "]";
	}
	
	
	
	
	
	







	
}
