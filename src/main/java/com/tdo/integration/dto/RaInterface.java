package com.tdo.integration.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public class RaInterface {
	
	List<RaInterfaceLinesAll> raInterfaceLinesAll;
	List<RaInterfaceDistributionsAll> raInterfaceDistributionsAll;
	
	public List<RaInterfaceLinesAll> getRaInterfaceLinesAll() {
		return raInterfaceLinesAll;
	}
	public void setRaInterfaceLinesAll(List<RaInterfaceLinesAll> raInterfaceLinesAll) {
		this.raInterfaceLinesAll = raInterfaceLinesAll;
	}
	public List<RaInterfaceDistributionsAll> getRaInterfaceDistributionsAll() {
		return raInterfaceDistributionsAll;
	}
	public void setRaInterfaceDistributionsAll(List<RaInterfaceDistributionsAll> raInterfaceDistributionsAll) {
		this.raInterfaceDistributionsAll = raInterfaceDistributionsAll;
	}
	
	

}
