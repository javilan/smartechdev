package com.tdo.integration.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SalesLinesTDO {

	@JsonProperty("LineNum")
	private short LineNum;
	
	@JsonProperty("ItemSKU")
	private String ItemSKU;
	
	@JsonProperty("ItemDescription")
	private String ItemDescription;
	
	@JsonProperty("UOM")
	private String UOM;
	
	@JsonProperty("ItemQuantity")	// only +
	private int ItemQuantity;
	
	@JsonProperty("TotalAmount")
	private int TotalAmount;
	
	@JsonProperty("ItemUnitPrice")
	private int ItemUnitPrice;

		
	public String getUOM() {
		return UOM;
	}

	public void setUOM(String uOM) {
		UOM = uOM;
	}

	public short getLineNum() {
		return LineNum;
	}

	public void setLineNum(short lineNum) {
		LineNum = lineNum;
	}

	public String getItemSKU() {
		return ItemSKU;
	}

	public void setItemSKU(String itemSKU) {
		ItemSKU = itemSKU;
	}

	public String getItemDescription() {
		return ItemDescription;
	}

	public void setItemDescription(String itemDescription) {
		ItemDescription = itemDescription;
	}

	public int getItemQuantity() {
		return ItemQuantity;
	}

	public void setItemQuantity(int itemQuantity) {
		ItemQuantity = itemQuantity;
	}

	public int getTotalAmount() {
		return TotalAmount;
	}

	public void setTotalAmount(int totalAmount) {
		TotalAmount = totalAmount;
	}

	public int getItemUnitPrice() {
		return ItemUnitPrice;
	}

	public void setItemUnitPrice(int itemUnitPrice) {
		ItemUnitPrice = itemUnitPrice;
	}

	@Override
	public String toString() {
		return "SalesLinesTDO [LineNum=" + LineNum + ", ItemSKU=" + ItemSKU + ", ItemDescription=" + ItemDescription
				+ ", ItemQuantity=" + ItemQuantity + ", TotalAmount=" + TotalAmount + ", ItemUnitPrice=" + ItemUnitPrice
				+ "]";
	}
	
	
}
