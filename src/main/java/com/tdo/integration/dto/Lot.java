package com.tdo.integration.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Lot{
	
	@JsonProperty("LotNumber")
	private String LotNumber;
	
	@JsonProperty("LotQuantity")
	private int LotQuantity;

	public String getLotNumber() {
		return LotNumber;
	}

	public void setLotNumber(String lotNumber) {
		LotNumber = lotNumber;
	}

	public int getLotQuantity() {
		return LotQuantity;
	}

	public void setLotQuantity(int lotQuantity) {
		LotQuantity = lotQuantity;
	}
	
	
	
}
