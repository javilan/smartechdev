package com.tdo.integration.dto;

import java.util.List;

public class PriceListKauffmanDTO {
	private String PriceListName;
	private String StatusCode;
	private String BusinessUnit;
	private String requestType;
	
	List<ItemPriceDTO> items;

	public String getPriceListName() {
		return PriceListName;
	}

	public void setPriceListName(String priceListName) {
		PriceListName = priceListName;
	}

	public String getStatusCode() {
		return StatusCode;
	}

	public void setStatusCode(String statusCode) {
		StatusCode = statusCode;
	}

	public String getBusinessUnit() {
		return BusinessUnit;
	}

	public void setBusinessUnit(String businessUnit) {
		BusinessUnit = businessUnit;
	}

	public List<ItemPriceDTO> getItems() {
		return items;
	}

	public void setItems(List<ItemPriceDTO> items) {
		this.items = items;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	

	
}
