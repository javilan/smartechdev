package com.tdo.integration.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TransactionLines {
	
	@JsonProperty("LineId")
	private String LineId;
	
	@JsonProperty("ItemSKU")
	private String ItemSKU;
	
	@JsonProperty("Subinventory")
	private String Subinventory;
	
	@JsonProperty("Quantity")
	private int Quantity;
	
	@JsonProperty("UOM")
	private String UOM;
	
	@JsonProperty("ReservationId")
	private long ReservationId;
	
	@JsonProperty("SerialLots")
	private String SerialLots;
	
	@JsonProperty("Serials")
	private Serial Serials;
	
	@JsonProperty("Lots")
	private Lot Lots;
	
	@JsonProperty("Devoluciones")
	private Devolucion Devoluciones;

	public String getLineId() {
		return LineId;
	}

	public void setLineId(String lineId) {
		LineId = lineId;
	}

	public String getItemSKU() {
		return ItemSKU;
	}

	public void setItemSKU(String itemSKU) {
		ItemSKU = itemSKU;
	}

	public String getSubinventory() {
		return Subinventory;
	}

	public void setSubinventory(String subinventory) {
		Subinventory = subinventory;
	}

	public int getQuantity() {
		return Quantity;
	}

	public void setQuantity(int quantity) {
		Quantity = quantity;
	}

	public String getUOM() {
		return UOM;
	}

	public void setUOM(String uOM) {
		UOM = uOM;
	}

	public long getReservationId() {
		return ReservationId;
	}

	public void setReservationId(long reservationId) {
		ReservationId = reservationId;
	}

	public String getSerialLots() {
		return SerialLots;
	}

	public void setSerialLots(String serialLots) {
		SerialLots = serialLots;
	}

	public Serial getSerials() {
		return Serials;
	}

	public void setSerials(Serial serials) {
		Serials = serials;
	}

	public Lot getLots() {
		return Lots;
	}

	public void setLots(Lot lots) {
		Lots = lots;
	}

	public Devolucion getDevoluciones() {
		return Devoluciones;
	}

	public void setDevoluciones(Devolucion devoluciones) {
		Devoluciones = devoluciones;
	}
	
	
}
