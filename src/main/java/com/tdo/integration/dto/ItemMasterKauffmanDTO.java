package com.tdo.integration.dto;

public class ItemMasterKauffmanDTO {

	private String sku  = "";
	private String arsoort  = "";
	private String samengest   = "0";
	private String kortoms  = "";
	private String oms300  = "";
	private String oms301  = "";
	private String oms302  = "";
	private String oms303  = "";
	private String oms304  = "";
	private String artgrp  = "";
	private String magcode = "";
	private String voorrart  = "";
	private String eenhverk  = "";
	private String aantverp  = "";
	private String verkpverp  = "";
	private String levcrdnr  = "";
	private String bestadvc  = "0";
	private String btwverk  = "";
	private String btwink  = "";
	private String statistnr  = "";
	private String omzstatjn  = "";
	private String backordart   = "";
	private String verpoms  = "";
	private String serieart  = "";
	private String orderink  = "";
	private String verpdummy  = "";
	private String emballaant  = "";
	private String vwrd1  = "";
	private String vwrd2  = "";
	private String gip  = "";
	private String vvp  = "";
	private String totnontv  = "";
	private String voorrgeg  = "";
	private String xartgeg  = "";
	private String intfacink  = "";
	private String intfacvrk  = "";
	private String stateenh  = "";
	private String nettogew  = "";
	private String brutogew  = "";
	private String volume  = "";
	private String generatie  = "";
	private String artcdverv  = "";
	private String garantie  = "";
	private String houdbaar  = "";
	private String imccode  = "";
	private String btwvlevy  = "";
	private String vmargin = "";

	private String u_CVEPRODSERV="";
	private String u_CVEUNIDAD="";
	private String u_artilinea="";
	private String u_cdesvmica="";
	private String u_clasedemic="";
	private String u_colorprove="";
	private String u_convenio="";
	private String u_cprincaro="";
	private String u_cprincmica="";
	private String u_csecundaro="";
	private String u_curvabase="";
	private String u_descuento="";
	private String u_familia="";
	private String u_forma="";
	private String u_gtiaext="";
	private String u_marca="";
	private String u_material="";
	private String u_medidaaro="";
	private String u_medidapuen="";
	private String u_medidavari="";
	private String u_modelodela="";
	private String u_nuevo="";
	private String u_paragenero="";
	private String u_pedido="";
	private String u_promocion="";
	private String u_tipo="";
	private String u_tipoarmazo="";
	private String u_tipoproduc="";
	
	private String inkstatJn = "";
	private String uDesclarga = "";
	private String uFechalimit = "";
	private String uFechaAlta = "";
	
	private String creationDate ="";
	private String modifiedDate ="";
	private String type ="";
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public String getArsoort() {
		return arsoort;
	}
	public void setArsoort(String arsoort) {
		this.arsoort = arsoort;
	}
	public String getSamengest() {
		return samengest;
	}
	public void setSamengest(String samengest) {
		this.samengest = samengest;
	}
	public String getKortoms() {
		return kortoms;
	}
	public void setKortoms(String kortoms) {
		this.kortoms = kortoms;
	}
	public String getOms300() {
		return oms300;
	}
	public void setOms300(String oms300) {
		this.oms300 = oms300;
	}
	public String getOms301() {
		return oms301;
	}
	public void setOms301(String oms301) {
		this.oms301 = oms301;
	}
	public String getOms302() {
		return oms302;
	}
	public void setOms302(String oms302) {
		this.oms302 = oms302;
	}
	public String getOms303() {
		return oms303;
	}
	public void setOms303(String oms303) {
		this.oms303 = oms303;
	}
	public String getOms304() {
		return oms304;
	}
	public void setOms304(String oms304) {
		this.oms304 = oms304;
	}
	public String getArtgrp() {
		return artgrp;
	}
	public void setArtgrp(String artgrp) {
		this.artgrp = artgrp;
	}
	public String getMagcode() {
		return magcode;
	}
	public void setMagcode(String magcode) {
		this.magcode = magcode;
	}
	public String getVoorrart() {
		return voorrart;
	}
	public void setVoorrart(String voorrart) {
		this.voorrart = voorrart;
	}
	public String getEenhverk() {
		return eenhverk;
	}
	public void setEenhverk(String eenhverk) {
		this.eenhverk = eenhverk;
	}
	public String getAantverp() {
		return aantverp;
	}
	public void setAantverp(String aantverp) {
		this.aantverp = aantverp;
	}
	public String getVerkpverp() {
		return verkpverp;
	}
	public void setVerkpverp(String verkpverp) {
		this.verkpverp = verkpverp;
	}
	public String getLevcrdnr() {
		return levcrdnr;
	}
	public void setLevcrdnr(String levcrdnr) {
		this.levcrdnr = levcrdnr;
	}
	public String getBestadvc() {
		return bestadvc;
	}
	public void setBestadvc(String bestadvc) {
		this.bestadvc = bestadvc;
	}
	public String getBtwverk() {
		return btwverk;
	}
	public void setBtwverk(String btwverk) {
		this.btwverk = btwverk;
	}
	public String getBtwink() {
		return btwink;
	}
	public void setBtwink(String btwink) {
		this.btwink = btwink;
	}
	public String getStatistnr() {
		return statistnr;
	}
	public void setStatistnr(String statistnr) {
		this.statistnr = statistnr;
	}
	public String getOmzstatjn() {
		return omzstatjn;
	}
	public void setOmzstatjn(String omzstatjn) {
		this.omzstatjn = omzstatjn;
	}
	public String getBackordart() {
		return backordart;
	}
	public void setBackordart(String backordart) {
		this.backordart = backordart;
	}
	public String getVerpoms() {
		return verpoms;
	}
	public void setVerpoms(String verpoms) {
		this.verpoms = verpoms;
	}
	public String getSerieart() {
		return serieart;
	}
	public void setSerieart(String serieart) {
		this.serieart = serieart;
	}
	public String getOrderink() {
		return orderink;
	}
	public void setOrderink(String orderink) {
		this.orderink = orderink;
	}
	public String getVerpdummy() {
		return verpdummy;
	}
	public void setVerpdummy(String verpdummy) {
		this.verpdummy = verpdummy;
	}
	public String getEmballaant() {
		return emballaant;
	}
	public void setEmballaant(String emballaant) {
		this.emballaant = emballaant;
	}
	public String getVwrd1() {
		return vwrd1;
	}
	public void setVwrd1(String vwrd1) {
		this.vwrd1 = vwrd1;
	}
	public String getVwrd2() {
		return vwrd2;
	}
	public void setVwrd2(String vwrd2) {
		this.vwrd2 = vwrd2;
	}
	public String getGip() {
		return gip;
	}
	public void setGip(String gip) {
		this.gip = gip;
	}
	public String getVvp() {
		return vvp;
	}
	public void setVvp(String vvp) {
		this.vvp = vvp;
	}
	public String getTotnontv() {
		return totnontv;
	}
	public void setTotnontv(String totnontv) {
		this.totnontv = totnontv;
	}
	public String getVoorrgeg() {
		return voorrgeg;
	}
	public void setVoorrgeg(String voorrgeg) {
		this.voorrgeg = voorrgeg;
	}
	public String getXartgeg() {
		return xartgeg;
	}
	public void setXartgeg(String xartgeg) {
		this.xartgeg = xartgeg;
	}
	public String getIntfacink() {
		return intfacink;
	}
	public void setIntfacink(String intfacink) {
		this.intfacink = intfacink;
	}
	public String getIntfacvrk() {
		return intfacvrk;
	}
	public void setIntfacvrk(String intfacvrk) {
		this.intfacvrk = intfacvrk;
	}
	public String getStateenh() {
		return stateenh;
	}
	public void setStateenh(String stateenh) {
		this.stateenh = stateenh;
	}
	public String getNettogew() {
		return nettogew;
	}
	public void setNettogew(String nettogew) {
		this.nettogew = nettogew;
	}
	public String getBrutogew() {
		return brutogew;
	}
	public void setBrutogew(String brutogew) {
		this.brutogew = brutogew;
	}
	public String getVolume() {
		return volume;
	}
	public void setVolume(String volume) {
		this.volume = volume;
	}
	public String getGeneratie() {
		return generatie;
	}
	public void setGeneratie(String generatie) {
		this.generatie = generatie;
	}
	public String getArtcdverv() {
		return artcdverv;
	}
	public void setArtcdverv(String artcdverv) {
		this.artcdverv = artcdverv;
	}
	public String getGarantie() {
		return garantie;
	}
	public void setGarantie(String garantie) {
		this.garantie = garantie;
	}
	public String getHoudbaar() {
		return houdbaar;
	}
	public void setHoudbaar(String houdbaar) {
		this.houdbaar = houdbaar;
	}
	public String getImccode() {
		return imccode;
	}
	public void setImccode(String imccode) {
		this.imccode = imccode;
	}
	public String getBtwvlevy() {
		return btwvlevy;
	}
	public void setBtwvlevy(String btwvlevy) {
		this.btwvlevy = btwvlevy;
	}
	public String getVmargin() {
		return vmargin;
	}
	public void setVmargin(String vmargin) {
		this.vmargin = vmargin;
	}
	public String getU_CVEPRODSERV() {
		return u_CVEPRODSERV;
	}
	public void setU_CVEPRODSERV(String u_CVEPRODSERV) {
		this.u_CVEPRODSERV = u_CVEPRODSERV;
	}
	public String getU_CVEUNIDAD() {
		return u_CVEUNIDAD;
	}
	public void setU_CVEUNIDAD(String u_CVEUNIDAD) {
		this.u_CVEUNIDAD = u_CVEUNIDAD;
	}
	public String getU_artilinea() {
		return u_artilinea;
	}
	public void setU_artilinea(String u_artilinea) {
		this.u_artilinea = u_artilinea;
	}
	public String getU_cdesvmica() {
		return u_cdesvmica;
	}
	public void setU_cdesvmica(String u_cdesvmica) {
		this.u_cdesvmica = u_cdesvmica;
	}
	public String getU_clasedemic() {
		return u_clasedemic;
	}
	public void setU_clasedemic(String u_clasedemic) {
		this.u_clasedemic = u_clasedemic;
	}
	public String getU_colorprove() {
		return u_colorprove;
	}
	public void setU_colorprove(String u_colorprove) {
		this.u_colorprove = u_colorprove;
	}
	public String getU_convenio() {
		return u_convenio;
	}
	public void setU_convenio(String u_convenio) {
		this.u_convenio = u_convenio;
	}
	public String getU_cprincaro() {
		return u_cprincaro;
	}
	public void setU_cprincaro(String u_cprincaro) {
		this.u_cprincaro = u_cprincaro;
	}
	public String getU_cprincmica() {
		return u_cprincmica;
	}
	public void setU_cprincmica(String u_cprincmica) {
		this.u_cprincmica = u_cprincmica;
	}
	public String getU_csecundaro() {
		return u_csecundaro;
	}
	public void setU_csecundaro(String u_csecundaro) {
		this.u_csecundaro = u_csecundaro;
	}
	public String getU_curvabase() {
		return u_curvabase;
	}
	public void setU_curvabase(String u_curvabase) {
		this.u_curvabase = u_curvabase;
	}
	public String getU_descuento() {
		return u_descuento;
	}
	public void setU_descuento(String u_descuento) {
		this.u_descuento = u_descuento;
	}
	public String getU_familia() {
		return u_familia;
	}
	public void setU_familia(String u_familia) {
		this.u_familia = u_familia;
	}
	public String getU_forma() {
		return u_forma;
	}
	public void setU_forma(String u_forma) {
		this.u_forma = u_forma;
	}
	public String getU_gtiaext() {
		return u_gtiaext;
	}
	public void setU_gtiaext(String u_gtiaext) {
		this.u_gtiaext = u_gtiaext;
	}
	public String getU_marca() {
		return u_marca;
	}
	public void setU_marca(String u_marca) {
		this.u_marca = u_marca;
	}
	public String getU_material() {
		return u_material;
	}
	public void setU_material(String u_material) {
		this.u_material = u_material;
	}
	public String getU_medidaaro() {
		return u_medidaaro;
	}
	public void setU_medidaaro(String u_medidaaro) {
		this.u_medidaaro = u_medidaaro;
	}
	public String getU_medidapuen() {
		return u_medidapuen;
	}
	public void setU_medidapuen(String u_medidapuen) {
		this.u_medidapuen = u_medidapuen;
	}
	public String getU_medidavari() {
		return u_medidavari;
	}
	public void setU_medidavari(String u_medidavari) {
		this.u_medidavari = u_medidavari;
	}
	public String getU_modelodela() {
		return u_modelodela;
	}
	public void setU_modelodela(String u_modelodela) {
		this.u_modelodela = u_modelodela;
	}
	public String getU_nuevo() {
		return u_nuevo;
	}
	public void setU_nuevo(String u_nuevo) {
		this.u_nuevo = u_nuevo;
	}
	public String getU_paragenero() {
		return u_paragenero;
	}
	public void setU_paragenero(String u_paragenero) {
		this.u_paragenero = u_paragenero;
	}
	public String getU_pedido() {
		return u_pedido;
	}
	public void setU_pedido(String u_pedido) {
		this.u_pedido = u_pedido;
	}
	public String getU_promocion() {
		return u_promocion;
	}
	public void setU_promocion(String u_promocion) {
		this.u_promocion = u_promocion;
	}
	public String getU_tipo() {
		return u_tipo;
	}
	public void setU_tipo(String u_tipo) {
		this.u_tipo = u_tipo;
	}
	public String getU_tipoarmazo() {
		return u_tipoarmazo;
	}
	public void setU_tipoarmazo(String u_tipoarmazo) {
		this.u_tipoarmazo = u_tipoarmazo;
	}
	public String getU_tipoproduc() {
		return u_tipoproduc;
	}
	public void setU_tipoproduc(String u_tipoproduc) {
		this.u_tipoproduc = u_tipoproduc;
	}
	public String getInkstatJn() {
		return inkstatJn;
	}
	public void setInkstatJn(String inkstatJn) {
		this.inkstatJn = inkstatJn;
	}
	public String getuDesclarga() {
		return uDesclarga;
	}
	public void setuDesclarga(String uDesclarga) {
		this.uDesclarga = uDesclarga;
	}
	public String getuFechalimit() {
		return uFechalimit;
	}
	public void setuFechalimit(String uFechalimit) {
		this.uFechalimit = uFechalimit;
	}
	public String getuFechaAlta() {
		return uFechaAlta;
	}
	public void setuFechaAlta(String uFechaAlta) {
		this.uFechaAlta = uFechaAlta;
	}
	public String getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	public String getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

}
