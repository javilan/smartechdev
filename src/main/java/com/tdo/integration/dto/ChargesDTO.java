package com.tdo.integration.dto;

public class ChargesDTO {
	
	private String LastUpdatedBy;

    private String PricePeriodicityCode;

    private double BasePrice;

    private String TierHeaderId;

    private String CreationDate;

    private String ParentEntityId;

    private String CreatedBy;

    private String PricingChargeDefinition;

    private String PricingChargeDefinitionCode;

    private String CalculationMethodCode;

    private String AllowManualAdjustment;

    private String PricingChargeDefinitionId;

    private String PriceListChargeId;

    private String ChargeLineNumber;

    private String CalculationTypeCode;

    private String EndDate;

    private String StartDate;

    private String PricePeriodicity;

    private String CalculationMethod;

    private String CalculationType;

    private String PriceListId;

    private String MatrixId;

    private String CostCalculationAmount;

    private String LastUpdateDate;

	public String getLastUpdatedBy() {
		return LastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		LastUpdatedBy = lastUpdatedBy;
	}

	public String getPricePeriodicityCode() {
		return PricePeriodicityCode;
	}

	public void setPricePeriodicityCode(String pricePeriodicityCode) {
		PricePeriodicityCode = pricePeriodicityCode;
	}

	public double getBasePrice() {
		return BasePrice;
	}

	public void setBasePrice(double basePrice) {
		BasePrice = basePrice;
	}

	public String getTierHeaderId() {
		return TierHeaderId;
	}

	public void setTierHeaderId(String tierHeaderId) {
		TierHeaderId = tierHeaderId;
	}

	public String getCreationDate() {
		return CreationDate;
	}

	public void setCreationDate(String creationDate) {
		CreationDate = creationDate;
	}

	public String getParentEntityId() {
		return ParentEntityId;
	}

	public void setParentEntityId(String parentEntityId) {
		ParentEntityId = parentEntityId;
	}

	public String getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(String createdBy) {
		CreatedBy = createdBy;
	}

	public String getPricingChargeDefinition() {
		return PricingChargeDefinition;
	}

	public void setPricingChargeDefinition(String pricingChargeDefinition) {
		PricingChargeDefinition = pricingChargeDefinition;
	}

	public String getPricingChargeDefinitionCode() {
		return PricingChargeDefinitionCode;
	}

	public void setPricingChargeDefinitionCode(String pricingChargeDefinitionCode) {
		PricingChargeDefinitionCode = pricingChargeDefinitionCode;
	}

	public String getCalculationMethodCode() {
		return CalculationMethodCode;
	}

	public void setCalculationMethodCode(String calculationMethodCode) {
		CalculationMethodCode = calculationMethodCode;
	}

	public String getAllowManualAdjustment() {
		return AllowManualAdjustment;
	}

	public void setAllowManualAdjustment(String allowManualAdjustment) {
		AllowManualAdjustment = allowManualAdjustment;
	}

	public String getPricingChargeDefinitionId() {
		return PricingChargeDefinitionId;
	}

	public void setPricingChargeDefinitionId(String pricingChargeDefinitionId) {
		PricingChargeDefinitionId = pricingChargeDefinitionId;
	}

	public String getPriceListChargeId() {
		return PriceListChargeId;
	}

	public void setPriceListChargeId(String priceListChargeId) {
		PriceListChargeId = priceListChargeId;
	}

	public String getChargeLineNumber() {
		return ChargeLineNumber;
	}

	public void setChargeLineNumber(String chargeLineNumber) {
		ChargeLineNumber = chargeLineNumber;
	}

	public String getCalculationTypeCode() {
		return CalculationTypeCode;
	}

	public void setCalculationTypeCode(String calculationTypeCode) {
		CalculationTypeCode = calculationTypeCode;
	}


	public String getEndDate() {
		return EndDate;
	}

	public void setEndDate(String endDate) {
		EndDate = endDate;
	}

	public String getStartDate() {
		return StartDate;
	}

	public void setStartDate(String startDate) {
		StartDate = startDate;
	}

	public String getPricePeriodicity() {
		return PricePeriodicity;
	}

	public void setPricePeriodicity(String pricePeriodicity) {
		PricePeriodicity = pricePeriodicity;
	}

	public String getCalculationMethod() {
		return CalculationMethod;
	}

	public void setCalculationMethod(String calculationMethod) {
		CalculationMethod = calculationMethod;
	}

	public String getCalculationType() {
		return CalculationType;
	}

	public void setCalculationType(String calculationType) {
		CalculationType = calculationType;
	}

	public String getPriceListId() {
		return PriceListId;
	}

	public void setPriceListId(String priceListId) {
		PriceListId = priceListId;
	}

	public String getMatrixId() {
		return MatrixId;
	}

	public void setMatrixId(String matrixId) {
		MatrixId = matrixId;
	}

	public String getCostCalculationAmount() {
		return CostCalculationAmount;
	}

	public void setCostCalculationAmount(String costCalculationAmount) {
		CostCalculationAmount = costCalculationAmount;
	}

	public String getLastUpdateDate() {
		return LastUpdateDate;
	}

	public void setLastUpdateDate(String lastUpdateDate) {
		LastUpdateDate = lastUpdateDate;
	}

  
}
			

