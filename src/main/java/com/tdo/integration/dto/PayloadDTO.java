package com.tdo.integration.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PayloadDTO {

	@JsonProperty("TransactionDate")
	private String TransactionDate;
	
	@JsonProperty("PayloadIdPOS")
	private String PayloadIdPOS;
	
	@JsonProperty("VentaIdPOS")
	private String VentaIdPOS;
	
	@JsonProperty("AccountNumber")
	private String AccountNumber;
	
	@JsonProperty("SiteNumber")
	private String SiteNumber;
	
	@JsonProperty("CurrencyCode")
	private String CurrencyCode;
	
	@JsonProperty("FormaDePago")
	private String FormaDePago;
	
	@JsonProperty("TotalAPagar")
	private int TotalAPagar;
	
	@JsonProperty("Deposito")
	private int Deposito;

	public String getTransactionDate() {
		return TransactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		TransactionDate = transactionDate;
	}

	public String getPayloadIdPOS() {
		return PayloadIdPOS;
	}

	public void setPayloadIdPOS(String payloadIdPOS) {
		PayloadIdPOS = payloadIdPOS;
	}

	public String getVentaIdPOS() {
		return VentaIdPOS;
	}

	public void setVentaIdPOS(String ventaIdPOS) {
		VentaIdPOS = ventaIdPOS;
	}

	public String getAccountNumber() {
		return AccountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		AccountNumber = accountNumber;
	}

	public String getSiteNumber() {
		return SiteNumber;
	}

	public void setSiteNumber(String siteNumber) {
		SiteNumber = siteNumber;
	}

	public String getCurrencyCode() {
		return CurrencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		CurrencyCode = currencyCode;
	}

	public String getFormaDePago() {
		return FormaDePago;
	}

	public void setFormaDePago(String formaDePago) {
		FormaDePago = formaDePago;
	}

	public int getTotalAPagar() {
		return TotalAPagar;
	}

	public void setTotalAPagar(int totalAPagar) {
		TotalAPagar = totalAPagar;
	}

	public int getDeposito() {
		return Deposito;
	}

	public void setDeposito(int deposito) {
		Deposito = deposito;
	}
	
	
}
