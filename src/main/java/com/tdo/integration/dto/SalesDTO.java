package com.tdo.integration.dto;

import java.util.List;

import com.tdo.integration.data.model.tdo.CancelacionVenta;
import com.tdo.integration.data.model.tdo.MovimientosCaja;
import com.tdo.integration.data.model.tdo.Sales;

public class SalesDTO {
	
	private static final long serialVersionUID = 1L;
	List<Sales> sales;
	List<CancelacionVenta> cancelaciones;
	List<Sales> ventasCanceladas;
	List<MovimientosCaja> movimientosCaja;
	
	public List<Sales> getSales() {
		return sales;
	}
	public void setSales(List<Sales> sales) {
		this.sales = sales;
	}
	public List<CancelacionVenta> getCancelaciones() {
		return cancelaciones;
	}
	public void setCancelaciones(List<CancelacionVenta> cancelaciones) {
		this.cancelaciones = cancelaciones;
	}
	public List<Sales> getVentasCanceladas() {
		return ventasCanceladas;
	}
	public void setVentasCanceladas(List<Sales> ventasCanceladas) {
		this.ventasCanceladas = ventasCanceladas;
	}
	public List<MovimientosCaja> getMovimientosCaja() {
		return movimientosCaja;
	}
	public void setMovimientosCaja(List<MovimientosCaja> movimientosCaja) {
		this.movimientosCaja = movimientosCaja;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

}
