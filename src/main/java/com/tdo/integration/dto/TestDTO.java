package com.tdo.integration.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TestDTO {
	
	@JsonProperty("address1")
	private String address1;

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

}
