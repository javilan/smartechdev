package com.tdo.integration.dto;

public class RAInterfaceLinesAllSO {
	
	//campos Complementarios---------------------------------------------
		private String _transactionBatchSourceName = "";
		private String _transactionTypeName = "";
		private String _paymentTerms = "";
		private String _transactionDate = "";
		private String _accountingDate = "";
		private String _transactionNumber = "";
		private String _BilltoCustomerAccountNumber = "";
		private String _BilltoCustomerSiteNumber = "";
		private String _transactionLineType = "";
		private String _transactionLineDescription = "";
		private String _currencyCode = "";
		private String _currencyConversionType = "";
		private String _currencyConversionDate = "";
		private String _currencyConversionRate = "";
		private String _transactionLineAmount = "";
		private String _transactionLineQuantity = "";
		private String _unitSellingPrice = "";
		private String _lineTransactionsFlexfieldContext = "";
		private String _lineTransactionsFlexfieldSegment1 = "";
		private String _lineTransactionsFlexfieldSegment2 = "";
		private String _lineTransactionsFlexfieldSegment3 = "";
		private String _taxClassificationCode = "";
		private String _salesOrderNumber = "";
		private String _unitofMeasureCode = "";
		private String _referenceTransactionsFlexfieldContext = "";
		private String _referenceTransactionsFlexfieldSegment1 = "";
		private String _referenceTransactionsFlexfieldSegment2 = "";
		private String _referenceTransactionsFlexfieldSegment3 = "";
		private String _inventoryItemNumber = "";
		private String _overrideAutoAccountingFlag = "";
		private String _businessUnitName = "";
		
		
		public String get_transactionBatchSourceName() {
			return _transactionBatchSourceName;
		}
		public void set_transactionBatchSourceName(String _transactionBatchSourceName) {
			this._transactionBatchSourceName = _transactionBatchSourceName;
		}
		public String get_transactionTypeName() {
			return _transactionTypeName;
		}
		public void set_transactionTypeName(String _transactionTypeName) {
			this._transactionTypeName = _transactionTypeName;
		}
		public String get_paymentTerms() {
			return _paymentTerms;
		}
		public void set_paymentTerms(String _paymentTerms) {
			this._paymentTerms = _paymentTerms;
		}
		public String get_transactionDate() {
			return _transactionDate;
		}
		public void set_transactionDate(String _transactionDate) {
			this._transactionDate = _transactionDate;
		}
		public String get_accountingDate() {
			return _accountingDate;
		}
		public void set_accountingDate(String _accountingDate) {
			this._accountingDate = _accountingDate;
		}
		public String get_transactionNumber() {
			return _transactionNumber;
		}
		public void set_transactionNumber(String _transactionNumber) {
			this._transactionNumber = _transactionNumber;
		}
		public String get_BilltoCustomerAccountNumber() {
			return _BilltoCustomerAccountNumber;
		}
		public void set_BilltoCustomerAccountNumber(String _BilltoCustomerAccountNumber) {
			this._BilltoCustomerAccountNumber = _BilltoCustomerAccountNumber;
		}
		public String get_BilltoCustomerSiteNumber() {
			return _BilltoCustomerSiteNumber;
		}
		public void set_BilltoCustomerSiteNumber(String _BilltoCustomerSiteNumber) {
			this._BilltoCustomerSiteNumber = _BilltoCustomerSiteNumber;
		}
		public String get_transactionLineType() {
			return _transactionLineType;
		}
		public void set_transactionLineType(String _transactionLineType) {
			this._transactionLineType = _transactionLineType;
		}
		public String get_transactionLineDescription() {
			return _transactionLineDescription;
		}
		public void set_transactionLineDescription(String _transactionLineDescription) {
			this._transactionLineDescription = _transactionLineDescription;
		}
		public String get_currencyCode() {
			return _currencyCode;
		}
		public void set_currencyCode(String _currencyCode) {
			this._currencyCode = _currencyCode;
		}
		public String get_currencyConversionType() {
			return _currencyConversionType;
		}
		public void set_currencyConversionType(String _currencyConversionType) {
			this._currencyConversionType = _currencyConversionType;
		}
		public String get_currencyConversionDate() {
			return _currencyConversionDate;
		}
		public void set_currencyConversionDate(String _currencyConversionDate) {
			this._currencyConversionDate = _currencyConversionDate;
		}
		public String get_currencyConversionRate() {
			return _currencyConversionRate;
		}
		public void set_currencyConversionRate(String _currencyConversionRate) {
			this._currencyConversionRate = _currencyConversionRate;
		}
		public String get_transactionLineAmount() {
			return _transactionLineAmount;
		}
		public void set_transactionLineAmount(String _transactionLineAmount) {
			this._transactionLineAmount = _transactionLineAmount;
		}
		public String get_transactionLineQuantity() {
			return _transactionLineQuantity;
		}
		public void set_transactionLineQuantity(String _transactionLineQuantity) {
			this._transactionLineQuantity = _transactionLineQuantity;
		}
		public String get_unitSellingPrice() {
			return _unitSellingPrice;
		}
		public void set_unitSellingPrice(String _unitSellingPrice) {
			this._unitSellingPrice = _unitSellingPrice;
		}
		public String get_lineTransactionsFlexfieldContext() {
			return _lineTransactionsFlexfieldContext;
		}
		public void set_lineTransactionsFlexfieldContext(String _lineTransactionsFlexfieldContext) {
			this._lineTransactionsFlexfieldContext = _lineTransactionsFlexfieldContext;
		}
		public String get_lineTransactionsFlexfieldSegment1() {
			return _lineTransactionsFlexfieldSegment1;
		}
		public void set_lineTransactionsFlexfieldSegment1(String _lineTransactionsFlexfieldSegment1) {
			this._lineTransactionsFlexfieldSegment1 = _lineTransactionsFlexfieldSegment1;
		}
		public String get_lineTransactionsFlexfieldSegment2() {
			return _lineTransactionsFlexfieldSegment2;
		}
		public void set_lineTransactionsFlexfieldSegment2(String _lineTransactionsFlexfieldSegment2) {
			this._lineTransactionsFlexfieldSegment2 = _lineTransactionsFlexfieldSegment2;
		}
		public String get_lineTransactionsFlexfieldSegment3() {
			return _lineTransactionsFlexfieldSegment3;
		}
		public void set_lineTransactionsFlexfieldSegment3(String _lineTransactionsFlexfieldSegment3) {
			this._lineTransactionsFlexfieldSegment3 = _lineTransactionsFlexfieldSegment3;
		}
		public String get_taxClassificationCode() {
			return _taxClassificationCode;
		}
		public void set_taxClassificationCode(String _taxClassificationCode) {
			this._taxClassificationCode = _taxClassificationCode;
		}
		public String get_unitofMeasureCode() {
			return _unitofMeasureCode;
		}
		public void set_unitofMeasureCode(String _unitofMeasureCode) {
			this._unitofMeasureCode = _unitofMeasureCode;
		}
		public String get_referenceTransactionsFlexfieldContext() {
			return _referenceTransactionsFlexfieldContext;
		}
		public void set_referenceTransactionsFlexfieldContext(String _referenceTransactionsFlexfieldContext) {
			this._referenceTransactionsFlexfieldContext = _referenceTransactionsFlexfieldContext;
		}
		public String get_referenceTransactionsFlexfieldSegment1() {
			return _referenceTransactionsFlexfieldSegment1;
		}
		public void set_referenceTransactionsFlexfieldSegment1(String _referenceTransactionsFlexfieldSegment1) {
			this._referenceTransactionsFlexfieldSegment1 = _referenceTransactionsFlexfieldSegment1;
		}
		public String get_referenceTransactionsFlexfieldSegment2() {
			return _referenceTransactionsFlexfieldSegment2;
		}
		public void set_referenceTransactionsFlexfieldSegment2(String _referenceTransactionsFlexfieldSegment2) {
			this._referenceTransactionsFlexfieldSegment2 = _referenceTransactionsFlexfieldSegment2;
		}
		public String get_referenceTransactionsFlexfieldSegment3() {
			return _referenceTransactionsFlexfieldSegment3;
		}
		public void set_referenceTransactionsFlexfieldSegment3(String _referenceTransactionsFlexfieldSegment3) {
			this._referenceTransactionsFlexfieldSegment3 = _referenceTransactionsFlexfieldSegment3;
		}
		public String get_inventoryItemNumber() {
			return _inventoryItemNumber;
		}
		public void set_inventoryItemNumber(String _inventoryItemNumber) {
			this._inventoryItemNumber = _inventoryItemNumber;
		}
		public String get_overrideAutoAccountingFlag() {
			return _overrideAutoAccountingFlag;
		}
		public void set_overrideAutoAccountingFlag(String _overrideAutoAccountingFlag) {
			this._overrideAutoAccountingFlag = _overrideAutoAccountingFlag;
		}
		public String get_businessUnitName() {
			return _businessUnitName;
		}
		public void set_businessUnitName(String _businessUnitName) {
			this._businessUnitName = _businessUnitName;
		}
		public String get_salesOrderNumber() {
			return _salesOrderNumber;
		}
		public void set_salesOrderNumber(String _salesOrderNumber) {
			this._salesOrderNumber = _salesOrderNumber;
		}
		@Override
		public String toString() {
			return "RAInterfaceLinesAllSO [_transactionBatchSourceName=" + _transactionBatchSourceName
					+ ", _transactionTypeName=" + _transactionTypeName + ", _paymentTerms=" + _paymentTerms
					+ ", _transactionDate=" + _transactionDate + ", _accountingDate=" + _accountingDate
					+ ", _transactionNumber=" + _transactionNumber + ", _BilltoCustomerAccountNumber="
					+ _BilltoCustomerAccountNumber + ", _BilltoCustomerSiteNumber=" + _BilltoCustomerSiteNumber
					+ ", _transactionLineType=" + _transactionLineType + ", _transactionLineDescription="
					+ _transactionLineDescription + ", _currencyCode=" + _currencyCode + ", _currencyConversionType="
					+ _currencyConversionType + ", _currencyConversionDate=" + _currencyConversionDate
					+ ", _currencyConversionRate=" + _currencyConversionRate + ", _transactionLineAmount="
					+ _transactionLineAmount + ", _transactionLineQuantity=" + _transactionLineQuantity
					+ ", _unitSellingPrice=" + _unitSellingPrice + ", _lineTransactionsFlexfieldContext="
					+ _lineTransactionsFlexfieldContext + ", _lineTransactionsFlexfieldSegment1="
					+ _lineTransactionsFlexfieldSegment1 + ", _lineTransactionsFlexfieldSegment2="
					+ _lineTransactionsFlexfieldSegment2 + ", _lineTransactionsFlexfieldSegment3="
					+ _lineTransactionsFlexfieldSegment3 + ", _taxClassificationCode=" + _taxClassificationCode
					+ ", _salesOrderNumber=" + _salesOrderNumber + ", _unitofMeasureCode=" + _unitofMeasureCode
					+ ", _referenceTransactionsFlexfieldContext=" + _referenceTransactionsFlexfieldContext
					+ ", _referenceTransactionsFlexfieldSegment1=" + _referenceTransactionsFlexfieldSegment1
					+ ", _referenceTransactionsFlexfieldSegment2=" + _referenceTransactionsFlexfieldSegment2
					+ ", _referenceTransactionsFlexfieldSegment3=" + _referenceTransactionsFlexfieldSegment3
					+ ", _inventoryItemNumber=" + _inventoryItemNumber + ", _overrideAutoAccountingFlag="
					+ _overrideAutoAccountingFlag + ", _businessUnitName=" + _businessUnitName + "]";
		}
		
		
		
	//--------------------------------------------------------------------
	
}
