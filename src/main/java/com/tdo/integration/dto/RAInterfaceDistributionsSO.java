package com.tdo.integration.dto;

public class RAInterfaceDistributionsSO {

	private String _accountClass = "";
	private String _amount = "";
	private String _percent = "";
	private String _lineTransactionsFlexfieldContext = "";
	private String _lineTransactionsFlexfieldSegment1 = "";
	private String _lineTransactionsFlexfieldSegment2 = "";
	private String _lineTransactionsFlexfieldSegment3 = "";
	private String _accountingFlexfieldSegment1 = ""; 
	private String _accountingFlexfieldSegment2 = "";
	private String _accountingFlexfieldSegment3 = "";
	private String _accountingFlexfieldSegment4 = "";
	private String _accountingFlexfieldSegment5 = "";
	private String _accountingFlexfieldSegment6 = "";
	private String _accountingFlexfieldSegment7 = "";
	private String _businessUnitName = "";
	
	public String get_accountClass() {
		return _accountClass;
	}
	public void set_accountClass(String _accountClass) {
		this._accountClass = _accountClass;
	}
	public String get_amount() {
		return _amount;
	}
	public void set_amount(String _amount) {
		this._amount = _amount;
	}
	public String get_percent() {
		return _percent;
	}
	public void set_percent(String _percent) {
		this._percent = _percent;
	}
	public String get_lineTransactionsFlexfieldContext() {
		return _lineTransactionsFlexfieldContext;
	}
	public void set_lineTransactionsFlexfieldContext(String _lineTransactionsFlexfieldContext) {
		this._lineTransactionsFlexfieldContext = _lineTransactionsFlexfieldContext;
	}
	public String get_lineTransactionsFlexfieldSegment1() {
		return _lineTransactionsFlexfieldSegment1;
	}
	public void set_lineTransactionsFlexfieldSegment1(String _lineTransactionsFlexfieldSegment1) {
		this._lineTransactionsFlexfieldSegment1 = _lineTransactionsFlexfieldSegment1;
	}
	public String get_lineTransactionsFlexfieldSegment2() {
		return _lineTransactionsFlexfieldSegment2;
	}
	public void set_lineTransactionsFlexfieldSegment2(String _lineTransactionsFlexfieldSegment2) {
		this._lineTransactionsFlexfieldSegment2 = _lineTransactionsFlexfieldSegment2;
	}
	public String get_lineTransactionsFlexfieldSegment3() {
		return _lineTransactionsFlexfieldSegment3;
	}
	public void set_lineTransactionsFlexfieldSegment3(String _lineTransactionsFlexfieldSegment3) {
		this._lineTransactionsFlexfieldSegment3 = _lineTransactionsFlexfieldSegment3;
	}
	public String get_accountingFlexfieldSegment1() {
		return _accountingFlexfieldSegment1;
	}
	public void set_accountingFlexfieldSegment1(String _accountingFlexfieldSegment1) {
		this._accountingFlexfieldSegment1 = _accountingFlexfieldSegment1;
	}
	public String get_accountingFlexfieldSegment2() {
		return _accountingFlexfieldSegment2;
	}
	public void set_accountingFlexfieldSegment2(String _accountingFlexfieldSegment2) {
		this._accountingFlexfieldSegment2 = _accountingFlexfieldSegment2;
	}
	public String get_accountingFlexfieldSegment3() {
		return _accountingFlexfieldSegment3;
	}
	public void set_accountingFlexfieldSegment3(String _accountingFlexfieldSegment3) {
		this._accountingFlexfieldSegment3 = _accountingFlexfieldSegment3;
	}
	public String get_accountingFlexfieldSegment4() {
		return _accountingFlexfieldSegment4;
	}
	public void set_accountingFlexfieldSegment4(String _accountingFlexfieldSegment4) {
		this._accountingFlexfieldSegment4 = _accountingFlexfieldSegment4;
	}
	public String get_accountingFlexfieldSegment5() {
		return _accountingFlexfieldSegment5;
	}
	public void set_accountingFlexfieldSegment5(String _accountingFlexfieldSegment5) {
		this._accountingFlexfieldSegment5 = _accountingFlexfieldSegment5;
	}
	public String get_accountingFlexfieldSegment6() {
		return _accountingFlexfieldSegment6;
	}
	public void set_accountingFlexfieldSegment6(String _accountingFlexfieldSegment6) {
		this._accountingFlexfieldSegment6 = _accountingFlexfieldSegment6;
	}
	public String get_accountingFlexfieldSegment7() {
		return _accountingFlexfieldSegment7;
	}
	public void set_accountingFlexfieldSegment7(String _accountingFlexfieldSegment7) {
		this._accountingFlexfieldSegment7 = _accountingFlexfieldSegment7;
	}
	public String get_businessUnitName() {
		return _businessUnitName;
	}
	public void set_businessUnitName(String _businessUnitName) {
		this._businessUnitName = _businessUnitName;
	}
	
	@Override
	public String toString() {
		return "RAInterfaceDistributionsSO [_accountClass=" + _accountClass + ", _amount=" + _amount + ", _percent="
				+ _percent + ", _lineTransactionsFlexfieldContext=" + _lineTransactionsFlexfieldContext
				+ ", _lineTransactionsFlexfieldSegment1=" + _lineTransactionsFlexfieldSegment1
				+ ", _lineTransactionsFlexfieldSegment2=" + _lineTransactionsFlexfieldSegment2
				+ ", _lineTransactionsFlexfieldSegment3=" + _lineTransactionsFlexfieldSegment3
				+ ", _accountingFlexfieldSegment1=" + _accountingFlexfieldSegment1 + ", _accountingFlexfieldSegment2="
				+ _accountingFlexfieldSegment2 + ", _accountingFlexfieldSegment3=" + _accountingFlexfieldSegment3
				+ ", _accountingFlexfieldSegment4=" + _accountingFlexfieldSegment4 + ", _accountingFlexfieldSegment5="
				+ _accountingFlexfieldSegment5 + ", _accountingFlexfieldSegment6=" + _accountingFlexfieldSegment6
				+ ", _accountingFlexfieldSegment7=" + _accountingFlexfieldSegment7 + ", _businessUnitName="
				+ _businessUnitName + "]";
	}

	
	
}
