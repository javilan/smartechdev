package com.tdo.integration.dto;

import java.util.Date;
import java.util.List;

import org.exolab.castor.types.DateTime;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SalesOrderDTO {
	
	@JsonProperty("BusinessUnit")
	private String BusinessUnit;
	
	@JsonProperty("TransactionDate")
	private String TransactionDate;
	
	@JsonProperty("VentaIdPOS")
	private String VentaIdPOS;
	
	@JsonProperty("AccountNumber")
	private String AccountNumber;
	
	@JsonProperty("SiteNumber")
	private String SiteNumber;
	
	@JsonProperty("CurrencyCode")
	private String CurrencyCode;
	
	@JsonProperty("TipoVenta")
	private String TipoVenta;
	
	@JsonProperty("Factura")	// Y N
	private char Factura;

	@JsonProperty("NCIdRefVenta")
	private String NCIdRefVenta;
	
	@JsonProperty("OrganizationName")
	private String OrganizationName;
	
	@JsonProperty("SalesLines")
	private List<SalesLinesTDO> SalesLines;
	
	//----------------------------------------------------------
	private List<RAInterfaceLinesAllSO> interLines;
	private List<RAInterfaceDistributionsSO> interDistr;
	
	public List<RAInterfaceLinesAllSO> getInterLines() {
		return interLines;
	}

	public void setInterLines(List<RAInterfaceLinesAllSO> interLines) {
		this.interLines = interLines;
	}

	public List<RAInterfaceDistributionsSO> getInterDistr() {
		return interDistr;
	}

	public void setInterDistr(List<RAInterfaceDistributionsSO> interDistr) {
		this.interDistr = interDistr;
	}
	
	//---------------------------------------------------------

	public String getBusinessUnit() {
		return BusinessUnit;
	}

	public void setBusinessUnit(String businessUnit) {
		BusinessUnit = businessUnit;
	}

	public String getTransactionDate() {
		return TransactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		TransactionDate = transactionDate;
	}

	public String getVentaIdPOS() {
		return VentaIdPOS;
	}

	public void setVentaIdPOS(String ventaIdPOS) {
		VentaIdPOS = ventaIdPOS;
	}

	public String getAccountNumber() {
		return AccountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		AccountNumber = accountNumber;
	}

	public String getSiteNumber() {
		return SiteNumber;
	}

	public void setSiteNumber(String siteNumber) {
		SiteNumber = siteNumber;
	}

	public String getCurrencyCode() {
		return CurrencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		CurrencyCode = currencyCode;
	}

	public String getTipoVenta() {
		return TipoVenta;
	}

	public void setTipoVenta(String tipoVenta) {
		TipoVenta = tipoVenta;
	}

	public char getFactura() {
		return Factura;
	}

	public void setFactura(char factura) {
		Factura = factura;
	}

	public String getNCIdRefVenta() {
		return NCIdRefVenta;
	}

	public void setNCIdRefVenta(String nCIdRefVenta) {
		NCIdRefVenta = nCIdRefVenta;
	}

	public String getOrganizationName() {
		return OrganizationName;
	}

	public void setOrganizationName(String organizationName) {
		OrganizationName = organizationName;
	}

	public List<SalesLinesTDO> getSalesLines() {
		return SalesLines;
	}

	public void setSalesLines(List<SalesLinesTDO> salesLines) {
		SalesLines = salesLines;
	}
	

	@Override
	public String toString() {
		return "SalesOrderDTO [BusinessUnit=" + BusinessUnit + ", TransactionDate=" + TransactionDate + ", VentaIdPOS="
				+ VentaIdPOS + ", AccountNumber=" + AccountNumber + ", SiteNumber=" + SiteNumber + ", CurrencyCode="
				+ CurrencyCode + ", TipoVenta=" + TipoVenta + ", Factura=" + Factura + ", NCIdRefVenta=" + NCIdRefVenta
				+ ", OrganizationName=" + OrganizationName + ", SalesLines=" + SalesLines + "]";
	}
	
	

}
