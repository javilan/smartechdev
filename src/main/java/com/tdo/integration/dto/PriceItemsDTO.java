package com.tdo.integration.dto;

public class PriceItemsDTO {

	private String LineTypeCode;

    private String LastUpdatedBy;

    private String CreationDate;

    private String PrimaryPricingUOM;

    private String CreatedBy;

    private String InventoryItemId;

    private String PriceListItemId;

    private String PricingUOMCode;

    private String ItemDescription;

    private String Item;

    private String PricingUOM;

    private String ItemLevel;

    private ChargesDTO[] charges;

    private String ItemLevelCode;

    private String ServiceDurationPeriodCode;

    private String PriceListId;

    private String ServiceDuration;

    private String LineType;

    private String ServiceDurationPeriod;

    private String LastUpdateDate;

	public String getLineTypeCode() {
		return LineTypeCode;
	}

	public void setLineTypeCode(String lineTypeCode) {
		LineTypeCode = lineTypeCode;
	}

	public String getLastUpdatedBy() {
		return LastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		LastUpdatedBy = lastUpdatedBy;
	}

	public String getCreationDate() {
		return CreationDate;
	}

	public void setCreationDate(String creationDate) {
		CreationDate = creationDate;
	}

	public String getPrimaryPricingUOM() {
		return PrimaryPricingUOM;
	}

	public void setPrimaryPricingUOM(String primaryPricingUOM) {
		PrimaryPricingUOM = primaryPricingUOM;
	}

	public String getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(String createdBy) {
		CreatedBy = createdBy;
	}

	public String getInventoryItemId() {
		return InventoryItemId;
	}

	public void setInventoryItemId(String inventoryItemId) {
		InventoryItemId = inventoryItemId;
	}

	public String getPriceListItemId() {
		return PriceListItemId;
	}

	public void setPriceListItemId(String priceListItemId) {
		PriceListItemId = priceListItemId;
	}

	public String getPricingUOMCode() {
		return PricingUOMCode;
	}

	public void setPricingUOMCode(String pricingUOMCode) {
		PricingUOMCode = pricingUOMCode;
	}

	public String getItemDescription() {
		return ItemDescription;
	}

	public void setItemDescription(String itemDescription) {
		ItemDescription = itemDescription;
	}

	public String getItem() {
		return Item;
	}

	public void setItem(String item) {
		Item = item;
	}

	public String getPricingUOM() {
		return PricingUOM;
	}

	public void setPricingUOM(String pricingUOM) {
		PricingUOM = pricingUOM;
	}

	public String getItemLevel() {
		return ItemLevel;
	}

	public void setItemLevel(String itemLevel) {
		ItemLevel = itemLevel;
	}

	public ChargesDTO[] getCharges() {
		return charges;
	}

	public void setCharges(ChargesDTO[] charges) {
		this.charges = charges;
	}

	public String getItemLevelCode() {
		return ItemLevelCode;
	}

	public void setItemLevelCode(String itemLevelCode) {
		ItemLevelCode = itemLevelCode;
	}

	public String getServiceDurationPeriodCode() {
		return ServiceDurationPeriodCode;
	}

	public void setServiceDurationPeriodCode(String serviceDurationPeriodCode) {
		ServiceDurationPeriodCode = serviceDurationPeriodCode;
	}

	public String getPriceListId() {
		return PriceListId;
	}

	public void setPriceListId(String priceListId) {
		PriceListId = priceListId;
	}

	public String getServiceDuration() {
		return ServiceDuration;
	}

	public void setServiceDuration(String serviceDuration) {
		ServiceDuration = serviceDuration;
	}

	public String getLineType() {
		return LineType;
	}

	public void setLineType(String lineType) {
		LineType = lineType;
	}

	public String getServiceDurationPeriod() {
		return ServiceDurationPeriod;
	}

	public void setServiceDurationPeriod(String serviceDurationPeriod) {
		ServiceDurationPeriod = serviceDurationPeriod;
	}

	public String getLastUpdateDate() {
		return LastUpdateDate;
	}

	public void setLastUpdateDate(String lastUpdateDate) {
		LastUpdateDate = lastUpdateDate;
	}

   
}
