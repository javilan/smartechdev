package com.tdo.integration.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Devolucion {
	
	@JsonProperty("RefVentaIdPOS")
	private String RefVentaIdPOS;
	
	@JsonProperty("RefLineId")
	private String RefLineId;

	public String getRefVentaIdPOS() {
		return RefVentaIdPOS;
	}

	public void setRefVentaIdPOS(String refVentaIdPOS) {
		RefVentaIdPOS = refVentaIdPOS;
	}

	public String getRefLineId() {
		return RefLineId;
	}

	public void setRefLineId(String refLineId) {
		RefLineId = refLineId;
	}
	
	
}
