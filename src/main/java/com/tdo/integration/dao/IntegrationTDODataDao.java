package com.tdo.integration.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;
import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import com.tdo.integration.data.model.tdo.Abono;
import com.tdo.integration.data.model.tdo.CancelacionVenta;
import com.tdo.integration.data.model.tdo.Cliente;
import com.tdo.integration.data.model.tdo.Credito;
import com.tdo.integration.data.model.tdo.CustomerAdditionalSpecs;
import com.tdo.integration.data.model.tdo.MovimientosCaja;
import com.tdo.integration.data.model.tdo.Sales;
import com.tdo.integration.data.model.tdo.Udc;
import com.tdo.integration.dto.CreditDTO;
import com.tdo.integration.dto.SalesDTO;

@Repository("integrationTDODataDao")
@Transactional 
public class IntegrationTDODataDao {
	
	DataSource dataSource;
	Connection conn = null;
    private SessionFactory sessionFactory;
    private final int ID_CLIENTE_VENTA_PUBLICO = 1;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	public void setDataSource(DataSource dataSource) {
		try {
			this.dataSource = dataSource;
			this.conn = dataSource.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public CustomerAdditionalSpecs saveCustomerAdditionalSpecs(CustomerAdditionalSpecs specs) throws Exception{
		try {
			Session session = this.sessionFactory.getCurrentSession();
			session.saveOrUpdate(specs);
			return specs;
		}catch(Exception e) {
			throw e;
		}
	
	}

	public boolean saveOrUpdateCustomer(Cliente cliente) {
		try {
			
			Cliente clienteCDB = new Cliente();
			Session session = sessionFactory.getCurrentSession();
			
			if(cliente.getClienteId().intValue() == ID_CLIENTE_VENTA_PUBLICO) {
				cliente.setClienteNubeId(new BigInteger("0"));
			}
			
			clienteCDB = (Cliente)session.byNaturalId(Cliente.class).using("clienteNubeId", cliente.getClienteNubeId()).load();
			
			if(clienteCDB == null) {
				//En la alta del cliente el Limite de Credito Absoluto es el Limite de Credito que viene por primera vez.
				cliente.setClienteLimiteAbsoluto(cliente.getClienteLimite());
				session.save(cliente);
			} else {
				//Se recuperan los valores que se encuentran unicamente en la BD centralizada.
				cliente.setClienteLimiteAbsoluto(clienteCDB.getClienteLimiteAbsoluto());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	@SuppressWarnings({"unchecked" })
	public boolean saveSales(SalesDTO sales){
		try {
			Session session = sessionFactory.getCurrentSession();
			
			List<Sales> sls = sales.getSales();
			
			if(sls != null) {
				Sales s = sls.get(0);
				Criteria criteria = session.createCriteria(Sales.class);
				criteria.add(Restrictions.eq("idResumenCaja", s.getIdResumenCaja()));
				criteria.add(Restrictions.eq("sucursal", s.getSucursal()));
				List<Sales> list = criteria.list();
				if(list != null) {
					if(!list.isEmpty()) {
						return true;
					}
				}
				

				for(Sales sale : sales.getSales()) {
					session.saveOrUpdate(sale);
				}
				
			}
						
			if(sales.getMovimientosCaja() != null) {
				for(MovimientosCaja mov : sales.getMovimientosCaja()) {
					session.saveOrUpdate(mov);
				}
			}

			if(sales.getCancelaciones() != null) {
				for(CancelacionVenta can : sales.getCancelaciones()) {
					session.saveOrUpdate(can);
				}
			}

			return true;
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	
	}
	

	public boolean saveOrUpdateCredit(List<CreditDTO> lstCreditoDTO) {
		try {			
			Session session = sessionFactory.getCurrentSession();
			
			if(lstCreditoDTO != null && lstCreditoDTO.size() > 0) {
				for(CreditDTO c : lstCreditoDTO) {
					
					this.saveOrUpdateCustomer(c.getCliente());
					this.deleteCreditByCustomer(c.getSucursal(), c.getCliente());
						
					if(c.getCreditos() != null && c.getCreditos().size() > 0) {
						for(Credito credito : c.getCreditos()) {	
							session.save(credito);
						}
					}
				}				
			}
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}	
	
	public boolean deleteCreditByCustomer(String sucursal, Cliente cliente) {
		
		try {		
			Session session = sessionFactory.getCurrentSession();
			
			String sql = String.format("delete Abono where sucursal = '%s' and clienteNubeId = %s", sucursal, cliente.getClienteNubeId());
			Query q = session.createQuery(sql);
			q.executeUpdate();
			
			sql = String.format("delete Credito where sucursal = '%s' and clienteNubeId = %s", sucursal, cliente.getClienteNubeId());
			q = session.createQuery(sql);
			q.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	} 
	
	public BigDecimal getCustomerCreditAmount(String sucursal, Cliente cliente) {
		BigDecimal creditoTotal = new BigDecimal(0);
		
		try {			
			Session session = sessionFactory.getCurrentSession();
			Criteria criteria = session.createCriteria(Credito.class);			
			if(!"".equals(sucursal)) {
				criteria.add(Restrictions.eq("sucursal", sucursal));
			}
			criteria.add(Restrictions.eq("clienteNubeId", cliente.getClienteNubeId()));
			criteria.add(Restrictions.ne("status", -1));
		    criteria.setProjection(Projections.sum("total"));
		    creditoTotal = (BigDecimal) criteria.uniqueResult();
		    
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return creditoTotal != null ? creditoTotal : new BigDecimal(0);
	}
	
	public BigDecimal getCustomerDebitAmount(String sucursal, Cliente cliente) {
		BigDecimal abonoTotal = new BigDecimal(0);
		
		try {			
			Session session = sessionFactory.getCurrentSession();			
			Criteria criteria = session.createCriteria(Abono.class);
			
			if(!"".equals(sucursal)) {
				criteria.add(Restrictions.eq("sucursal", sucursal));
			}
			criteria.add(Restrictions.eq("clienteNubeId", cliente.getClienteNubeId()));
		    criteria.setProjection(Projections.sum("abonoTotal"));
		    abonoTotal = (BigDecimal) criteria.uniqueResult();
		    
		} catch (Exception e) {
			e.printStackTrace();
		}
		return abonoTotal != null ? abonoTotal : new BigDecimal(0);
	}
	
	@SuppressWarnings("unchecked")
	public List<CreditDTO> getCustomerCreditByName(String value) {
		List<CreditDTO> listCreditDTO = new ArrayList<CreditDTO>();
		Session session = sessionFactory.getCurrentSession();
		
		try { 
			Criteria criteria = session.createCriteria(Cliente.class);
			criteria.add(Restrictions.ne("clienteNubeId", new BigInteger("0")));
//			criteria.add(Restrictions.ilike("clienteNombre", "%a%", MatchMode.ANYWHERE));
			List<Cliente> list = criteria.list();
			
//			Query queryObject = session.createQuery("from Cliente where clienteNombre like '%||:vBusca||%' and clienteNubeId > 0");
//			queryObject.setParameter("vBusca", value);
//			List<Cliente> list = queryObject.list();
			
			if(list != null && list.size() > 0) {
				CreditDTO creditDTO;
				for(Cliente c : list) {
					creditDTO = new CreditDTO();
					creditDTO.setCliente(c);
					listCreditDTO.add(creditDTO);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return listCreditDTO;
	}
	
	public boolean updateCreditLimit(CreditDTO creditDTO) {
		
		try {
			Session session = sessionFactory.getCurrentSession();
			
			String sql = String.format("update Cliente set clienteLimiteAbsoluto = %s where clienteNubeId = %s", creditDTO.getCliente().getClienteLimiteAbsoluto(), creditDTO.getCliente().getClienteNubeId());
			Query q = session.createQuery(sql);
			q.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * 
	 * @param udc
	 * @throws Exception
	 */
	public boolean saveOrUpdateUdc(Udc udc) throws Exception{
		try {
			Session session = this.sessionFactory.getCurrentSession();
			session.saveOrUpdate(udc);
			return true;

		}catch(Exception e) {
			throw e;
		}
		
	}
	
	public List<Udc> getUdcByUdcKey(String udcKey) {
		
			Session session = this.sessionFactory.getCurrentSession();
			Criteria cr = session.createCriteria(Udc.class);
			cr.add(Restrictions.eq("udckey",udcKey));

			List<Udc> results = cr.list();
			return results;
		}	
}
