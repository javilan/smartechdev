package com.tdo.integration.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tdo.integration.data.model.tdo.CustomerAdditionalSpecs;
import com.tdo.integration.data.model.tdo.Udc;
import com.tdo.integration.data.services.IntegrationCDBService;
import com.tdo.integration.data.services.IntegrationDataService;
import com.tdo.integration.data.services.IntegrationRestServiceTDO;
import com.tdo.integration.dto.ArticuloDTO;
import com.tdo.integration.dto.CreditDTO;
import com.tdo.integration.dto.CustomerDTO;
import com.tdo.integration.dto.GLInterfaceV3;
import com.tdo.integration.dto.InventoryTransactionDTO;
import com.tdo.integration.dto.InventoryTransactionV1;
import com.tdo.integration.dto.ItemMasterKauffmanDTO;
import com.tdo.integration.dto.RaInterface;
import com.tdo.integration.dto.SalesDTO;
import com.tdo.integration.pojo.BaseResponse;
import com.tdo.integration.pojo.ClientesJson;
import com.tdo.integration.util.AppConstants;
import com.tdo.integration.ws.SOAPCustomerService;
import com.tdo.integration.ws.SOAPInventoryTransactionsService;
import com.tdo.integration.ws.SOAPItemMasterService;
import com.tdo.integration.ws.RESTItemPriceService;
import com.tdo.integration.ws.SSLTestService;
import com.tdo.integration.ws.SOAPSalesService;
import com.tdo.integration.ws.SOAPPayloadsServices;
import com.tdo.integration.ws.SOAPInvTransactionsService;
import com.tdo.integration.dto.*;

@RestController
@RequestMapping("/rest")
public class IntegrationRestController {
	
	@Autowired
	ServletContext servletContext;
	
	@Autowired
	IntegrationRestServiceTDO integrationRestServiceTDO;
	
	@Autowired
	IntegrationDataService integrationDataService;
	
	@Autowired
	SOAPCustomerService SOAPCustomerService;
	
	@Autowired
	RESTItemPriceService RESTItemPriceService;
	
	@Autowired
	SOAPItemMasterService SOAPItemMasterService;
	
	@Autowired
	IntegrationCDBService integrationCDBService;
	
	@Autowired
	SOAPInventoryTransactionsService SOAPInventoryTransactionsService;
	
	@Autowired
	SSLTestService SSLTestService;
	
	@Autowired
	SOAPSalesService SOAPSalesService;
	
	@Autowired
	SOAPPayloadsServices SOAPPayloadsServices;
	
	@Autowired
	SOAPInvTransactionsService SOAPInvTransactionsService;
		
	@RequestMapping(value = "/inventory", method = RequestMethod.POST, consumes = "application/json; charset=utf-8")
	 public BaseResponse inventory(@RequestBody InventoryTransactionV1 request) {
		return integrationRestServiceTDO.processInventoryFile(request);
	}
	
	@RequestMapping(value = "/ar", method = RequestMethod.POST, consumes = "application/json; charset=utf-8")
	 public BaseResponse ar(@RequestBody RaInterface request) {
		return integrationRestServiceTDO.processARFile(request);
	}
	
	@RequestMapping(value = "/gl", method = RequestMethod.POST, consumes = "application/json; charset=utf-8")
	 public BaseResponse gl(@RequestBody List<GLInterfaceV3> request) {
		return integrationRestServiceTDO.processGlFile(request);
	}

	@RequestMapping(value = "/oracleCustomers", method = RequestMethod.GET)
	 public List<ClientesJson> oracleCustomers(@RequestParam String request) {
		if("".equals(request)) {
			return null;
		}
		return SOAPCustomerService.getCustomers(request);
	}
	
	@RequestMapping(value = "/createOracleCustomers", method = RequestMethod.POST, consumes = "application/json; charset=utf-8")
	 public List<CustomerDTO> createOracleCustomers(@RequestBody List<CustomerDTO> request,@RequestParam String entidad) {
		return SOAPCustomerService.insertCustomers(request);
	}

	@RequestMapping(value = "/getCustomerCreditByName", method = RequestMethod.POST, consumes = "application/json; charset=utf-8")
	 public List<CreditDTO> getCustomerCreditByName(@RequestBody String request) {
		return integrationCDBService.getCustomerCreditByName(request);
	}
	
	@RequestMapping(value = "/updateCustomerCreditLimit", method = RequestMethod.POST, consumes = "application/json; charset=utf-8")
	 public BaseResponse updateCustomerCreditLimit(@RequestBody CreditDTO request) {
		return integrationCDBService.updateCustomerCreditLimit(request);
	}
	
	@RequestMapping(value = "/updateCustomerCredit", method = RequestMethod.POST, consumes = "application/json; charset=utf-8")
	 public List<CreditDTO> updateCustomerCredit(@RequestBody List<CreditDTO> request) {
		return integrationCDBService.updateCustomerCredit(request);
	}
	
	@RequestMapping(value = "/oracleItems", method = RequestMethod.GET)
	 public List<ItemMasterKauffmanDTO> oracleItems(@RequestParam String entidad,
			 										@RequestParam String dateStart,
			 										@RequestParam String dateEnd,
			 										@RequestParam String type,
			                                        @RequestParam String start,
			 										@RequestParam String size) {
		if("".equals(dateStart) || "".equals(dateEnd) || "".equals(type) || "".equals(entidad)) {
			return null;
		}
		
		return SOAPItemMasterService.getItems(dateStart, dateEnd, start, size, type, entidad,"");
	}
	


	@RequestMapping(value = "/oracleItemPrice", method = RequestMethod.GET)
	 public List<PriceListKauffmanDTO> oracleItemPrice(@RequestParam String entidad,
			 										@RequestParam String dateStart,
			 										@RequestParam String dateEnd,
			 										@RequestParam String type,
			                                        @RequestParam String start,
			 										@RequestParam String size) {
		if("".equals(dateStart) || "".equals(dateEnd) || "".equals(type) || "".equals(entidad)) {
			return null;
		}
		
		return RESTItemPriceService.getItemPriceList(dateStart, dateEnd, start, size, type, entidad);
	}
	
		
	@RequestMapping(value = "/registraClienteCultivo", method = RequestMethod.POST)
	 public BaseResponse registraClienteCultivo(@RequestBody CustomerAdditionalSpecs specs) {
		return integrationRestServiceTDO.registerCustomerAdditionalSpecs(specs);	
	}
	
	@RequestMapping(value = "/salesService", method = RequestMethod.POST)
	 public BaseResponse salesService(@RequestBody SalesDTO sales) {
		return integrationCDBService.insertSales(sales);
	}
		
	
	@RequestMapping(value = "/registro", method = RequestMethod.POST)
	 public BaseResponse registro(@RequestBody String message) {
		
		JSONObject jsonObj=new JSONObject(message);
		String str = "*" + jsonObj.getString("message") + "*" + " Recibido en: " +  getCurrentTimeStamp();
		System.out.println(str);
		AppConstants.addResult(str);
		BaseResponse response = new BaseResponse();
		response.setContent("Su registro ha sido recibido");
		return response;
	}
		
	@RequestMapping(value = "/invTransfers", method = RequestMethod.GET)
	 public List<InventoryTransactionDTO> transfers(@RequestParam String request) {
		if("".equals(request)) {
			return null;
		}
		return SOAPInventoryTransactionsService.getInventoryTransactionTransfers(request);
	}
	
	@RequestMapping(value = "/avgRate", method = RequestMethod.GET)
	 public double getAvgRate(@RequestParam String request) {
		if("".equals(request)) {
			return 0;
		}
		return SOAPInventoryTransactionsService.getItemRateAverage(request);
	}
	
	@RequestMapping(value = "/usdItemCost", method = RequestMethod.GET)
	 public List<ArticuloDTO> oracleItems(@RequestBody List<ArticuloDTO> list) {
		return null;
	}
	
	
	public String getCurrentTimeStamp() {
	    return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date());
	}
	
	@RequestMapping(value ="/getUdcList", method = RequestMethod.GET)
	public List<Udc> getCurrentExchangeRate (@RequestParam String request) {
		
		if("".equals(request)) {
			return null;
		}
	return  integrationCDBService.getUdcByUdcKey(request);
	}
	
	@RequestMapping(value = "/registraUdc", method = RequestMethod.GET)
	 public BaseResponse registraUdc(@RequestParam String udcValue, @RequestParam String udcKey) throws Exception {
		
		List<Udc> udcList =integrationCDBService.getUdcByUdcKey(udcKey);
		Udc udc=null;
		if(udcList.size()>0) {
			udc = udcList.get(0);
			udc.setUdcValue(udcValue);
		}
		return integrationCDBService.saveOrUpdateUdc(udc);	
	}
	

	
	 @RequestMapping(value = "/home", method = RequestMethod.GET)
	 public String home(Model model) {
		
		String dateStart = "2019-07-18 20:00:00";
		String dateEnd = "2020-07-01 17:00:00";
		String type = "N";
		String entidad = "KAUFFMAN";
		if("".equals(dateStart) || "".equals(dateEnd) || "".equals(type) || "".equals(entidad)) {
			return null;
		}
		
		String response = SSLTestService.getItems(dateStart, dateEnd, "0", "20", type, entidad);
		model.addAttribute("response", response);

		return response;
		
	}
	 
	@RequestMapping(value = "/salesOrderService", method = RequestMethod.POST, consumes = "application/json; charset=utf-8")
	 public BaseResponse salesOrderService(@RequestBody List<SalesOrderDTO> sales) {
		String resp = SOAPSalesService.createSalesOrder(sales);
		BaseResponse o = new BaseResponse();
		o.setStatus("200");
		o.setCode(0);
		o.setContent(resp);
		return o;
	}

	@RequestMapping(value = "/inventoryService", method = RequestMethod.POST, consumes = "application/json; charset=utf-8")
	 public BaseResponse inventoryService(@RequestBody List<InvTransactionDTO> invTrans) {
		String resp = SOAPInvTransactionsService.createInvTransaction(invTrans);
		BaseResponse o = new BaseResponse();
		o.setStatus("200");
		o.setCode(0);
		o.setContent(resp);
		return o;
	}
	
	@RequestMapping(value = "/paymentsService", method = RequestMethod.POST, consumes = "application/json; charset=utf-8")
	 public BaseResponse paymentsService(@RequestBody List<PayloadDTO> payload) {
		String resp = SOAPPayloadsServices.createPayloadOrder(payload);
		BaseResponse o = new BaseResponse();
		o.setStatus("200");
		o.setCode(0);
		o.setContent(resp);
		return o;
	}
	
}
