package com.tdo.integration.util;

import java.io.IOException;

import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

public class CustomClientHttpRequestInterceptor implements   ClientHttpRequestInterceptor {

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
        throws IOException {
    	System.out.println(request.getURI());
        System.out.println(request.getHeaders());
        System.out.println(request.getMethod()); 
        System.out.println(request.toString()); 
        System.out.println(new String(body));

        return execution.execute( request, body );
    }

}
