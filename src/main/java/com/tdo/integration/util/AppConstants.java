package com.tdo.integration.util;

import java.util.ArrayList;
import java.util.List;

public final class AppConstants {
	
	public static final String GLSuccess_Msg = "El archivo de ventas ha sido generado de forma exitosa: ";
	public static final String SOAP_SALESPARTY_URL = "https://ejbh-test.fa.us2.oraclecloud.com:443/crmService/SalesPartyService";
	public static final String SOAP_CUSTOMERPROFILE_URL = "https://ejbfh-test.fa.us2.oraclecloud.com:443/fscmService/ReceivablesCustomerProfileService";
	public static final String SOAP_CUSTOMERACCOUNT_URL = "https://ejbh-test.fa.us2.oraclecloud.com:443/crmService/CustomerAccountService";
	
	public static final String SOAP_ITEMMASTER_URL = "https://ejbh-test.fa.us2.oraclecloud.com:443/fscmService/ItemServiceV2";
	public static final String SOAP_ITEMCOST_URL = "https://ejbh-test.fa.us2.oraclecloud.com:443/fscmService/ItemCostServiceV2";
	
	
	public static final String SOAP_CREATELOCATION_URL = "https://ejbh-test.fa.us2.oraclecloud.com:443/crmService/FoundationPartiesLocationService";
	public static final String SOAP_CREATEORGANIZATION_URL = "https://ejbh-test.fa.us2.oraclecloud.com:443/crmService/FoundationPartiesOrganizationService";	
	public static final String SOAP_CREATEACCOUNT_URL = "https://ejbh-test.fa.us2.oraclecloud.com:443/crmService/CustomerAccountService";
	public static final String SOAP_CREATEPROFILE_URL = "https://ejbh-test.fa.us2.oraclecloud.com:443/fscmService/ReceivablesCustomerProfileService";
	public static final String SOAP_SESSIONID_URL = "https://ejbh-test.fa.us2.oraclecloud.com/analytics-ws/saw.dll?SoapImpl=nQSessionService";
	public static final String SOAP_INVTRANSACTIONS_URL = "https://ejbh-test.fa.us2.oraclecloud.com/analytics-ws/saw.dll?SoapImpl=xmlViewService";
	public static final String SOAP_ITEMCOSTS_URL = "https://ejbh-test.fa.us2.oraclecloud.com:443/fscmService/ItemCostServiceV2";
	public static final String SOAP_FIND_ORGANIZATION_URL = "https://ejbh-test.fa.us2.oraclecloud.com:443/crmService/FoundationPartiesOrganizationService";
	public static final String SOAP_FIND_LOCATION_URL = "https://ejbh-test.fa.us2.oraclecloud.com:443/crmService/FoundationPartiesLocationService";
	
	public static final String REST_FIND_PRICE_LIST_URL = "https://ejbh-test.fa.us2.oraclecloud.com/fscmRestApi/resources/11.13.18.05/priceLists?onlyData=true&fields=PriceListName,PriceListId,Status,StatusCode,BusinessUnit,CalculationMethodCode,StartDate,EndDate,CreationDate,LastUpdateDate&offset=0&limit=100&totalResults=false&q=StatusCode='APPROVED';BusinessUnit='KAUFFMAN'";
	public static final String REST_FIND_ITEM_PRICE_LIST_URL = "https://ejbh-test.fa.us2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/priceLists/300000002697369/child/items?onlyData=true&orderBy=Item&offset=0&limit=100&expand=charges";
	
	public static final String SOAP_BULKIMPORT_URL = "https://ejbh-test.fa.us2.oraclecloud.com/fscmService/ErpIntegrationService";
	
	public static final String SET_ID = "300000000002027";
	public static final String DEFAULT_CONTACT_ID = "100000000350044";
	
	
	public static final String CLOUD_CREDENTIALS = "rrocha@smartech.com.mx:mexico2019";
	
	public static List<String> resultValues = new ArrayList<String>();
	
	public static String countries[] = new String[] {"AF","AX","AL","DZ","AS","AD","AO","AI","AQ","AG","AR","AM","AW","AU","AT","AZ","BS","BH","BD","BB","BY","BE","BZ","BJ","BM","BT","BO","BQ","BA","BW","BV","BR","IO","BN","BG","BF","BI","KH","CM","CA","CV","KY","CF","TD","CL","CN","CX","CC","CO","KM","CG","CD","CK","CR","CI","HR","CU","CW","CY","CZ","DK","DJ","DM","DO","EC","EG","SV","GQ","ER","EE","ET","FK","FO","FJ","FI","FR","GF","PF","TF","GA","GM","GE","DE","GH","GI","GR","GL","GD","GP","GU","GT","GG","GN","GW","GY","HT","HM","VA","HN","HK","HU","IS","IN","ID","IR","IQ","IE","IM","IL","IT","JM","JP","JE","JO","KZ","KE","KI","KP","KR","KW","KG","LA","LV","LB","LS","LR","LY","LI","LT","LU","MO","MK","MG","MW","MY","MV","ML","MT","MH","MQ","MR","MU","YT","MX","FM","MD","MC","MN","ME","MS","MA","MZ","MM","NA","NR","NP","NL","NC","NZ","NI","NE","NG","NU","NF","MP","NO","OM","PK","PW","PS","PA","PG","PY","PE","PH","PN","PL","PT","PR","QA","RE","RO","RU","RW","BL","SH","KN","LC","MF","PM","VC","WS","SM","ST","SA","SN","RS","SC","SL","SG","SX","SK","SI","SB","SO","ZA","GS","SS","ES","LK","SD","SR","SJ","SZ","SE","CH","SY","TW","TJ","TZ","TH","TL","TG","TK","TO","TT","TN","TR","TM","TC","TV","UG","UA","AE","GB","US","UM","UY","UZ","VU","VE","VN","VG","VI","WF","EH","YE","ZM","ZW"};
	
	//DB CONSTANTES PARA INTEGRACION DE VENTAS------------------------------------------------------------------
	public static final String VENTAS_CONTADO = "CONTADO"; 
	public static final String VENTAS_LINE = "LINE";
	public static final String VENTAS_USER = "User";
	public static final String VENTAS_Line = "Line";
	public static final String VENTAS_OVERRIDE = "Y";
	public static final String VENTAS_REV = "REV";
	public static final String VENTAS_PERCENT = "100";
	public static final String VENTAS_F = "FACTURA";
	public static final String VENTAS_NC = "NOTA DE CREDITO";
	
	
	public static final String VENTAS_MXN_CONV_RATE = "1";
	
	public static final String VENTAS_K_TRANS_BAT_SOU_NAME = "POS_Kauffman";
	
	public static final String VENTAS_SERIE = "00";
	
	public static final String VENTAS_K_F_Y = "Factura_Kauffman_Adm";
	public static final String VENTAS_K_F_N = "Factura_Kauffman_Op";
	public static final String VENTAS_K_NC_Y = "NC_Kauffman_Adm";
	public static final String VENTAS_K_NC_N = "NC_Kauffman_Op";
	
	public static final String VENTAS_N_PLAZA[] = new String[] {"001", "02", "2002", "4011101", "000", "0000", "000"};
	public static final String VENTAS_Y_PLAZA[] = new String[] {"001", "02", "2002", "4010101", "000", "0000", "000"};
	
	public static final String NC_N_PLAZA[] = new String[] {"001", "02", "2002", "4021101",	"000", "0000", "000"};
	public static final String NC_Y_PLAZA[] = new String[] {"001", "02", "2002", "4020101", "000", "0000", "000"};
	
	//DB CONSTANTES PARA INTEGRACIÓN DE PAGOS--------------------------------------------------------------------
	public static final String PAGOS_RECORDTYPE = "6";
	
	public static final String PAGOS_RECEIPTMETHOD_SANTANDER = "STND9521_MXN";
	public static final String PAGOS_RECEIPTMETHOD_EFECTIVO = "EFECTIVO_KAUFFMAN";
	
	public static final String PAGOS_LOCKBOXNUMBER_SANTANDER = "SANTANDER_9521";
	public static final String PAGOS_LOCKBOXNUMBER_EFECTIVO = "EFECTIVO_KAUFFMAN";

	public static final String PAGOS_CONVERSIONRATE_USD = "18";

	
	//DB CONSTANTES PARA INTEGRACIÓN DE Inventarios--------------------------------------------------------------------
	public static final String INV_PROCESSFLAG = "1";
	public static final String INV_SER = "SER";
	public static final String INV_LOT = "LOT";
	
	public static final String INV_COST_COMPONENT_CODE = "ITEM_PRICE";
	public static final String INV_SOURCE_HEADER_ID = "1";
	public static final String INV_TRANSACTION_MODE = "3";
	public static final String INV_LOCK_FLAG = "2";
	
	public static final String INV_PALMA[] = new String[] {"001", "02", "2008", "000", "5010101", "0000", "000"};
	
	
	
	public static void addResult(String result) {
		resultValues.add(result);
	}
	
	public static List<String> getResults(){
		return resultValues;
	}

	//Solamente para este caso de las constantes.-------------------------------------------------------------------------------
	public static String getAccountingFlexfieldSegment(String organizationName, String transactionType, String factura, int flexNumber) {
		String AFS1 = "";
		
		if(transactionType.contentEquals("Venta")) {
				switch(organizationName) {
			
					case "PLAZA": 
						if(factura.contentEquals("N")) {
							AFS1 = AppConstants.VENTAS_N_PLAZA[flexNumber - 1];
						}else if(factura.contentEquals("Y")) {
							AFS1 = AppConstants.VENTAS_Y_PLAZA[flexNumber - 1];
						}
		
				}
		}else if(transactionType.contentEquals("NC")) {
				switch(organizationName) {
			
					case "PLAZA": 
						if(factura.contentEquals("N")) {
							AFS1 = AppConstants.NC_N_PLAZA[flexNumber - 1];
						}else if(factura.contentEquals("Y")) {
							AFS1 = AppConstants.NC_Y_PLAZA[flexNumber - 1];
						}
		
				}
		}
		
		return AFS1;
		
			
	}
	
	public static String getDSTSEGMENT(String organizationName, int flexNumber) {
		String AFS1 = "";
		
				switch(organizationName) {
			
					case "PALMA": 
						AFS1 = AppConstants.INV_PALMA[flexNumber - 1];
		
				}

		
		return AFS1;
		
			
	}
	
}
