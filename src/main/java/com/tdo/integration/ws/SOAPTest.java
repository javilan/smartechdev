package com.tdo.integration.ws;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.json.XML;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tdo.integration.pojo.ClientesJson;
import com.tdo.integration.services.HTTPRequestService;
import com.tdo.integration.util.NullValidator;

public class SOAPTest {

	@SuppressWarnings("unused")
	public static void main(String[] args) {

		//String payload = getRequestByCustomerNumber("300000002263344");
		String payload="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">    <soapenv:Header/>    <soapenv:Body>       <tem:Consulta>          <tem:expresionImpresa><![CDATA[?re=MLG820701IE1&rr=EPM811006HH2&tt=18413.0&id=4B0710EA-292E-4378-8671-65313AB777E4]]></tem:expresionImpresa>       </tem:Consulta>    </soapenv:Body> </soapenv:Envelope> ";
		
		String url = "https://consultaqr.facturaelectronica.sat.gob.mx/ConsultaCFDIService.svc";
		List<ClientesJson> clientesJson = new ArrayList<ClientesJson>();
		
		HTTPRequestService HTTPRequestService = new HTTPRequestService();
		try {
			String response = HTTPRequestService.httpXmlRequest(url + "?invoke=", payload,"");

			//String response = "<env:Envelope xmlns:env=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wsa=\"http://www.w3.org/2005/08/addressing\"><env:Header><wsa:Action>http://xmlns.oracle.com/apps/crmCommon/salesParties/salesPartiesService//SalesPartyService/findSalesPartyResponse</wsa:Action><wsa:MessageID>urn:uuid:b8176bb9-137c-4c6a-b40e-2d4e2df31bdc</wsa:MessageID></env:Header><env:Body><ns0:findSalesPartyResponse xmlns:ns0=\"http://xmlns.oracle.com/apps/crmCommon/salesParties/salesPartiesService/types/\"><ns2:result xmlns:ns2=\"http://xmlns.oracle.com/apps/crmCommon/salesParties/salesPartiesService/types/\" xmlns:ns1=\"http://xmlns.oracle.com/apps/crmCommon/salesParties/salesPartiesService/\" xmlns:ns14=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/groupService/\" xmlns:ns3=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/organizationService/\" xmlns:ns0=\"http://xmlns.oracle.com/adf/svc/types/\" xmlns:ns5=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/personService/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"ns1:SalesParty\"><ns1:PartyId>300000002697133</ns1:PartyId><ns1:PartyName>HUMBERTO ROCHA</ns1:PartyName><ns1:PartyNumber>15003</ns1:PartyNumber><ns1:PartyType>ORGANIZATION</ns1:PartyType><ns1:Status>A</ns1:Status><ns1:OrganizationParty xmlns:ns6=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/partyService/\" xmlns:ns11=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/relationshipService/\" xmlns:ns8=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/contactPointService/\"><ns3:CreationDate>2019-01-02T18:07:22.0Z</ns3:CreationDate><ns3:CreatedBy>hrocha@smartech.com.mx</ns3:CreatedBy><ns3:JgzzFiscalCode>ROVH910916M57</ns3:JgzzFiscalCode><ns3:OrganizationProfile xmlns:ns2=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/organization/\"><ns3:JgzzFiscalCode>ROVH910916M57</ns3:JgzzFiscalCode><ns3:CurrencyCode>USD</ns3:CurrencyCode><ns3:PreferredContactName xsi:nil=\"true\"/><ns3:PrimaryEmailAddress xsi:nil=\"true\"/><ns3:PrimaryPhoneNumber xsi:nil=\"true\"/></ns3:OrganizationProfile><ns3:PartySite xmlns:ns10=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/partySite/\"><ns6:PartySiteNumber>11003</ns6:PartySiteNumber><ns6:Country>MX</ns6:Country><ns6:Address1>Cedro 22</ns6:Address1><ns6:Address2 xsi:nil=\"true\"/><ns6:Address3 xsi:nil=\"true\"/><ns6:City>Cuautitlan</ns6:City><ns6:PostalCode>54800</ns6:PostalCode><ns6:State>México</ns6:State></ns3:PartySite></ns1:OrganizationParty></ns2:result></ns0:findSalesPartyResponse></env:Body></env:Envelope>";
			System.out.println(response);
			JSONObject xmlJSONObj = XML.toJSONObject(response, true);
			
			String jsonPrettyPrintString = xmlJSONObj.toString(4);
			System.out.println("PRINTING STRING :::::::::::::::::::::" + jsonPrettyPrintString);
			
			
			JsonElement jelement = new JsonParser().parse(xmlJSONObj.toString());
			JsonObject jobject = jelement.getAsJsonObject();
			
			JsonElement value = jobject.get("env:Envelope").getAsJsonObject().get("env:Body").getAsJsonObject().get("ns0:getActiveCustomerProfileResponse").getAsJsonObject().get("ns4:result").getAsJsonObject().getAsJsonObject().get("ns3:Value").getAsJsonObject();
			String credLimit = value.getAsJsonObject().get("ns3:CreditLimit").toString();
			String pTerms = value.getAsJsonObject().get("ns3:PaymentTerms").toString();
			
			JsonElement soapEnvelope = jobject.get("env:Envelope").getAsJsonObject().get("env:Body").getAsJsonObject().get("ns0:findSalesPartyResponse").getAsJsonObject();
			JsonElement bdy = soapEnvelope.getAsJsonObject().get("ns2:result");
			
			if(bdy instanceof JsonArray) {
				JsonArray jsonarray = bdy.getAsJsonArray();
				for (int i = 0; i < jsonarray.size(); i++) {
					JsonElement op = jsonarray.get(i).getAsJsonObject();
					ClientesJson c = getClientFromJSONTmp(op);
					clientesJson.add(c); 
				}
			}else {
				ClientesJson c = getClientFromJSONTmp(bdy);
				clientesJson.add(c);	
			}	
			
			@SuppressWarnings("unused")
			int i = 0;
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public static String getRequestByCustomerNumber(String partyId){
		String accountNumber = "3003";		
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/financials/receivables/customers/customerProfileService/types/\" xmlns:cus=\"http://xmlns.oracle.com/apps/financials/receivables/customers/customerProfileService/\" xmlns:cus1=\"http://xmlns.oracle.com/apps/financials/receivables/customerSetup/customerProfiles/model/flex/CustomerProfileDff/\" xmlns:cus2=\"http://xmlns.oracle.com/apps/financials/receivables/customerSetup/customerProfiles/model/flex/CustomerProfileGdf/\"> " + 
				"   <soapenv:Header/> " + 
				"   <soapenv:Body>" + 
				"   <typ:getActiveCustomerProfile xmlns:typ=\"http://xmlns.oracle.com/apps/financials/receivables/customers/customerProfileService/types/\" xmlns:cus=\"http://xmlns.oracle.com/apps/financials/receivables/customers/customerProfileService/\"> " +
			    "    <typ:customerProfile> " +
			    "       <cus:AccountNumber>" + accountNumber + "</cus:AccountNumber> " +
			    "       <cus:PartyId>" + partyId + "</cus:PartyId> " +
			    "    </typ:customerProfile> " +
			    " </typ:getActiveCustomerProfile>" +
				" </soapenv:Body> " + 
				" </soapenv:Envelope> ";
		return SOAPRequest;
	}
	
private static ClientesJson getClientFromJSONTmp(JsonElement root) {
	
		
		ClientesJson clientes = new ClientesJson();
		JsonElement organizationParty = root.getAsJsonObject().get("ns1:OrganizationParty");
		JsonElement organizationProfile = organizationParty.getAsJsonObject().get("ns3:OrganizationProfile");
		JsonElement partySite = organizationParty.getAsJsonObject().get("ns3:PartySite");
	   
	    clientes.setCliId(0);
		clientes.setNombre(root.getAsJsonObject().get("ns1:PartyName").toString());
		clientes.setRepresentante(NullValidator.isNull(organizationProfile.getAsJsonObject().get("ns3:PreferredContactName").toString()));
		clientes.setDomicilio(NullValidator.isNull(partySite.getAsJsonObject().get("ns6:Address1").toString()));
		clientes.setNoExt(NullValidator.isNull(partySite.getAsJsonObject().get("ns6:Address2").toString()));
		clientes.setNoInt("");
		clientes.setLocalidad("");
		clientes.setCiudad(NullValidator.isNull(partySite.getAsJsonObject().get("ns6:City").toString()));
		clientes.setEstado(NullValidator.isNull(partySite.getAsJsonObject().get("ns6:State").toString()));
		clientes.setPais(NullValidator.isNull(partySite.getAsJsonObject().get("ns6:Country").toString()));
		clientes.setCodigoPostal(NullValidator.isNull(partySite.getAsJsonObject().get("ns6:PostalCode").toString()));
		clientes.setColonia(NullValidator.isNull(partySite.getAsJsonObject().get("ns6:Address3").toString()));
		clientes.setRfc(NullValidator.isNull(organizationProfile.getAsJsonObject().get("ns3:JgzzFiscalCode").toString()));
		String partyNumber = String.valueOf(root.getAsJsonObject().get("ns1:PartyNumber").toString());
		String siteNumber = NullValidator.isNull(partySite.getAsJsonObject().get("ns6:PartySiteNumber").toString());
		clientes.setCurp(partyNumber + "/" + siteNumber);
		clientes.setTelefono(NullValidator.isNull(organizationProfile.getAsJsonObject().get("ns3:PrimaryPhoneNumber").toString()));
		clientes.setCelular("");
		clientes.setMail(NullValidator.isNull(organizationProfile.getAsJsonObject().get("ns3:PrimaryEmailAddress").toString()));
		clientes.setComentario("");
		
		if("A".equals(NullValidator.isNull(root.getAsJsonObject().get("ns1:Status").toString())))
		     clientes.setStatus(1);
		else
			 clientes.setStatus(0);
	
		clientes.setLimite(0); 
		clientes.setPrecio(1); 
		clientes.setDiasCredito(0); 
		clientes.setRetener(false);
		clientes.setDesglosarIEPS(false);
		clientes.setClave("");
		clientes.setFoto(null);
		clientes.setHuella(null);
		clientes.setMuestra(null);
		clientes.setUsoCfdi("");
		clientes.setComplementoList(new String[]{});
		
		if("".equals(clientes.getRepresentante())) {
			clientes.setRepresentante("Sin Representante");
		}
		return clientes;
	}


@SuppressWarnings("unused")
private static ClientesJson getClientFromJSON(JSONObject root) {
	
		
		ClientesJson clientes = new ClientesJson();
	    JSONObject organizationParty = root.getJSONObject("ns1:OrganizationParty");
	    JSONObject organizationProfile = organizationParty.getJSONObject("ns3:OrganizationProfile");
	    JSONObject partySite = organizationParty.getJSONObject("ns3:PartySite");
	   
	    clientes.setCliId(0);
		clientes.setNombre(root.getString("ns1:PartyName"));
		clientes.setRepresentante(NullValidator.isNull(organizationProfile.getString("ns3:PreferredContactName").toString()));
		clientes.setDomicilio(NullValidator.isNull(partySite.getString("ns6:Address1")));
		clientes.setNoExt(NullValidator.isNull(partySite.getString("ns6:Address2")));
		clientes.setNoInt("");
		clientes.setLocalidad("");
		clientes.setCiudad(NullValidator.isNull(partySite.getString("ns6:City")));
		clientes.setEstado(NullValidator.isNull(partySite.getString("ns6:State")));
		clientes.setPais(NullValidator.isNull(partySite.getString("ns6:Country")));
		clientes.setCodigoPostal(NullValidator.isNull(partySite.getString("ns6:PostalCode")));
		clientes.setColonia(NullValidator.isNull(partySite.getString("ns6:Address3")));
		clientes.setRfc(NullValidator.isNull(organizationProfile.getString("ns3:JgzzFiscalCode")));
		String partyNumber = String.valueOf(root.getString("ns1:PartyNumber"));
		String siteNumber = NullValidator.isNull(partySite.getString("ns6:PartySiteNumber"));
		clientes.setCurp(partyNumber + "/" + siteNumber);
		clientes.setTelefono(NullValidator.isNull(organizationProfile.getString("ns3:OrganizationProfile")));
		clientes.setCelular("");
		clientes.setMail(NullValidator.isNull(organizationProfile.getString("ns3:PrimaryEmailAddress")));
		clientes.setComentario("");
		
		if("A".equals(NullValidator.isNull(root.getString("ns1:Status"))))
		     clientes.setStatus(1);
		else
			 clientes.setStatus(0);
	
		clientes.setLimite(0); 
		clientes.setPrecio(1); 
		clientes.setDiasCredito(0); 
		clientes.setRetener(false);
		clientes.setDesglosarIEPS(false);
		clientes.setClave("");
		clientes.setFoto(null);
		clientes.setHuella(null);
		clientes.setMuestra(null);
		clientes.setUsoCfdi("");
		clientes.setComplementoList(new String[]{});
		
		if("".equals(clientes.getRepresentante())) {
			clientes.setRepresentante("Sin Representante");
		}
		return clientes;
	}


}
