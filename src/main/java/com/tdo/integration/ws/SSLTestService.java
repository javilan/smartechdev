package com.tdo.integration.ws;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tdo.integration.dto.ItemMasterKauffmanDTO;
import com.tdo.integration.services.HTTPRequestService;
import com.tdo.integration.util.AppConstants;


@Service("SSLTestService")
public class SSLTestService {
	
	@Autowired  
	HTTPRequestService HTTPRequestService; 
	
	final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	final SimpleDateFormat formatterUTC = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	final SimpleDateFormat lv_formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss z'('Z')'");
	
	Date requestStartDate = null;
	Date requestEndDate = null;
	String dateStartUTC = "";
	String dateEndUTC = "";
	
	public String getItems(String dateStart, String dateEnd, String start, String size, String type, String entidad) {
		
		String response = "";
		sdf.setTimeZone(TimeZone.getTimeZone("America/Mexico_City"));
		formatterUTC.setTimeZone(TimeZone.getTimeZone("UTC"));

		List<ItemMasterKauffmanDTO> itemList = new ArrayList<ItemMasterKauffmanDTO>();
		
		String dateType = "";
		if("U".equals(type)) {
			dateType = "LastUpdateDateTime";
		}else {
			dateType = "CreationDateTime";
		}
		
		String entity = "";
		List<String> entityList = null;
		if("KAUFFMAN".equals(entidad)) {
			entity = "BC,K006";
		}else {
			entity = "AS";
		}
		
		entityList = Arrays.asList(entity.split(","));

		try {
			
			requestStartDate = sdf.parse(dateStart);
			dateStartUTC = formatterUTC.format(requestStartDate);
			
			requestEndDate = sdf.parse(dateEnd);
			dateEndUTC = formatterUTC.format(requestEndDate);
			
			 response = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_ITEMMASTER_URL + "?invoke=", 
					                                            PayloadProducer.getItemMasterSOAPXmlContent(dateStartUTC, dateEndUTC, start, size, dateType, entityList,""),
					                                            AppConstants.CLOUD_CREDENTIALS);
			 
			 return response;
			 
			
			
		} catch (Exception e) {
			e.printStackTrace();
			return e.toString();
		}
	}



}
