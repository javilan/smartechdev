package com.tdo.integration.ws;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tdo.integration.dto.RAInterfaceDistributionsSO;
import com.tdo.integration.dto.RAInterfaceLinesAllSO;
import com.tdo.integration.dto.RaInterface;
import com.tdo.integration.dto.RaInterfaceDistributionsAll;
import com.tdo.integration.dto.RaInterfaceLinesAll;
import com.tdo.integration.dto.SalesOrderDTO;
import com.tdo.integration.util.AppConstants;
import com.berdico.webservices.services.file.IntegrationRestServiceFile;


@Service("SOAPSalesService")
public class SOAPSalesService {

	@Autowired
	IntegrationRestServiceFile IntegrationRestServiceFile;

	public String createSalesOrder(List<SalesOrderDTO> sales) {
		
		String resp = null;

		List<RAInterfaceLinesAllSO> lines = new ArrayList<RAInterfaceLinesAllSO>();
		List<RAInterfaceDistributionsSO> dists = new ArrayList<RAInterfaceDistributionsSO>();
		
		if(sales != null) {
			if(!sales.isEmpty()) {
				for(SalesOrderDTO c: sales) {
					
					
					
					int nc = 0, f = 0;									//Validar------------------------------------------
					if(c.getTipoVenta().contentEquals("Venta")) {
						f++;
					}else if(c.getTipoVenta().contentEquals("NC")) {
						nc++;
					}													//Validar------------------------------------------
					
					for(int d = 0; d < c.getSalesLines().size(); d++){
						
						RAInterfaceLinesAllSO line = new RAInterfaceLinesAllSO();
						RAInterfaceDistributionsSO dist = new RAInterfaceDistributionsSO();
						
						
						if(c.getBusinessUnit().contentEquals("KAUFFMAN")) {
							if(c.getTipoVenta().contentEquals("Venta") && c.getFactura() == 'N') {									
									
								String transNum = "K-" + c.getTransactionDate().substring(5,7) + "-" + c.getTransactionDate().substring(0, 4) + "-" + c.getTransactionDate().substring(8,10) + "-" + "F" + (String.valueOf(f));
								
								line.set_transactionBatchSourceName(AppConstants.VENTAS_K_TRANS_BAT_SOU_NAME);
								line.set_transactionTypeName(AppConstants.VENTAS_K_F_N);
								line.set_paymentTerms(AppConstants.VENTAS_CONTADO);
								line.set_transactionDate(c.getTransactionDate().substring(0, 4) + "/" + c.getTransactionDate().substring(5,7) + "/" + c.getTransactionDate().substring(8, 10));
								line.set_accountingDate(c.getTransactionDate().substring(0, 4) + "/" + c.getTransactionDate().substring(5,7) + "/" + c.getTransactionDate().substring(8, 10));
/*--------*/					line.set_transactionNumber(transNum);
								line.set_BilltoCustomerAccountNumber(c.getAccountNumber());
								line.set_BilltoCustomerSiteNumber(c.getSiteNumber());
								line.set_transactionLineType(AppConstants.VENTAS_LINE);
								line.set_transactionLineDescription(c.getSalesLines().get(d).getItemDescription());
								line.set_currencyCode(c.getCurrencyCode());
								line.set_currencyConversionType(AppConstants.VENTAS_USER);
								line.set_currencyConversionDate(c.getTransactionDate().substring(0, 4) + "/" + c.getTransactionDate().substring(5,7) + "/" + c.getTransactionDate().substring(8, 10));
								line.set_currencyConversionRate(AppConstants.VENTAS_MXN_CONV_RATE);
								line.set_transactionLineAmount(String.valueOf(c.getSalesLines().get(d).getTotalAmount()));
								line.set_transactionLineQuantity(String.valueOf(c.getSalesLines().get(d).getItemQuantity()));
								line.set_unitSellingPrice(String.valueOf(c.getSalesLines().get(d).getItemUnitPrice()));
								line.set_lineTransactionsFlexfieldContext(AppConstants.VENTAS_K_TRANS_BAT_SOU_NAME);
								line.set_lineTransactionsFlexfieldSegment1(transNum);
								line.set_lineTransactionsFlexfieldSegment2(AppConstants.VENTAS_Line + c.getSalesLines().get(d).getLineNum());
								line.set_lineTransactionsFlexfieldSegment3(AppConstants.VENTAS_F);
								line.set_salesOrderNumber(c.getVentaIdPOS());
								line.set_unitofMeasureCode(c.getSalesLines().get(d).getUOM());
								line.set_inventoryItemNumber(c.getSalesLines().get(d).getItemSKU());
								line.set_overrideAutoAccountingFlag(AppConstants.VENTAS_OVERRIDE);
								line.set_businessUnitName(c.getBusinessUnit());
								
								//--------------------------------------------------------------------------
								
								dist.set_accountClass(String.valueOf(c.getSalesLines().get(d).getTotalAmount()));
								dist.set_percent(AppConstants.VENTAS_PERCENT);
								dist.set_lineTransactionsFlexfieldContext(AppConstants.VENTAS_K_TRANS_BAT_SOU_NAME);
								dist.set_lineTransactionsFlexfieldSegment1(transNum);
								dist.set_lineTransactionsFlexfieldSegment2(AppConstants.VENTAS_Line + c.getSalesLines().get(d).getLineNum());
								dist.set_lineTransactionsFlexfieldSegment3(AppConstants.VENTAS_F);
								dist.set_accountingFlexfieldSegment1(AppConstants.getAccountingFlexfieldSegment(c.getOrganizationName(), c.getTipoVenta(), String.valueOf(c.getFactura()), 1));
								dist.set_accountingFlexfieldSegment2(AppConstants.getAccountingFlexfieldSegment(c.getOrganizationName(), c.getTipoVenta(), String.valueOf(c.getFactura()), 2));
								dist.set_accountingFlexfieldSegment3(AppConstants.getAccountingFlexfieldSegment(c.getOrganizationName(), c.getTipoVenta(), String.valueOf(c.getFactura()), 3));
								dist.set_accountingFlexfieldSegment4(AppConstants.getAccountingFlexfieldSegment(c.getOrganizationName(), c.getTipoVenta(), String.valueOf(c.getFactura()), 4));
								dist.set_accountingFlexfieldSegment5(AppConstants.getAccountingFlexfieldSegment(c.getOrganizationName(), c.getTipoVenta(), String.valueOf(c.getFactura()), 5));
								dist.set_accountingFlexfieldSegment6(AppConstants.getAccountingFlexfieldSegment(c.getOrganizationName(), c.getTipoVenta(), String.valueOf(c.getFactura()), 6));
								dist.set_accountingFlexfieldSegment7(AppConstants.getAccountingFlexfieldSegment(c.getOrganizationName(), c.getTipoVenta(), String.valueOf(c.getFactura()), 7));
								dist.set_businessUnitName(c.getBusinessUnit());
								
								
							}else if(c.getTipoVenta().contentEquals("Venta") && c.getFactura() == 'Y') {
								
								
								String transNum = "K-" + c.getTransactionDate().substring(5,7) + "-" + c.getTransactionDate().substring(0, 4) + "-" + c.getTransactionDate().substring(8,10) + "-" + "F" + (String.valueOf(f));
								
								line.set_transactionBatchSourceName(AppConstants.VENTAS_K_TRANS_BAT_SOU_NAME);
								line.set_transactionTypeName(AppConstants.VENTAS_K_F_Y);
								line.set_paymentTerms(AppConstants.VENTAS_CONTADO);
								line.set_transactionDate(c.getTransactionDate().substring(0, 4) + "/" + c.getTransactionDate().substring(5,7) + "/" + c.getTransactionDate().substring(8, 10));
								line.set_accountingDate(c.getTransactionDate().substring(0, 4) + "/" + c.getTransactionDate().substring(5,7) + "/" + c.getTransactionDate().substring(8, 10));
/*--------*/					line.set_transactionNumber(transNum);
								line.set_BilltoCustomerAccountNumber(c.getAccountNumber());
								line.set_BilltoCustomerSiteNumber(c.getSiteNumber());
								line.set_transactionLineType(AppConstants.VENTAS_LINE);
								line.set_transactionLineDescription(c.getSalesLines().get(d).getItemDescription());
								line.set_currencyCode(c.getCurrencyCode());
								line.set_currencyConversionType(AppConstants.VENTAS_USER);
								line.set_currencyConversionDate(c.getTransactionDate().substring(0, 4) + "/" + c.getTransactionDate().substring(5,7) + "/" + c.getTransactionDate().substring(8, 10));
								line.set_currencyConversionRate(AppConstants.VENTAS_MXN_CONV_RATE);
								line.set_transactionLineAmount(String.valueOf(c.getSalesLines().get(d).getTotalAmount()));
								line.set_transactionLineQuantity(String.valueOf(c.getSalesLines().get(d).getItemQuantity()));
								line.set_unitSellingPrice(String.valueOf(c.getSalesLines().get(d).getItemUnitPrice()));
								line.set_lineTransactionsFlexfieldContext(AppConstants.VENTAS_K_TRANS_BAT_SOU_NAME);
								line.set_lineTransactionsFlexfieldSegment1(transNum);
								line.set_lineTransactionsFlexfieldSegment2(AppConstants.VENTAS_Line + c.getSalesLines().get(d).getLineNum());
								line.set_lineTransactionsFlexfieldSegment3(AppConstants.VENTAS_F);
								line.set_salesOrderNumber(c.getVentaIdPOS());
								line.set_unitofMeasureCode(c.getSalesLines().get(d).getUOM());
								line.set_inventoryItemNumber(c.getSalesLines().get(d).getItemSKU());
								line.set_overrideAutoAccountingFlag(AppConstants.VENTAS_OVERRIDE);
								line.set_businessUnitName(c.getBusinessUnit());
								
								//--------------------------------------------------------------------------
								
								dist.set_accountClass(String.valueOf(c.getSalesLines().get(d).getTotalAmount()));
								dist.set_percent(AppConstants.VENTAS_PERCENT);
								dist.set_lineTransactionsFlexfieldContext(AppConstants.VENTAS_K_TRANS_BAT_SOU_NAME);
								dist.set_lineTransactionsFlexfieldSegment1(transNum);
								dist.set_lineTransactionsFlexfieldSegment2(AppConstants.VENTAS_Line + c.getSalesLines().get(d).getLineNum());
								dist.set_lineTransactionsFlexfieldSegment3(AppConstants.VENTAS_F);
								dist.set_accountingFlexfieldSegment1(AppConstants.getAccountingFlexfieldSegment(c.getOrganizationName(), c.getTipoVenta(), String.valueOf(c.getFactura()), 1));
								dist.set_accountingFlexfieldSegment2(AppConstants.getAccountingFlexfieldSegment(c.getOrganizationName(), c.getTipoVenta(), String.valueOf(c.getFactura()), 2));
								dist.set_accountingFlexfieldSegment3(AppConstants.getAccountingFlexfieldSegment(c.getOrganizationName(), c.getTipoVenta(), String.valueOf(c.getFactura()), 3));
								dist.set_accountingFlexfieldSegment4(AppConstants.getAccountingFlexfieldSegment(c.getOrganizationName(), c.getTipoVenta(), String.valueOf(c.getFactura()), 4));
								dist.set_accountingFlexfieldSegment5(AppConstants.getAccountingFlexfieldSegment(c.getOrganizationName(), c.getTipoVenta(), String.valueOf(c.getFactura()), 5));
								dist.set_accountingFlexfieldSegment6(AppConstants.getAccountingFlexfieldSegment(c.getOrganizationName(), c.getTipoVenta(), String.valueOf(c.getFactura()), 6));
								dist.set_accountingFlexfieldSegment7(AppConstants.getAccountingFlexfieldSegment(c.getOrganizationName(), c.getTipoVenta(), String.valueOf(c.getFactura()), 7));
								dist.set_businessUnitName(c.getBusinessUnit());
							}else if(c.getTipoVenta().contentEquals("NC") && c.getFactura() == 'N') {
								
							
								String transNum = "K-" + c.getTransactionDate().substring(5,7) + "-" + c.getTransactionDate().substring(0, 4)  + "-" + c.getTransactionDate().substring(8,10) + "-" + "NC" + (String.valueOf(nc));
								
								line.set_transactionBatchSourceName(AppConstants.VENTAS_K_TRANS_BAT_SOU_NAME);
								line.set_transactionTypeName(AppConstants.VENTAS_K_NC_N);
								//line.set_paymentTerms(AppConstants.VENTAS_CONTADO);
								line.set_transactionDate(c.getTransactionDate().substring(0, 4) + "/" + c.getTransactionDate().substring(5,7) + "/" + c.getTransactionDate().substring(8, 10));
								line.set_accountingDate(c.getTransactionDate().substring(0, 4) + "/" + c.getTransactionDate().substring(5,7) + "/" + c.getTransactionDate().substring(8, 10));
/*--------*/					line.set_transactionNumber(transNum);
								line.set_BilltoCustomerAccountNumber(c.getAccountNumber());
								line.set_BilltoCustomerSiteNumber(c.getSiteNumber());
								line.set_transactionLineType(AppConstants.VENTAS_LINE);
								line.set_transactionLineDescription(c.getSalesLines().get(d).getItemDescription());
								line.set_currencyCode(c.getCurrencyCode());
								line.set_currencyConversionType(AppConstants.VENTAS_USER);
								line.set_currencyConversionDate(c.getTransactionDate().substring(0, 4) + "/" + c.getTransactionDate().substring(5,7) + "/" + c.getTransactionDate().substring(8, 10));
								line.set_currencyConversionRate(AppConstants.VENTAS_MXN_CONV_RATE);
								line.set_transactionLineAmount(String.valueOf(c.getSalesLines().get(d).getTotalAmount()));
								line.set_transactionLineQuantity(String.valueOf(c.getSalesLines().get(d).getItemQuantity()));
								line.set_unitSellingPrice(String.valueOf(c.getSalesLines().get(d).getItemUnitPrice()));
								line.set_lineTransactionsFlexfieldContext(AppConstants.VENTAS_K_TRANS_BAT_SOU_NAME);
								line.set_lineTransactionsFlexfieldSegment1(transNum);
								line.set_lineTransactionsFlexfieldSegment2(AppConstants.VENTAS_Line + c.getSalesLines().get(d).getLineNum());
								line.set_lineTransactionsFlexfieldSegment3(AppConstants.VENTAS_NC);
								line.set_salesOrderNumber(c.getVentaIdPOS());
								line.set_unitofMeasureCode(c.getSalesLines().get(d).getUOM());
								line.set_referenceTransactionsFlexfieldContext(AppConstants.VENTAS_K_TRANS_BAT_SOU_NAME);
								line.set_referenceTransactionsFlexfieldSegment1(c.getNCIdRefVenta());
								line.set_referenceTransactionsFlexfieldSegment2(AppConstants.VENTAS_Line + c.getSalesLines().get(d).getLineNum());
								line.set_referenceTransactionsFlexfieldSegment3(AppConstants.VENTAS_F);
								line.set_inventoryItemNumber(c.getSalesLines().get(d).getItemSKU());
								line.set_overrideAutoAccountingFlag(AppConstants.VENTAS_OVERRIDE);
								line.set_businessUnitName(c.getBusinessUnit());
								
								//--------------------------------------------------------------------------
								
								dist.set_accountClass(String.valueOf(c.getSalesLines().get(d).getTotalAmount()));
								dist.set_percent(AppConstants.VENTAS_PERCENT);
								dist.set_lineTransactionsFlexfieldContext(AppConstants.VENTAS_K_TRANS_BAT_SOU_NAME);
	/*----*/   					dist.set_lineTransactionsFlexfieldSegment1(transNum);
								dist.set_lineTransactionsFlexfieldSegment2(AppConstants.VENTAS_Line + c.getSalesLines().get(d).getLineNum());
								dist.set_lineTransactionsFlexfieldSegment3(AppConstants.VENTAS_NC);
								dist.set_accountingFlexfieldSegment1(AppConstants.getAccountingFlexfieldSegment(c.getOrganizationName(), c.getTipoVenta(), String.valueOf(c.getFactura()), 1));
								dist.set_accountingFlexfieldSegment2(AppConstants.getAccountingFlexfieldSegment(c.getOrganizationName(), c.getTipoVenta(), String.valueOf(c.getFactura()), 2));
								dist.set_accountingFlexfieldSegment3(AppConstants.getAccountingFlexfieldSegment(c.getOrganizationName(), c.getTipoVenta(), String.valueOf(c.getFactura()), 3));
								dist.set_accountingFlexfieldSegment4(AppConstants.getAccountingFlexfieldSegment(c.getOrganizationName(), c.getTipoVenta(), String.valueOf(c.getFactura()), 4));
								dist.set_accountingFlexfieldSegment5(AppConstants.getAccountingFlexfieldSegment(c.getOrganizationName(), c.getTipoVenta(), String.valueOf(c.getFactura()), 5));
								dist.set_accountingFlexfieldSegment6(AppConstants.getAccountingFlexfieldSegment(c.getOrganizationName(), c.getTipoVenta(), String.valueOf(c.getFactura()), 6));
								dist.set_accountingFlexfieldSegment7(AppConstants.getAccountingFlexfieldSegment(c.getOrganizationName(), c.getTipoVenta(), String.valueOf(c.getFactura()), 7));
								dist.set_businessUnitName(c.getBusinessUnit());
								
							}else if(c.getTipoVenta().contentEquals("NC") && c.getFactura() == 'Y') {
								
								
								String transNum = "K-" + c.getTransactionDate().substring(5,7) + "-" + c.getTransactionDate().substring(0, 4) + "-" + c.getTransactionDate().substring(8,10)  + "-" + "NC" + (String.valueOf(nc));
								
								line.set_transactionBatchSourceName(AppConstants.VENTAS_K_TRANS_BAT_SOU_NAME);
								line.set_transactionTypeName(AppConstants.VENTAS_K_NC_Y);
								//line.set_paymentTerms(AppConstants.VENTAS_CONTADO);
								line.set_transactionDate(c.getTransactionDate().substring(0, 4) + "/" + c.getTransactionDate().substring(5,7) + "/" + c.getTransactionDate().substring(8, 10));
								line.set_accountingDate(c.getTransactionDate().substring(0, 4) + "/" + c.getTransactionDate().substring(5,7) + "/" + c.getTransactionDate().substring(8, 10));
/*--------*/					line.set_transactionNumber(transNum);
								line.set_BilltoCustomerAccountNumber(c.getAccountNumber());
								line.set_BilltoCustomerSiteNumber(c.getSiteNumber());
								line.set_transactionLineType(AppConstants.VENTAS_LINE);
								line.set_transactionLineDescription(c.getSalesLines().get(d).getItemDescription());
								line.set_currencyCode(c.getCurrencyCode());
								line.set_currencyConversionType(AppConstants.VENTAS_USER);
								line.set_currencyConversionDate(c.getTransactionDate().substring(0, 4) + "/" + c.getTransactionDate().substring(5,7) + "/" + c.getTransactionDate().substring(8, 10));
								line.set_currencyConversionRate(AppConstants.VENTAS_MXN_CONV_RATE);
								line.set_transactionLineAmount(String.valueOf(c.getSalesLines().get(d).getTotalAmount()));
								line.set_transactionLineQuantity(String.valueOf(c.getSalesLines().get(d).getItemQuantity()));
								line.set_unitSellingPrice(String.valueOf(c.getSalesLines().get(d).getItemUnitPrice()));
								line.set_lineTransactionsFlexfieldContext(AppConstants.VENTAS_K_TRANS_BAT_SOU_NAME);
								line.set_lineTransactionsFlexfieldSegment1(transNum);
								line.set_lineTransactionsFlexfieldSegment2(AppConstants.VENTAS_Line + c.getSalesLines().get(d).getLineNum());
								line.set_lineTransactionsFlexfieldSegment3(AppConstants.VENTAS_NC);
								line.set_salesOrderNumber(c.getVentaIdPOS());
								line.set_unitofMeasureCode(c.getSalesLines().get(d).getUOM());
								line.set_referenceTransactionsFlexfieldContext(AppConstants.VENTAS_K_TRANS_BAT_SOU_NAME);
								line.set_referenceTransactionsFlexfieldSegment1(c.getNCIdRefVenta());
								line.set_referenceTransactionsFlexfieldSegment2(AppConstants.VENTAS_Line + c.getSalesLines().get(d).getLineNum());
								line.set_referenceTransactionsFlexfieldSegment3(AppConstants.VENTAS_F);
								line.set_inventoryItemNumber(c.getSalesLines().get(d).getItemSKU());
								line.set_overrideAutoAccountingFlag(AppConstants.VENTAS_OVERRIDE);
								line.set_businessUnitName(c.getBusinessUnit());
								
								//--------------------------------------------------------------------------
								
								dist.set_accountClass(String.valueOf(c.getSalesLines().get(d).getTotalAmount()));
								dist.set_percent(AppConstants.VENTAS_PERCENT);
								dist.set_lineTransactionsFlexfieldContext(AppConstants.VENTAS_K_TRANS_BAT_SOU_NAME);
								dist.set_lineTransactionsFlexfieldSegment1(transNum);
								dist.set_lineTransactionsFlexfieldSegment2(AppConstants.VENTAS_Line + c.getSalesLines().get(d).getLineNum());
								dist.set_lineTransactionsFlexfieldSegment3(AppConstants.VENTAS_NC);
								dist.set_accountingFlexfieldSegment1(AppConstants.getAccountingFlexfieldSegment(c.getOrganizationName(), c.getTipoVenta(), String.valueOf(c.getFactura()), 1));
								dist.set_accountingFlexfieldSegment2(AppConstants.getAccountingFlexfieldSegment(c.getOrganizationName(), c.getTipoVenta(), String.valueOf(c.getFactura()), 2));
								dist.set_accountingFlexfieldSegment3(AppConstants.getAccountingFlexfieldSegment(c.getOrganizationName(), c.getTipoVenta(), String.valueOf(c.getFactura()), 3));
								dist.set_accountingFlexfieldSegment4(AppConstants.getAccountingFlexfieldSegment(c.getOrganizationName(), c.getTipoVenta(), String.valueOf(c.getFactura()), 4));
								dist.set_accountingFlexfieldSegment5(AppConstants.getAccountingFlexfieldSegment(c.getOrganizationName(), c.getTipoVenta(), String.valueOf(c.getFactura()), 5));
								dist.set_accountingFlexfieldSegment6(AppConstants.getAccountingFlexfieldSegment(c.getOrganizationName(), c.getTipoVenta(), String.valueOf(c.getFactura()), 6));
								dist.set_accountingFlexfieldSegment7(AppConstants.getAccountingFlexfieldSegment(c.getOrganizationName(), c.getTipoVenta(), String.valueOf(c.getFactura()), 7));
								dist.set_businessUnitName(c.getBusinessUnit());
								
							}
							
							
						}
						
						lines.add(line);
						dists.add(dist);
						
					}
					
					
				}
				
				
			}
			
			List<RaInterfaceLinesAll> header = new ArrayList<RaInterfaceLinesAll>();
			List<RaInterfaceDistributionsAll> detail = new ArrayList<RaInterfaceDistributionsAll>();
			
			for(RAInterfaceLinesAllSO a: lines) {
				RaInterfaceLinesAll h1 = new RaInterfaceLinesAll();
				
				h1.setTransaction_Batch_Source_Name(a.get_transactionBatchSourceName());
				h1.setTransaction_Type_Name(a.get_transactionTypeName());
				h1.setPayment_Terms(a.get_paymentTerms());
				h1.setTransaction_Date(a.get_transactionDate());
				h1.setAccounting_Date(a.get_accountingDate());
				h1.setTransaction_Number(a.get_transactionNumber());
				h1.setBill_to_Customer_Account_Number(a.get_BilltoCustomerAccountNumber());
				h1.setBill_to_Customer_Site_Number(a.get_BilltoCustomerSiteNumber());
				h1.setTransaction_Line_Type(a.get_transactionLineType());
				h1.setTransaction_Line_Description(a.get_transactionLineDescription());
				h1.setCurrency_Code(a.get_currencyCode());
				h1.setCurrency_Conversion_Type(a.get_currencyConversionType());
				h1.setCurrency_Conversion_Date(a.get_currencyConversionDate());
				h1.setCurrency_Conversion_Rate(a.get_currencyConversionRate());
				h1.setTransaction_Line_Amount(a.get_transactionLineAmount());
				h1.setTransaction_Line_Quantity(a.get_transactionLineQuantity());
				h1.setUnit_Selling_Price(a.get_unitSellingPrice());
				h1.setLine_Transactions_Flexfield_Context(a.get_lineTransactionsFlexfieldContext());
				h1.setLine_Transactions_Flexfield_Segment_1(a.get_lineTransactionsFlexfieldSegment1());
				h1.setLine_Transactions_Flexfield_Segment_2(a.get_lineTransactionsFlexfieldSegment2());
				h1.setLine_Transactions_Flexfield_Segment_3(a.get_lineTransactionsFlexfieldSegment3());
				h1.setSales_Order_Number(a.get_salesOrderNumber());
				h1.setUnit_of_Measure_Code(a.get_unitofMeasureCode());
				h1.setReference_Transactions_Flexfield_Context(a.get_referenceTransactionsFlexfieldContext());
				h1.setReference_Transactions_Flexfield_Segment_1(a.get_referenceTransactionsFlexfieldSegment1());
				h1.setReference_Transactions_Flexfield_Segment_2(a.get_referenceTransactionsFlexfieldSegment2());
				h1.setReference_Transactions_Flexfield_Segment_3(a.get_referenceTransactionsFlexfieldSegment3());
				h1.setInventory_Item_Number(a.get_inventoryItemNumber());
				h1.setOverride_AutoAccounting_Flag(a.get_overrideAutoAccountingFlag());
				h1.setBusiness_Unit_Name(a.get_businessUnitName());
				
				header.add(h1);
			}
			
			for(RAInterfaceDistributionsSO a: dists) {
				RaInterfaceDistributionsAll d1 = new RaInterfaceDistributionsAll();
				
				d1.setAccount_Class(a.get_accountClass());
				d1.setPercent(a.get_percent());
				d1.setLine_Transactions_Flexfield_Context(a.get_lineTransactionsFlexfieldContext());
				d1.setLine_Transactions_Flexfield_Segment_1(a.get_lineTransactionsFlexfieldSegment1());
				d1.setLine_Transactions_Flexfield_Segment_2(a.get_lineTransactionsFlexfieldSegment2());
				d1.setLine_Transactions_Flexfield_Segment_3(a.get_lineTransactionsFlexfieldSegment3());
				d1.setAccounting_Flexfield_Segment_1(a.get_accountingFlexfieldSegment1());
				d1.setAccounting_Flexfield_Segment_2(a.get_accountingFlexfieldSegment2());
				d1.setAccounting_Flexfield_Segment_3(a.get_accountingFlexfieldSegment3());
				d1.setAccounting_Flexfield_Segment_4(a.get_accountingFlexfieldSegment4());
				d1.setAccounting_Flexfield_Segment_5(a.get_accountingFlexfieldSegment5());
				d1.setAccounting_Flexfield_Segment_6(a.get_accountingFlexfieldSegment6());
				d1.setAccounting_Flexfield_Segment_7(a.get_accountingFlexfieldSegment7());
				d1.setBusiness_Unit_Name(a.get_businessUnitName());
				
				detail.add(d1);
			}
			
			
			RaInterface ra = new RaInterface();
			ra.setRaInterfaceDistributionsAll(detail);
			ra.setRaInterfaceLinesAll(header);
			
			resp = IntegrationRestServiceFile.processARFile(ra);

		}
		return resp;
		
	}
	
	private void sendSalesOrder(SalesOrderDTO sales) {
		
		
		//Convertimos a CSV dependiendo del tipo: Ventas, Inventarios o Pagos
		//Invocamos WS de creación de ventas e inventarios (pagos is aplica)
		// Verificamos el resultado de la invocación
		
	}
	
	
}
