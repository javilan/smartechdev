package com.tdo.integration.ws;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tdo.integration.dto.CustomerDTO;
import com.tdo.integration.pojo.ClientesJson;
import com.tdo.integration.services.HTTPRequestService;
import com.tdo.integration.util.AppConstants;
import com.tdo.integration.util.NullValidator;

@Service("SOAPCustomerService")
public class SOAPCustomerService {
	
	@Autowired  
	HTTPRequestService HTTPRequestService; 
	
	public List<ClientesJson> getCustomers(String request) {
		
		List<ClientesJson> custList = new ArrayList<ClientesJson>();
		
		try {
			 String response = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_SALESPARTY_URL + "?invoke=", 
					                                            PayloadProducer.getSalesPartySOAPXmlContent(request),
					                                            AppConstants.CLOUD_CREDENTIALS);
			
			if(response != null) {
				JSONObject xmlJSONObj = XML.toJSONObject(response, true);
				JsonElement jelement = new JsonParser().parse(xmlJSONObj.toString());
				custList = getCustomersFromJson(jelement);	
				if(!custList.isEmpty()) {
					for(ClientesJson c : custList) {
						if(c.getClave() != null) {
							String accNbr = getCustomerAccount(c);
							if(!"".equals(accNbr)) {
								getCustomerProfile(c, accNbr);
								c.setClave("");
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return custList;
	}
	
	
	private String getCustomerAccount(ClientesJson c) {
		String accNbr = "";
		try {
			String response = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_CUSTOMERACCOUNT_URL + "?invoke=", 
	                PayloadProducer.getCustomerAccountSOAPXmlContent(c.getClave()),
	                AppConstants.CLOUD_CREDENTIALS);
			
			if(response != null) {
				JSONObject xmlJSONObj = XML.toJSONObject(response, true);
				JsonElement jelement = new JsonParser().parse(xmlJSONObj.toString());
				JsonObject jobject = jelement.getAsJsonObject();
				JsonElement result = jobject.get("env:Envelope").getAsJsonObject().get("env:Body").getAsJsonObject().get("ns0:findCustomerAccountResponse").getAsJsonObject().get("ns0:result").getAsJsonObject();
				
				try {
					JsonElement value = result.getAsJsonObject().get("ns2:Value").getAsJsonObject();
					accNbr = value.getAsJsonObject().get("ns2:AccountNumber").toString();
				}catch(Exception e) {
					return "";
				}
				
			}
			
		}catch(Exception e) {
			e.printStackTrace();
			return "";
		}
		return accNbr;
	}
	
	private ClientesJson getCustomerProfile(ClientesJson c, String accNbr) {
		
		try {
			String response = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_CUSTOMERPROFILE_URL + "?invoke=", 
	                PayloadProducer.getCustomerProfileSOAPXmlContent(c.getClave(), accNbr),
	                AppConstants.CLOUD_CREDENTIALS);
			
			if(response != null) {
				JSONObject xmlJSONObj = XML.toJSONObject(response, true);
				JsonElement jelement = new JsonParser().parse(xmlJSONObj.toString());
				JsonObject jobject = jelement.getAsJsonObject();
				JsonElement value = jobject.get("env:Envelope").getAsJsonObject().get("env:Body").getAsJsonObject().get("ns0:getActiveCustomerProfileResponse").getAsJsonObject().get("ns4:result").getAsJsonObject().getAsJsonObject().get("ns3:Value").getAsJsonObject();
				String credLimit = value.getAsJsonObject().get("ns3:CreditLimit").toString();
				String pTerms = value.getAsJsonObject().get("ns3:PaymentTerms").toString();
				String nil = "{\"xsi:nil\":\"true\"}";
				 if(credLimit.contains(nil)){
					 c.setLimite(0);
				 }else {
					 credLimit = credLimit.replaceAll("\"", "");
					 c.setLimite(Float.valueOf(credLimit));
					 
				 }
				 
				 if(pTerms.contains(nil)){
					 c.setDiasCredito(0);
				 }else {
					 pTerms = pTerms.replaceAll("\"", "");
					 c.setDiasCredito(Integer.valueOf(pTerms)); 
				 }
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return c;
	}
	
	public List<ClientesJson> getCustomersFromJson(JsonElement jelement){
		
		
		List<ClientesJson> clientesJson = new ArrayList<ClientesJson>();
		try {
			JsonObject jobject = jelement.getAsJsonObject();
			JsonElement soapEnvelope = jobject.get("env:Envelope").getAsJsonObject().get("env:Body").getAsJsonObject().get("ns0:findSalesPartyResponse").getAsJsonObject();
			JsonElement bdy = soapEnvelope.getAsJsonObject().get("ns2:result");
	
			if(bdy instanceof JsonArray) {
				JsonArray jsonarray = bdy.getAsJsonArray();
				for (int i = 0; i < jsonarray.size(); i++) {
					JsonElement op = jsonarray.get(i).getAsJsonObject();
					ClientesJson c = getClientFromJSON(op);
					if(c != null) {
					   clientesJson.add(c);
					}
				}
			}else {
				ClientesJson c = getClientFromJSON(bdy);
				clientesJson.add(c);	
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return clientesJson;
	}
	
private static ClientesJson getClientFromJSON(JsonElement root) {

		ClientesJson clientes = new ClientesJson();
		try {
			if(root != null) {
				JsonElement organizationParty = root.getAsJsonObject().get("ns1:OrganizationParty");
				JsonElement organizationProfile = organizationParty.getAsJsonObject().get("ns3:OrganizationProfile");
				JsonElement partySite = organizationParty.getAsJsonObject().get("ns3:PartySite");
			   
			    clientes.setCliId(0);
				clientes.setNombre(NullValidator.isNull(root.getAsJsonObject().get("ns1:PartyName").toString()));
				clientes.setRepresentante(NullValidator.isNull(organizationProfile.getAsJsonObject().get("ns3:PreferredContactName").toString()));
				String partyNumber = NullValidator.isNull(String.valueOf(root.getAsJsonObject().get("ns1:PartyNumber").toString()));
				clientes.setRfc(NullValidator.isNull(organizationProfile.getAsJsonObject().get("ns3:JgzzFiscalCode").toString()));

				if(partySite != null) {
					clientes.setDomicilio(NullValidator.isNull(partySite.getAsJsonObject().get("ns6:Address1").toString()));
					clientes.setNoExt(NullValidator.isNull(partySite.getAsJsonObject().get("ns6:Address2").toString()));
					clientes.setNoInt("");
					clientes.setLocalidad("");
					clientes.setCiudad(NullValidator.isNull(partySite.getAsJsonObject().get("ns6:City").toString()));
					clientes.setEstado(NullValidator.isNull(partySite.getAsJsonObject().get("ns6:State").toString()));
					clientes.setPais(NullValidator.isNull(partySite.getAsJsonObject().get("ns6:Country").toString()));
					clientes.setCodigoPostal(NullValidator.isNull(partySite.getAsJsonObject().get("ns6:PostalCode").toString()));
					clientes.setColonia(NullValidator.isNull(partySite.getAsJsonObject().get("ns6:Address3").toString()));
					String siteNumber = NullValidator.isNull(partySite.getAsJsonObject().get("ns6:PartySiteNumber").toString());
					clientes.setCurp(partyNumber + "/" + siteNumber);
				}else {
					clientes.setCurp(partyNumber + "/" + "UKNOWN");
				}
				
				clientes.setTelefono(NullValidator.isNull(organizationProfile.getAsJsonObject().get("ns3:PrimaryPhoneNumber").toString()));
				clientes.setCelular("");
				clientes.setMail(NullValidator.isNull(organizationProfile.getAsJsonObject().get("ns3:PrimaryEmailAddress").toString()));
				clientes.setComentario("");
				
				if("A".equals(NullValidator.isNull(root.getAsJsonObject().get("ns1:Status").toString())))
				     clientes.setStatus(1);
				else
					 clientes.setStatus(0);
			
				clientes.setLimite(0); 
				clientes.setPrecio(1); 
				clientes.setDiasCredito(0); 
				clientes.setRetener(false);
				clientes.setDesglosarIEPS(false);
				clientes.setClave(NullValidator.isNull(root.getAsJsonObject().get("ns1:PartyId").toString()));
				clientes.setFoto(null);
				clientes.setHuella(null);
				clientes.setMuestra(null);
				clientes.setUsoCfdi("");
				clientes.setComplementoList(new String[]{});
				
				if("".equals(clientes.getRepresentante())) {
					clientes.setRepresentante("Sin Representante");
				}
			}
			
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
		return clientes;
	}


	/* ORCHESTRATION:
	1. Create Location: Return locationId
	2. Create Customer Organization: Return partyId, partyNumber, partySiteId, partySiteNumber
	3. Create Customer Account: Return customerAccountId
	4. Create Customer Profile: Return customerAccountProfileId
	* 
	*/
	public List<CustomerDTO> insertCustomers(List<CustomerDTO> request) {
		
		if(request != null) {
			if(!request.isEmpty()) {
				for(CustomerDTO c : request) {
					
					c.setPartyNumber("");
					c.setAccountNumber("");
					c.setSiteNumber("");
					c.setMsg("");
					
					if("".equals(c.getAddress1())) {
						c.setMsg("Error en la dirección (address1) del registro " + c.getAccount());
						continue;
					}
					
					if("".equals(c.getName())) {
						c.setMsg("Error en el nombre del cliente para el registro " + c.getAccount());
						continue;
					}
					
					if("".equals(c.getAccount()) || "0".equals(c.getAccount())) {
						c.setMsg("Error en el valor del id del cliente " + c.getAccount());
						continue;
					}
					
					if("".equals(c.getCountry())) {
						c.setMsg("Error en el código del país para el cliente " + c.getAccount());
						continue;
					}else {
						boolean isValidCountry = Arrays.asList(AppConstants.countries).contains(c.getCountry());
						if(!isValidCountry) {
							c.setMsg("Error en el código del país. El código es inválido para el cliente " + c.getAccount());
							continue;
						}
					}
					
					if("".equals(c.getRfc())) {
						c.setRfc("DESCONOCIDO");
					}
					
					if("".equals(c.getPostalCode())) {
						c.setPostalCode("00000");
					}
					
					if("".equals(c.getCity())) {
						c.setCity("Desconocido");
					}
					
					if("".equals(c.getState())) {
						c.setState("Desconocido");
					}
 
					boolean isUpdate = false;
					String accNumber = "";
					if(c.getAccount().contains("_")) {
						String[] token = c.getAccount().split("_");
						if(token.length != 3) {
							c.setMsg("Se identificó un cambio para el registro " + c.getAccount() + " pero el formato del dato ce account es incorrecto");
							continue;
						}else {
							isUpdate = true;
							accNumber = token[1];
						}
					}
					
					
					try {
						

						if(isUpdate) {
							if(!"".equals(accNumber)) {
								//1. Retreive the PartyId from the account number
								String accountResponse = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_CREATEACCOUNT_URL + "?invoke=", 
		                                PayloadProducer.findAccountByAccountXmlContent(accNumber),
		                                AppConstants.CLOUD_CREDENTIALS);
								
								if(accountResponse !=  null) {
									JSONObject xmlJSONObj = XML.toJSONObject(accountResponse, true);
									JsonElement jelement = new JsonParser().parse(xmlJSONObj.toString());
					/*-- */			JsonObject jobject = jelement.getAsJsonObject();
									JsonElement soapEnvelope = jobject.get("env:Envelope").getAsJsonObject().get("env:Body").getAsJsonObject().get("ns0:findCustomerAccountResponse").getAsJsonObject();
									JsonElement result = soapEnvelope.getAsJsonObject().get("ns0:result");
									try {
									JsonElement value = result.getAsJsonObject().get("ns2:Value").getAsJsonObject();
									String partyId = NullValidator.isNull(value.getAsJsonObject().get("ns2:PartyId").toString());
									
										//2. Retreive the LocationId from Organization record
										String responseLocation = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_CREATEORGANIZATION_URL + "?invoke=", 
				                                PayloadProducer.findLocationByPartyIdXmlContent(partyId),
				                                AppConstants.CLOUD_CREDENTIALS);
										
										if(responseLocation != null) {
											xmlJSONObj = XML.toJSONObject(responseLocation, true);
											jelement = new JsonParser().parse(xmlJSONObj.toString());
											jobject = jelement.getAsJsonObject();
											soapEnvelope = jobject.get("env:Envelope").getAsJsonObject().get("env:Body").getAsJsonObject().get("ns0:findOrganizationResponse").getAsJsonObject();
											result = soapEnvelope.getAsJsonObject().get("ns3:result");
											try {
												value = result.getAsJsonObject().get("ns2:Value").getAsJsonObject();
												String locationId = NullValidator.isNull(value.getAsJsonObject().get("ns2:IdenAddrLocationId").toString());
												
												//3. Update Location
												HTTPRequestService.httpXmlRequest(AppConstants.SOAP_CREATELOCATION_URL + "?invoke=", 
						                                PayloadProducer.updateLocationUpdateSOAPXmlContent(locationId, c),
						                                AppConstants.CLOUD_CREDENTIALS);
												
												
												//4. Update RFC
												HTTPRequestService.httpXmlRequest(AppConstants.SOAP_CREATEORGANIZATION_URL + "?invoke=", 
						                                PayloadProducer.updateCustomerProfileSOAPXmlContent(partyId, c),
						                                AppConstants.CLOUD_CREDENTIALS);

											}catch(Exception e) {
												c.setMsg("Error: Los datos de localización no existen.");
												continue;
											}
											
											
										}else {
											c.setMsg("Error: Los datos de localización no existen.");
											continue;
										}
									
									}catch(Exception e) {
										c.setMsg("Error: El valor de AccountNumber no existe.");
										e.printStackTrace();
										continue;
									}
									
									
								}
							}
							
						}else {
							
                            
                            if(!"DESCONOCIDO".equals(c.getRfc())){

								String responseFindCustomer = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_FIND_ORGANIZATION_URL + "?invoke=", 
		                                PayloadProducer.findOrganizationByFiscalCodeSOAPXmlContent(c.getRfc().trim()),
		                                AppConstants.CLOUD_CREDENTIALS);
								
								
								if(responseFindCustomer != null) {
									JSONObject xmlJSONObj = XML.toJSONObject(responseFindCustomer, true);
									JsonElement jelement = new JsonParser().parse(xmlJSONObj.toString());
									JsonObject jobject = jelement.getAsJsonObject();
									JsonElement soapEnvelope = jobject.get("env:Envelope").getAsJsonObject().get("env:Body").getAsJsonObject().get("ns0:findOrganizationResponse").getAsJsonObject();
									JsonElement result = soapEnvelope.getAsJsonObject().get("ns3:result");
									JsonElement value = result.getAsJsonObject().get("ns2:Value");
									
									if(value != null) {
										String pId = null;
										if(value instanceof JsonArray) {
											JsonArray jsonarray = value.getAsJsonArray();
											for (int i = 0; i < jsonarray.size(); i++) {
												JsonElement partySite = jsonarray.get(i).getAsJsonObject().get("ns2:PartySite");
												JsonElement partyId = partySite.getAsJsonObject().get("ns1:PartyId");
												
												if(partyId != null) {
													pId = partyId.toString();
													break;
												}
											}
										}else {
											JsonElement partySite = value.getAsJsonObject().get("ns2:PartySite");
											JsonElement PartyId = partySite.getAsJsonObject().get("ns1:PartyId");
											
											if(PartyId != null) {
												pId = PartyId.toString().replace("\"", "");
											}
										}
										
										if(pId != null) {
											pId = pId.replaceAll("\"", "");
											
											String siteNumber ="";
											String accountNumber = "";
											
											String responseSiteNumber = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_FIND_ORGANIZATION_URL + "?invoke=", 
					                                PayloadProducer.findPartySiteXmlContent(pId),
					                                AppConstants.CLOUD_CREDENTIALS);
											
											if(responseSiteNumber != null) {
												xmlJSONObj = XML.toJSONObject(responseSiteNumber, true);
												jelement = new JsonParser().parse(xmlJSONObj.toString());
												jobject = jelement.getAsJsonObject();
												soapEnvelope = jobject.get("env:Envelope").getAsJsonObject().get("env:Body").getAsJsonObject().get("ns0:findOrganizationResponse").getAsJsonObject();
												result = soapEnvelope.getAsJsonObject().get("ns3:result");
												value = result.getAsJsonObject().get("ns2:Value");	
												if(value != null) {
													if(value instanceof JsonArray) {
														JsonArray jsonarray = value.getAsJsonArray();
														for (int i = 0; i < jsonarray.size(); i++) {
															JsonElement partySite = jsonarray.get(i).getAsJsonObject().get("ns2:PartySite");
															JsonElement siteNbr = partySite.getAsJsonObject().get("ns1:PartySiteNumber");
															
															if(siteNbr != null) {
																siteNumber = siteNbr.toString();
																break;
															}
														}
													}else {
														JsonElement partySite = value.getAsJsonObject().get("ns2:PartySite");
														JsonElement siteNbr = partySite.getAsJsonObject().get("ns1:PartySiteNumber");
														
														if(siteNbr != null) {
															siteNumber = siteNbr.toString().replace("\"", "");
														}
													}
													
													
												}else {
													c.setMsg("ERROR: El RFC se encuentra registrado, pero el siteNumber no existe");
													continue;	
												}
												
												
												String responseAccountNumber = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_CREATEACCOUNT_URL + "?invoke=", 
						                                PayloadProducer.findAccountNumberXmlContent(pId),
						                                AppConstants.CLOUD_CREDENTIALS);
												
												if(responseAccountNumber != null) {
													xmlJSONObj = XML.toJSONObject(responseAccountNumber, true);
													jelement = new JsonParser().parse(xmlJSONObj.toString());
													jobject = jelement.getAsJsonObject();
													soapEnvelope = jobject.get("env:Envelope").getAsJsonObject().get("env:Body").getAsJsonObject().get("ns0:findCustomerAccountResponse").getAsJsonObject();
													result = soapEnvelope.getAsJsonObject().get("ns0:result");
													value = result.getAsJsonObject().get("ns2:Value");	
													if(value != null) {
														if(value instanceof JsonArray) {
															JsonArray jsonarray = value.getAsJsonArray();
															for (int i = 0; i < jsonarray.size(); i++) {
																JsonElement accNbr = value.getAsJsonObject().get("ns2:AccountNumber");
																
																if(accNbr != null) {
																	accountNumber = accNbr.toString();
																	break;
																}
															}
														}else {
															JsonElement accNbr = value.getAsJsonObject().get("ns2:AccountNumber");
															if(accNbr != null) {
																accountNumber = accNbr.toString().replace("\"", "");
															}
														}	
													}else {
														c.setMsg("ERROR: El RFC se encuentra registrado, pero el accountNumber no existe");
														continue;	
													}
													
													
													c.setAccountNumber(accountNumber);
													c.setSiteNumber(siteNumber);
													c.setPartyNumber(pId);
													c.setMsg("El RFC ya existe. Se devuelve SiteNumber, AccountNumber y PartyId asociados. Este no es un mensaje de Error.");
		
												}else {
													c.setMsg("ERROR: El RFC se encuentra registrado, pero el accountNumber no existe");
													continue;
												}
	
											}else {
												c.setMsg("ERROR: El RFC se encuentra registrado, pero el siteNumber no existe");
												continue;	
											}
										}else {
											c.setMsg("ERROR: El RFC se encuentra registrado, pero el partyId no existe");
											continue;
										}
									}else {
										if(!insertCustomerRecord(c)) {
											  continue;
										  }
									}
									continue;
							  }else {
								  if(!insertCustomerRecord(c)) {
									  continue;
								  } 
							  }
							}else {
								if(!insertCustomerRecord(c)) {
									  continue;
								  }
							}
						}
					}catch(Exception e) {
						c.setMsg(e.getMessage());
						e.printStackTrace();
						continue;
					}
				}
			}
		}
		
		return request;
	}
	
	public boolean insertCustomerRecord(CustomerDTO c) {
		//1. Create Location: Return locationId
		String responseLocation = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_CREATELOCATION_URL + "?invoke=", 
                PayloadProducer.getLocationSOAPXmlContent(c),
                AppConstants.CLOUD_CREDENTIALS);
		
		if(responseLocation != null) {
			JSONObject xmlJSONObj = XML.toJSONObject(responseLocation, true);
			JsonElement jelement = new JsonParser().parse(xmlJSONObj.toString());
			JsonObject jobject = jelement.getAsJsonObject();
			JsonElement soapEnvelope = jobject.get("env:Envelope").getAsJsonObject().get("env:Body").getAsJsonObject().get("ns0:createLocationResponse").getAsJsonObject();
			JsonElement result = soapEnvelope.getAsJsonObject().get("ns2:result");
			JsonElement value = result.getAsJsonObject().get("ns1:Value").getAsJsonObject();
			String locationId = NullValidator.isNull(value.getAsJsonObject().get("ns1:LocationId").toString());
			locationId = locationId.replace("\"", "");
			if(!"".equals(locationId)) {
				
				//2. Create Customer Organization: Return partyId, partyNumber, partySiteId, partySiteNumber
				String responseOrganization = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_CREATEORGANIZATION_URL + "?invoke=", 
                        PayloadProducer.getCustomerOrganizationSOAPXmlContent(c, locationId),
                        AppConstants.CLOUD_CREDENTIALS);
				
				if(responseOrganization != null) {
					xmlJSONObj = XML.toJSONObject(responseOrganization, true);
					jelement = new JsonParser().parse(xmlJSONObj.toString());
					jobject = jelement.getAsJsonObject();
					soapEnvelope = jobject.get("env:Envelope").getAsJsonObject().get("env:Body").getAsJsonObject().get("ns0:createOrganizationResponse").getAsJsonObject();
					result = soapEnvelope.getAsJsonObject().get("ns3:result");
					value = result.getAsJsonObject().get("ns2:Value").getAsJsonObject();
					String partyId = NullValidator.isNull(value.getAsJsonObject().get("ns2:PartyId").toString());
					String partyNumber = NullValidator.isNull(value.getAsJsonObject().get("ns2:PartyNumber").toString());
					
					JsonElement partySite = value.getAsJsonObject().get("ns2:PartySite");
					String partySiteId = NullValidator.isNull(partySite.getAsJsonObject().get("ns1:PartySiteId").toString());
					String partySiteNumber = NullValidator.isNull(partySite.getAsJsonObject().get("ns1:PartySiteNumber").toString());
					partyId = partyId.replace("\"", "");
					partyNumber = partyNumber.replace("\"", "");
					partySiteNumber = partySiteNumber.replace("\"", "");
					
					if(!"".equals(partyId)) {
						c.setPartyNumber(partyNumber);
						c.setSiteNumber(partySiteNumber);
						
						//3. Create Customer Account: Return customerAccountId
						String responseAccount = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_CREATEACCOUNT_URL + "?invoke=", 
                                PayloadProducer.getCustomerAccountSOAPXmlContent(c, partyId, partySiteId, partyNumber, AppConstants.SET_ID),
                                AppConstants.CLOUD_CREDENTIALS);
						
						if(responseAccount != null) {
							xmlJSONObj = XML.toJSONObject(responseAccount, true);
							jelement = new JsonParser().parse(xmlJSONObj.toString());
							jobject = jelement.getAsJsonObject();
							soapEnvelope = jobject.get("env:Envelope").getAsJsonObject().get("env:Body").getAsJsonObject().get("ns0:createCustomerAccountResponse").getAsJsonObject();
							result = soapEnvelope.getAsJsonObject().get("ns0:result");
							value = result.getAsJsonObject().get("ns2:Value").getAsJsonObject();
							String customerAccountId = NullValidator.isNull(value.getAsJsonObject().get("ns2:CustomerAccountId").toString());
							String accountNumber = NullValidator.isNull(value.getAsJsonObject().get("ns2:AccountNumber").toString());
							customerAccountId = customerAccountId.replace("\"", "");
							accountNumber = accountNumber.replace("\"", "");
							
							if(!"".equals(customerAccountId)) {
								c.setAccountNumber(accountNumber);
								
								//4. Create Customer Profile: Return profileClassName
								String responseProfile = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_CREATEPROFILE_URL + "?invoke=", 
		                                PayloadProducer.getCustomerProfileServiceSOAPXmlContent(partyId, customerAccountId),
		                                AppConstants.CLOUD_CREDENTIALS);
								
								if(responseProfile != null) {
									xmlJSONObj = XML.toJSONObject(responseProfile, true);
									jelement = new JsonParser().parse(xmlJSONObj.toString());
									jobject = jelement.getAsJsonObject();
									soapEnvelope = jobject.get("env:Envelope").getAsJsonObject().get("env:Body").getAsJsonObject().get("ns0:createCustomerProfileResponse").getAsJsonObject();
									result = soapEnvelope.getAsJsonObject().get("ns4:result");
									value = result.getAsJsonObject().get("ns3:Value").getAsJsonObject();
									String profileClassName = NullValidator.isNull(value.getAsJsonObject().get("ns3:ProfileClassName").toString());
									profileClassName = profileClassName.replace("\"", "");
									
									if(!"".equals(customerAccountId)) {
										c.setProfileClassName(profileClassName);														
									}else {
										c.setMsg("Error en la creación del registro de PROFILE. El cliente " + c.getName() + " no pudo ser creado");
										return false;
									}
								}else {
									c.setMsg("Error en la creación del registro de PROFILE. El cliente " + c.getName() + " no pudo ser creado");
									return false;
								}
							}else {
								c.setMsg("Error en la creación del registro de ACCOUNT. El cliente " + c.getName() + " no pudo ser creado");
								return false;
							}
						}else {
							c.setMsg("Error en la creación del registro de ACCOUNT. El cliente " + c.getName() + " no pudo ser creado");
							return false;
						}
					}else {
						c.setMsg("Error en la creación del registro de ORGANIZACIÓN. El cliente " + c.getName() + " no pudo ser creado");
						return false;
					}	
				}else {
					c.setMsg("Error en la creación del registro de ORGANIZACIÓN. El cliente " + c.getName() + " no pudo ser creado");
					return false;
				}
			}else {
				c.setMsg("Error en la creación del registro de DIRECCIONES. El cliente " + c.getName() + " no pudo ser creado");
				return false;
			}
		}
		
		return true;
	}
	
	
	public String getRfcString(String rfc) {
		
		String response =  HTTPRequestService.httpXmlRequest(AppConstants.SOAP_FIND_ORGANIZATION_URL + "?invoke=", 
                PayloadProducer.findOrganizationByFiscalCodeSOAPXmlContent(rfc),
                AppConstants.CLOUD_CREDENTIALS);
		
		if(response != null) {
			JSONObject xmlJSONObj = XML.toJSONObject(response, true);
			JsonElement jelement = new JsonParser().parse(xmlJSONObj.toString());
			JsonObject jobject = jelement.getAsJsonObject();
			JsonElement soapEnvelope = jobject.get("env:Envelope").getAsJsonObject().get("env:Body").getAsJsonObject().get("ns0:findOrganizationResponse").getAsJsonObject();
			JsonElement result = soapEnvelope.getAsJsonObject().get("ns3:result");
			JsonElement value = result.getAsJsonObject().get("ns2:Value");
			if(value != null) {
				return "OK";
			}else {
				return null;
			}
		}else {
			return null;
		}
	}
	
	
}
