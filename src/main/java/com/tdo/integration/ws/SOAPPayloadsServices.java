package com.tdo.integration.ws;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.berdico.webservices.services.file.IntegrationRestServiceFile;
import com.tdo.integration.dto.ARLockboxImportCV;
import com.tdo.integration.dto.LockboxDTO;
import com.tdo.integration.dto.PayloadDTO;
import com.tdo.integration.dto.SalesOrderDTO;
import com.tdo.integration.util.AppConstants;

@Service("SOAPPayloadsServices")
public class SOAPPayloadsServices {

	@Autowired
	IntegrationRestServiceFile IntegrationRestServiceFile;
	
	public String createPayloadOrder(List<PayloadDTO> payload) {
		
	String resp = null;
	List<ARLockboxImportCV> payloads = new ArrayList<ARLockboxImportCV>();
	
		if(payload != null) {
			if(!payload.isEmpty()) {
				
				int size = 0;
				String date = "";
				for(PayloadDTO c: payload) {
					ARLockboxImportCV d = new ARLockboxImportCV();
					size++;
					date = (c.getTransactionDate().substring(2, 4)) + (c.getTransactionDate().substring(5, 7)) + (c.getTransactionDate().substring(8, 10));
					
					d.setRecord_Type(AppConstants.PAGOS_RECORDTYPE);
					d.setItem_Number(String.valueOf(size));
					d.setRemittance_Amount(String.valueOf(c.getTotalAPagar()));
					d.setReceipt_Number(c.getPayloadIdPOS());
					d.setReceipt_Date(date);
					d.setCurrency(c.getCurrencyCode());
					d.setCustomer_Account_Number(c.getAccountNumber());
					d.setCustomer_Site(c.getSiteNumber());
					
					if(c.getFormaDePago().contentEquals("SANTANDER_BERDICO")) {
						d.setReceipt_Method(AppConstants.PAGOS_RECEIPTMETHOD_SANTANDER);
						d.setLockbox_Number(AppConstants.PAGOS_LOCKBOXNUMBER_SANTANDER);
					}else if(c.getFormaDePago().contentEquals("EFECTIVO_KAUFFMAN")) {
						d.setReceipt_Method(AppConstants.PAGOS_RECEIPTMETHOD_EFECTIVO);
						d.setLockbox_Number(AppConstants.PAGOS_LOCKBOXNUMBER_EFECTIVO);
					}
					
					d.setTransaction_Reference_1(c.getVentaIdPOS());
					d.setApplied_Amount_1(String.valueOf(c.getDeposito()));
					d.setEND("END");
					
					payloads.add(d);
				}
				

			}	
			
			resp = IntegrationRestServiceFile.processARLockBoxImportCFile(payloads);
			return resp;
			
		}
		return resp;
		
	}
	
	private void sendPayloadOrder(PayloadDTO payload) {
		
		
		//Convertimos a CSV dependiendo del tipo: Ventas, Inventarios o Pagos
		//Invocamos WS de creación de ventas e inventarios (pagos is aplica)
		// Verificamos el resultado de la invocación
		
	}
}
