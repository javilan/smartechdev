package com.tdo.integration.ws;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tdo.integration.dto.ItemMasterKauffmanDTO;
import com.tdo.integration.services.HTTPRequestService;
import com.tdo.integration.util.AppConstants;
import com.tdo.integration.util.NullValidator;

@Service("SOAPItemMasterService")
public class SOAPItemMasterService {
	
	@Autowired  
	HTTPRequestService HTTPRequestService; 
	
	final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	final SimpleDateFormat formatterUTC = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	final SimpleDateFormat lv_formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss z'('Z')'");
	
	Date requestStartDate = null;
	Date requestEndDate = null;
	String dateStartUTC = "";
	String dateEndUTC = "";
	
	
	public List<ItemMasterKauffmanDTO> getItems(String dateStart, String dateEnd, String start, String size, String type, String entidad, String itemType) {
		
		sdf.setTimeZone(TimeZone.getTimeZone("America/Mexico_City"));
		formatterUTC.setTimeZone(TimeZone.getTimeZone("UTC"));

		List<ItemMasterKauffmanDTO> itemList = new ArrayList<ItemMasterKauffmanDTO>();
		
		if("".equals(start)) return itemList;
		if("".equals(size)) return itemList;
		if(!StringUtils.isNumeric(start)) return itemList;
		if(!StringUtils.isNumeric(size)) return itemList;
		
		int startInt = Integer.valueOf(start);
		int sizeInt = Integer.valueOf(size);
		
		String dateType = "";
		if("U".equals(type)) {
			dateType = "LastUpdateDateTime";
		}else {
			dateType = "CreationDateTime";
		}
		
		String entity = "";
		List<String> entityList = null;
		if("KAUFFMAN".equals(entidad)) {
			entity = "BC,K015";
		}else {
			entity = "E001,AS_01";
		}
		entityList = Arrays.asList(entity.split(","));
		
		if(!"".equals(itemType)) {
			if("mica".equals(itemType)) {
				itemType = "<![CDATA[A&S LENTES]]>";
			}
			if("accesorio".equals(itemType)) {
				itemType = "<![CDATA[A&S_ACCESORIOS]]>";
			}
			if("armazon".equals(itemType)) {
				itemType = "<![CDATA[A&S_ARMAZONES]]>";
			}
		}

		try {
			
			requestStartDate = sdf.parse(dateStart);
			dateStartUTC = formatterUTC.format(requestStartDate);
			
			requestEndDate = sdf.parse(dateEnd);
			dateEndUTC = formatterUTC.format(requestEndDate);
			
			 String startFrom = "0";
			 String sizeTo = "1000";
			 String response = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_ITEMMASTER_URL + "?invoke=", 
					                                            PayloadProducer.getItemMasterSOAPXmlContent(dateStartUTC, dateEndUTC, startFrom, sizeTo, dateType, entityList, itemType),
					                                            AppConstants.CLOUD_CREDENTIALS);
			
			if(response != null) {
				JSONObject xmlJSONObj = XML.toJSONObject(response, true);
				JsonElement jelement = new JsonParser().parse(xmlJSONObj.toString());
				itemList = getItemsFromJson(jelement, type);	
			}
		} catch (Exception e) {
			e.printStackTrace();	
			return itemList;
		}

		if(itemList.size() > 0) {
			
			//remove duplicates
			 Map<String, ItemMasterKauffmanDTO> set = new HashMap<String, ItemMasterKauffmanDTO>(); 
			 for(ItemMasterKauffmanDTO o : itemList) {
				 ItemMasterKauffmanDTO obj = set.get(o.getSku().trim());
				 if(obj == null) {
					 set.put(o.getSku().trim(), o);
				 }
			 }
		
			 itemList.clear();
			 itemList = new ArrayList<ItemMasterKauffmanDTO>(set.values());
			
			if(itemList.size() >= startInt) {
				ArrayList<ItemMasterKauffmanDTO> al2 = new ArrayList<ItemMasterKauffmanDTO>();
				int arrayLimit = itemList.size();
				if(arrayLimit > (startInt + sizeInt)) {
					al2 = new ArrayList<ItemMasterKauffmanDTO>(itemList.subList(startInt, (startInt + sizeInt)));
				}else {
					al2 = new ArrayList<ItemMasterKauffmanDTO>(itemList.subList(startInt, itemList.size()));
				}
				return al2;
			}else {
				return new ArrayList<ItemMasterKauffmanDTO>();
			}
		}
		return new ArrayList<ItemMasterKauffmanDTO>();

	}
	
	
	public List<ItemMasterKauffmanDTO> getItemsFromJson(JsonElement jelement, String type){
		
		JsonObject jobject = jelement.getAsJsonObject();
		JsonElement soapEnvelope = jobject.get("env:Envelope").getAsJsonObject().get("env:Body").getAsJsonObject().get("ns0:findItemResponse");
		JsonElement bdy = soapEnvelope.getAsJsonObject().get("ns2:result").getAsJsonObject().get("ns0:Value");
		
		List<ItemMasterKauffmanDTO> articulosJson = new ArrayList<ItemMasterKauffmanDTO>();
		if(bdy != null) {
			if(bdy instanceof JsonArray) {
				JsonArray jsonarray = bdy.getAsJsonArray();
				for (int i = 0; i < jsonarray.size(); i++) {
					JsonElement op = jsonarray.get(i).getAsJsonObject();
					ItemMasterKauffmanDTO c = getItemFromJSON(op);
					if(c != null) {
						try {
							
							if("N".equals(type)) {
								String creationDate = c.getCreationDate().substring(0,19);
								Date cDate = localTimeZoneDate(creationDate);
								Date cDateOffset = DateUtils.addMinutes(cDate, 10);
								
								String updatedDate = c.getModifiedDate().substring(0,19);;
								Date uDate = localTimeZoneDate(updatedDate);
								
								if(cDate.before(requestStartDate) || cDate.after(requestEndDate)) {
									continue;
								}
								
								c.setType(type);
								int compare = uDate.compareTo(cDateOffset);
								if(compare < 0){
									c.setCreationDate(lv_formatter.format(cDate));
									c.setModifiedDate(lv_formatter.format(uDate));
									articulosJson.add(c);
								}
							}
							
							if("U".equals(type)) {
								String updatedDate = c.getModifiedDate().substring(0,19);
								Date uDate = localTimeZoneDate(updatedDate);
								Date cDateOffset = DateUtils.addMinutes(uDate, -10);
								
								if(uDate.before(requestStartDate) || uDate.after(requestEndDate)) {
									continue;
								}
								
								String creationDate = c.getCreationDate().substring(0,19);
								Date cDate = localTimeZoneDate(creationDate);
	
								c.setType(type);
								int compare = cDate.compareTo(cDateOffset);
								if(compare < 0){
									c.setCreationDate(lv_formatter.format(cDate));
									c.setModifiedDate(lv_formatter.format(uDate));
									articulosJson.add(c);
								}
							}
							
						}catch(Exception e) {
							e.printStackTrace();
							continue;
						}
						
						 
					}
				}
			}else {
				ItemMasterKauffmanDTO c = getItemFromJSON(bdy);
				articulosJson.add(c);	
			}
		}
		return articulosJson;
	}
	
	
	private ItemMasterKauffmanDTO getItemFromJSON(JsonElement value) {
		ItemMasterKauffmanDTO articulo = new ItemMasterKauffmanDTO();
		
		try {
			
			articulo.setSku(NullValidator.isNull(value.getAsJsonObject().get("ns1:ItemNumber").toString()));
			articulo.setOms300(NullValidator.isNull(value.getAsJsonObject().get("ns1:ItemDescription").toString()));
			articulo.setOms301(NullValidator.isNull(value.getAsJsonObject().get("ns1:ItemDescription").toString()));
			articulo.setVoorrart(NullValidator.isNull(value.getAsJsonObject().get("ns1:StockEnabledFlag").toString()));

			articulo.setBackordart(NullValidator.isNull(value.getAsJsonObject().get("ns1:BackOrderableFlag").toString()));
			articulo.setSerieart(NullValidator.isNull(value.getAsJsonObject().get("ns1:SerialStatusEnabledFlag").toString()));
			articulo.setOrderink(NullValidator.isNull(value.getAsJsonObject().get("ns1:BackOrderableFlag").toString()));
			articulo.setGip(NullValidator.isNull(value.getAsJsonObject().get("ns1:ListPrice").toString()));
			String gip = NullValidator.isNull(value.getAsJsonObject().get("ns1:ListPrice").toString());
			
			if("".equals(gip)) {
				articulo.setGip("0");
			}else {
				JSONObject obj = new JSONObject(value.getAsJsonObject().get("ns1:ListPrice").toString());
				articulo.setGip(String.valueOf(obj.getDouble("content")));
			}
			
			articulo.setNettogew(NullValidator.isNull(value.getAsJsonObject().get("ns1:MaximumLoadWeight").toString()));
			articulo.setVolume(NullValidator.isNull(value.getAsJsonObject().get("ns1:InternalVolume").toString()));
			articulo.setCreationDate(NullValidator.isNull(value.getAsJsonObject().get("ns1:CreationDateTime").toString()));
			articulo.setModifiedDate(NullValidator.isNull(value.getAsJsonObject().get("ns1:LastUpdateDateTime").toString()));
			articulo.setuFechaAlta(NullValidator.isNull(value.getAsJsonObject().get("ns1:CreationDateTime").toString()));
			
			JsonElement cat = value.getAsJsonObject().get("ns1:ItemCategory");
			if(cat != null) {
				if(cat instanceof JsonArray) {
					JsonArray jsonarray = cat.getAsJsonArray();
					for (int i = 0; i < jsonarray.size(); i++) {
						JsonElement op = jsonarray.get(i).getAsJsonObject();
						String itemCatalog = NullValidator.isNull(op.getAsJsonObject().get("ns1:ItemCatalog").toString());
						String categoryName = NullValidator.isNull(op.getAsJsonObject().get("ns1:CategoryName").toString());
						if(!"".equals(itemCatalog)) {
							switch(itemCatalog) {
							
							   case "A&S_Material": articulo.setU_material(categoryName); break;
							   case "A&S_Color": articulo.setU_colorprove(categoryName); break;
							   case "A&S_Medida": articulo.setU_medidaaro(categoryName); break;
							   case "A&S_Tamaño": articulo.setU_forma(categoryName); break;
							   case "A&S_Genero": articulo.setU_paragenero(categoryName); break;
							   case "A&S_Tipo_Mica": articulo.setU_clasedemic(categoryName); break;
							
							   case "u_CVEPRODSERV": articulo.setU_CVEPRODSERV(categoryName); break;
							   case "u_CVEUNIDAD": articulo.setU_CVEUNIDAD(categoryName); break;
							   case "u_artilinea": articulo.setU_artilinea(categoryName); break;
							   case "u_cdesvmica": articulo.setU_cdesvmica(categoryName); break;
							   case "u_clasedemic": articulo.setU_clasedemic(categoryName); break;
							   case "u_colorprove": articulo.setU_colorprove(categoryName); break;
							   case "u_convenio": articulo.setU_convenio(categoryName); break;
							   case "u_cprincaro": articulo.setU_cprincaro(categoryName); break;
							   case "u_cprincmica": articulo.setU_cprincmica(categoryName); break;
							   case "u_csecundaro": articulo.setU_csecundaro(categoryName); break;
							   case "u_curvabase": articulo.setU_curvabase(categoryName); break;
							   case "u_descuento": articulo.setU_descuento(categoryName); break;
							   case "u_familia": articulo.setU_familia(categoryName); break;
							   case "u_forma": articulo.setU_forma(categoryName); break;
							   case "u_gtiaext": articulo.setU_gtiaext(categoryName); break;
							   case "u_marca": articulo.setU_marca(categoryName); break;
							   case "u_material": articulo.setU_material(categoryName); break;
							   case "u_medidaaro": articulo.setU_medidaaro(categoryName); break;
							   case "u_medidapuen": articulo.setU_medidapuen(categoryName); break;
							   case "u_medidavari": articulo.setU_medidavari(categoryName); break;
							   case "u_modelodela": articulo.setU_modelodela(categoryName); break;
							   case "u_nuevo": articulo.setU_nuevo(categoryName); break;
							   case "u_paragenero": articulo.setU_paragenero(categoryName); break;
							   case "u_pedido": articulo.setU_pedido(categoryName); break;
							   case "u_promocion": articulo.setU_promocion(categoryName); break;
							   case "u_tipo": articulo.setU_tipo(categoryName); break;
							   case "u_tipoarmazo": articulo.setU_tipoarmazo(categoryName); break;
							   case "u_tipoproduc": articulo.setU_tipoproduc(categoryName); break;
							   case "u_desclarga": articulo.setuDesclarga(categoryName); break;
							   case "u_fechalimit": articulo.setuFechalimit(categoryName); break;
							   case "GrupoArticulo": articulo.setArtgrp(categoryName); break;
							   case "UnidadPaqVta": articulo.setEenhverk(categoryName); break;
							   case "CantidadPaqVta": articulo.setAantverp(categoryName); break;
							   case "PrecioPaqVta": articulo.setVerkpverp(categoryName); break;
							   case "DescCorta": articulo.setKortoms(categoryName); break;
							   case "IvaRepercutido": articulo.setBtwverk(categoryName); break;
							   case "IVASoportado": articulo.setBtwink(categoryName); break;
							   default :
							        break;
							}
						}
					}
				}else {
					String itemCatalog = NullValidator.isNull(cat.getAsJsonObject().get("ns1:ItemCatalog").toString());
					String categoryName = NullValidator.isNull(cat.getAsJsonObject().get("ns1:CategoryName").toString());
					if(!"".equals(itemCatalog)) {
						switch(itemCatalog) {
						
						   case "A&S_Material": articulo.setU_material(categoryName); break;
						   case "A&S_Color": articulo.setU_colorprove(categoryName); break;
						   case "A&S_Medida": articulo.setU_medidaaro(categoryName); break;
						   case "A&S_Tamaño": articulo.setU_forma(categoryName); break;
						   case "A&S_Genero": articulo.setU_paragenero(categoryName); break;
						   case "A&S_Tipo_Mica": articulo.setU_clasedemic(categoryName); break;
						   
						   case "u_CVEPRODSERV": articulo.setU_CVEPRODSERV(categoryName); break;
						   case "u_CVEUNIDAD": articulo.setU_CVEUNIDAD(categoryName); break;
						   case "u_artilinea": articulo.setU_artilinea(categoryName); break;
						   case "u_cdesvmica": articulo.setU_cdesvmica(categoryName); break;
						   case "u_clasedemic": articulo.setU_clasedemic(categoryName); break;
						   case "u_colorprove": articulo.setU_colorprove(categoryName); break;
						   case "u_convenio": articulo.setU_convenio(categoryName); break;
						   case "u_cprincaro": articulo.setU_cprincaro(categoryName); break;
						   case "u_cprincmica": articulo.setU_cprincmica(categoryName); break;
						   case "u_csecundaro": articulo.setU_csecundaro(categoryName); break;
						   case "u_curvabase": articulo.setU_curvabase(categoryName); break;
						   case "u_descuento": articulo.setU_descuento(categoryName); break;
						   case "u_familia": articulo.setU_familia(categoryName); break;
						   case "u_forma": articulo.setU_forma(categoryName); break;
						   case "u_gtiaext": articulo.setU_gtiaext(categoryName); break;
						   case "u_marca": articulo.setU_marca(categoryName); break;
						   case "u_material": articulo.setU_material(categoryName); break;
						   case "u_medidaaro": articulo.setU_medidaaro(categoryName); break;
						   case "u_medidapuen": articulo.setU_medidapuen(categoryName); break;
						   case "u_medidavari": articulo.setU_medidavari(categoryName); break;
						   case "u_modelodela": articulo.setU_modelodela(categoryName); break;
						   case "u_nuevo": articulo.setU_nuevo(categoryName); break;
						   case "u_paragenero": articulo.setU_paragenero(categoryName); break;
						   case "u_pedido": articulo.setU_pedido(categoryName); break;
						   case "u_promocion": articulo.setU_promocion(categoryName); break;
						   case "u_tipo": articulo.setU_tipo(categoryName); break;
						   case "u_tipoarmazo": articulo.setU_tipoarmazo(categoryName); break;
						   case "u_tipoproduc": articulo.setU_tipoproduc(categoryName); break;
						   case "u_desclarga": articulo.setuDesclarga(categoryName); break;
						   case "u_fechalimit": articulo.setuFechalimit(categoryName); break;
						   case "GrupoArticulo": articulo.setArtgrp(categoryName); break;
						   case "UnidadPaqVta": articulo.setEenhverk(categoryName); break;
						   case "CantidadPaqVta": articulo.setAantverp(categoryName); break;
						   case "PrecioPaqVta": articulo.setVerkpverp(categoryName); break;
						   case "DescCorta": articulo.setKortoms(categoryName); break;
						   case "IvaRepercutido": articulo.setBtwverk(categoryName); break;
						   case "IVASoportado": articulo.setBtwink(categoryName); break;
						   default :
						        break;
						}
					}
				}
			}
			
			JsonElement ff = value.getAsJsonObject().get("ns1:ItemDFF");
			if(ff != null) {
					articulo.setStatistnr(NullValidator.isNull(ff.getAsJsonObject().get("ns9:fechaRecibo").toString()));
					articulo.setSamengest("0"); 
					articulo.setArsoort("I");
					//articulo.setStatistnr(NullValidator.isNull(ff.getAsJsonObject().get("ns9:STATISTNR").toString()));
					//articulo.setOmzstatjn(NullValidator.isNull(ff.getAsJsonObject().get("ns9:OMZSTATJN").toString()));

			}

		}catch(Exception e) {
			e.printStackTrace();
		}

   	    return articulo;
	}
	
	private Date localTimeZoneDate(String utcDate) {
		try {
			dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
			Date dateUtc = dateFormat.parse(utcDate);
			String localDate = lv_formatter.format(dateUtc);
			return lv_formatter.parse(localDate);
		}catch(Exception e){
			return null;
		}
		
	}

}
