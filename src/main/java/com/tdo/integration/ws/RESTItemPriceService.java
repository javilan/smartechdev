package com.tdo.integration.ws;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tdo.integration.dto.ItemMasterKauffmanDTO;
import com.tdo.integration.dto.ItemPriceDTO;
import com.tdo.integration.dto.PriceListKauffmanDTO;
import com.tdo.integration.services.HTTPRequestService;
import com.tdo.integration.util.AppConstants;

@Service("RESTItemPriceService")
public class RESTItemPriceService {
	
	@Autowired
	HTTPRequestService HTTPRequestService;
	
	private final String  CALCULATION_METHOD_COST = "PRICE";
	final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	final SimpleDateFormat requestDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	final SimpleDateFormat lv_formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss z'('Z')'");
	
	public List<PriceListKauffmanDTO> getItemPriceList(String dateStart, String dateEnd, String start, String size, String type, String entidad){
		
       List<PriceListKauffmanDTO> itemList = new ArrayList<PriceListKauffmanDTO>();
		
		if("".equals(start)) return itemList;
		if("".equals(size)) return itemList;
		if(!StringUtils.isNumeric(start)) return itemList;
		if(!StringUtils.isNumeric(size)) return itemList;
		
		int startInt = Integer.valueOf(start);
		int sizeInt = Integer.valueOf(size);
		
		String dateType = "";
		if("U".equals(type)) {
			dateType = "LastUpdateDate";
		}else {
			dateType = "CreationDate";
		}
		
		 Date requestStartDate = null;
		 Date requestEndDate = null;
		 
		try {
			
			requestStartDate = requestDateFormat.parse(dateStart);
			requestEndDate = requestDateFormat.parse(dateEnd);
			 
		String resp = HTTPRequestService.httpRestRequest(PayloadProducer.findPriceListJsonContent(entidad), AppConstants.CLOUD_CREDENTIALS);
		if(resp != null) {
			JSONObject jsnobject = new JSONObject(resp);
			JSONArray jsonArray = jsnobject.getJSONArray("items");
			if(jsonArray != null) {
				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject o = jsonArray.getJSONObject(i);
					
					PriceListKauffmanDTO pl = new PriceListKauffmanDTO();
					pl.setPriceListName(o.get("PriceListName").toString());
					pl.setBusinessUnit(o.get("BusinessUnit").toString());
					pl.setStatusCode(o.get("StatusCode").toString());
					pl.setRequestType(type);
					List<ItemPriceDTO> items = new ArrayList<ItemPriceDTO>();

					  String listId = o.get("PriceListId").toString();
					  String itemPriceResp = null;
					  
					  if("U".equals(type)) {
						itemPriceResp = HTTPRequestService.httpRestRequest(PayloadProducer.findAllItemPriceListJsonContent(listId, start, size, dateStart, dateEnd, dateType), AppConstants.CLOUD_CREDENTIALS);
					  }else {
						itemPriceResp = HTTPRequestService.httpRestRequest(PayloadProducer.findNewItemPriceListJsonContent(listId, start, size, dateStart, dateEnd, dateType), AppConstants.CLOUD_CREDENTIALS);
					  }
					  
					  
					  if(itemPriceResp != null) {
						  JSONObject jobject = new JSONObject(itemPriceResp);
						  JSONArray jArray = jobject.getJSONArray("items");
						  if(jArray != null) {
							  for (int j = 0; j < jArray.length(); j++) {
								  JSONObject q = jArray.getJSONObject(j); 
								  JSONArray crgArray = q.getJSONArray("charges");
								  if(crgArray != null) {
									  for (int k = 0; k < crgArray.length(); k++) {
										  JSONObject r = crgArray.getJSONObject(k);
										  try {
											  if(CALCULATION_METHOD_COST.equals(r.get("CalculationMethodCode").toString())) {
												  
												  ItemPriceDTO idto = new ItemPriceDTO();
												  
												  idto.setItemNumber(q.get("Item").toString());
												  idto.setPrice(r.get("BasePrice").toString());

												  if("N".equals(type)) {
														String creationDate = r.get("CreationDate").toString().substring(0,19);
														Date cDate = localTimeZoneDate(creationDate);
														Date cDateOffset = DateUtils.addMinutes(cDate, 10);
														
														String updatedDate = r.get("LastUpdateDate").toString().substring(0,19);
														Date uDate = localTimeZoneDate(updatedDate);
														
														if(cDate.before(requestStartDate) || cDate.after(requestEndDate)) {
															continue;
														}
														
														int compare = uDate.compareTo(cDateOffset);
														if(compare < 0){
															idto.setCreationDate(lv_formatter.format(cDate));
															idto.setLastUpdate(lv_formatter.format(uDate));
															items.add(idto);
														}
													}
													
													if("U".equals(type)) {
														String updatedDate = r.get("LastUpdateDate").toString().substring(0,19);;
														Date uDate = localTimeZoneDate(updatedDate);
														Date cDateOffset = DateUtils.addMinutes(uDate, -10);
														
														if(uDate.before(requestStartDate) || uDate.after(requestEndDate)) {
															continue;
														}
														
														String creationDate = r.get("CreationDate").toString().substring(0,19);
														Date cDate = localTimeZoneDate(creationDate);
														
														int compare = cDate.compareTo(cDateOffset);
														if(compare < 0){
															idto.setCreationDate(lv_formatter.format(cDate));
															idto.setLastUpdate(lv_formatter.format(uDate));
															items.add(idto);
														}
													}
												  
											  }
										  }catch(Exception e) {
											  continue;
										  }

										  
									  }
								  }
								  
							  }
							  pl.setItems(items);
						  }
					  }
					  
					  if(pl.getItems() != null) {
						  if(pl.getItems().size() > 0) {
							  itemList.add(pl);
						  }
					  }
				}
			}
		}

		}catch(Exception e) {
			e.printStackTrace();
			return new ArrayList<PriceListKauffmanDTO>();
		}
		
		if(itemList.size() > 0) {
			if(itemList.size() >= startInt) {
				ArrayList<PriceListKauffmanDTO> al2 = new ArrayList<PriceListKauffmanDTO>();
				int arrayLimit = itemList.size();
				if(arrayLimit > (startInt + sizeInt)) {
					al2 = new ArrayList<PriceListKauffmanDTO>(itemList.subList(startInt, (startInt + sizeInt)));
				}else {
					al2 = new ArrayList<PriceListKauffmanDTO>(itemList.subList(startInt, itemList.size()));
				}
				return al2;
			}else {
				return new ArrayList<PriceListKauffmanDTO>();
			}
		}
		return new ArrayList<PriceListKauffmanDTO>();

	}
	
	private Date localTimeZoneDate(String utcDate) {
		try {
			dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
			Date dateUtc = dateFormat.parse(utcDate);
			String localDate = lv_formatter.format(dateUtc);
			return lv_formatter.parse(localDate);
		}catch(Exception e){
			return null;
		}
		
	}

}
