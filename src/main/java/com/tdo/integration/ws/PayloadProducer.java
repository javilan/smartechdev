package com.tdo.integration.ws;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.tdo.integration.dto.CustomerDTO;
import com.tdo.integration.dto.InventoryTransactionDTO;
import com.tdo.integration.util.AppConstants;

public class PayloadProducer {
	
	static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	public static String getSalesPartySOAPXmlContent(String request) {
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/crmCommon/salesParties/salesPartiesService/types/\" xmlns:typ1=\"http://xmlns.oracle.com/adf/svc/types/\"> " + 
				"   <soapenv:Header/> " + 
				"   <soapenv:Body> " + 
				"<ns2:findSalesParty xmlns=\"http://xmlns.oracle.com/adf/svc/types/\" xmlns:ns2=\"http://xmlns.oracle.com/apps/crmCommon/salesParties/salesPartiesService/types/\"> " +
				" <ns2:findCriteria> " + 
				" <fetchStart>0</fetchStart> " + 
				" <fetchSize>50</fetchSize> " + 
				" <filter> " + 
				" <group> " + 
				//" <item> " + 
				//" <attribute>PartyNumber</attribute> " + 
				//" <operator>=</operator> " + 
				//" <value>" + request + "</value> " +
				//" </item> " + 
				" <item> " +
		        " <attribute>OrganizationParty</attribute> " +
                " <nested> " +
                "   <group> " +
                "       <item> " +
                "           <attribute>CreationDate</attribute> " +
                "           <operator>ONORAFTER</operator> " +
                "           <value>" + request + "</value> " +
                "       </item> " +
                "   </group> " +
                " </nested> " +
                " </item> " +
				" </group> " + 
				" </filter> " + 
				" <findAttribute>PartyId</findAttribute> " + 
				" <findAttribute>PartyName</findAttribute> " + 
				" <findAttribute>PartyNumber</findAttribute> " + 
				" <findAttribute>PartyType</findAttribute> " + 
				" <findAttribute>Status</findAttribute> " + 
				" <findAttribute>OrganizationParty</findAttribute> " + 
				" <childFindCriteria> " + 
				" <findAttribute>CreatedBy</findAttribute> " + 
				" <findAttribute>CreationDate</findAttribute> " + 
				" <findAttribute>JgzzFiscalCode</findAttribute> " + 
				" <childAttrName>OrganizationParty</childAttrName> " + 
				" <findAttribute>PartySite</findAttribute> " + 
				" <findAttribute>OrganizationProfile</findAttribute> " + 
				" <childFindCriteria> " + 
				" <findAttribute>Address1</findAttribute> " + 
				" <findAttribute>Address2</findAttribute> " + 
				" <findAttribute>Address3</findAttribute> " + 
				" <findAttribute>City</findAttribute> " + 
				" <findAttribute>Country</findAttribute> " + 
				" <findAttribute>PostalCode</findAttribute> " + 
				" <findAttribute>PartySiteNumber</findAttribute> " + 
				" <findAttribute>State</findAttribute> " + 
				" <childAttrName>PartySite</childAttrName> " + 
				" </childFindCriteria> " + 
				" <childFindCriteria> " + 
				" <findAttribute>PreferredContactName</findAttribute> " + 
				" <findAttribute>JgzzFiscalCode</findAttribute> " + 
				" <findAttribute>CurrencyCode</findAttribute> " + 
				" <findAttribute>PrimaryPhoneNumber</findAttribute> " + 
				" <findAttribute>PrimaryEmailAddress</findAttribute> " + 
				" <childAttrName>OrganizationProfile</childAttrName> " + 
				" </childFindCriteria> " + 
				" </childFindCriteria> " + 
				" </ns2:findCriteria> " + 
				" </ns2:findSalesParty> " +
				" </soapenv:Body> " + 
				" </soapenv:Envelope> ";
		return SOAPRequest;
	}
	
	public static String getCustomerProfileSOAPXmlContent(String partyId, String accountNbr){
		partyId = partyId.replace("\"", "");	
		accountNbr = accountNbr.replace("\"", "");
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/financials/receivables/customers/customerProfileService/types/\" xmlns:cus=\"http://xmlns.oracle.com/apps/financials/receivables/customers/customerProfileService/\" xmlns:cus1=\"http://xmlns.oracle.com/apps/financials/receivables/customerSetup/customerProfiles/model/flex/CustomerProfileDff/\" xmlns:cus2=\"http://xmlns.oracle.com/apps/financials/receivables/customerSetup/customerProfiles/model/flex/CustomerProfileGdf/\"> " + 
				"   <soapenv:Header/> " + 
				"   <soapenv:Body>" + 
				"   <typ:getActiveCustomerProfile xmlns:typ=\"http://xmlns.oracle.com/apps/financials/receivables/customers/customerProfileService/types/\" xmlns:cus=\"http://xmlns.oracle.com/apps/financials/receivables/customers/customerProfileService/\"> " +
			    "    <typ:customerProfile> " +
			    "       <cus:AccountNumber>" + accountNbr + "</cus:AccountNumber> " +
			    "       <cus:PartyId>" + partyId + "</cus:PartyId> " +
			    "    </typ:customerProfile> " +
			    " </typ:getActiveCustomerProfile>" +
				" </soapenv:Body> " + 
				" </soapenv:Envelope> ";
		return SOAPRequest;
	}
	
	public static String getCustomerAccountSOAPXmlContent(String partyId){
		partyId = partyId.replace("\"", "");		
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/customerAccountService/applicationModule/types/\" xmlns:typ1=\"http://xmlns.oracle.com/adf/svc/types/\"> " + 
				"   <soapenv:Header/> " + 
				"   <soapenv:Body> " + 
				"      <typ:findCustomerAccount> " + 
				"         <typ:findCriteria> " + 
				"            <typ1:filter> " + 
				"               <typ1:group> " + 
				"                  <typ1:item> " + 
				"                     <typ1:attribute>PartyId</typ1:attribute> " + 
				"                     <typ1:operator>=</typ1:operator> " + 
				"                     <typ1:value> " + partyId + "</typ1:value> " + 
				"                  </typ1:item> " + 
				"               </typ1:group> " + 
				"            </typ1:filter> " + 
				"            <typ1:findAttribute>AccountNumber</typ1:findAttribute> " + 
				"         </typ:findCriteria> " + 
				"      </typ:findCustomerAccount> " + 
				"   </soapenv:Body> " + 
				"</soapenv:Envelope> ";
		return SOAPRequest;
	}
	
	public static String getItemMasterSOAPXmlContent(String dateStart, String dateEnd, String start, String size, String dateType, List<String> entityList, String itemType){
		
		dateStart = dateStart.replace("\"", "");
		dateEnd = dateEnd.replace("\"", "");
		
		String itmTypeRequest = "";
		if(!"".contentEquals(itemType)) {
			
			itmTypeRequest = "<typ1:item> " + 
			"                      <typ1:conjunction>And</typ1:conjunction> " + 
			"                      <typ1:attribute>ItemClass</typ1:attribute> " + 
			"                      <typ1:operator>CONTAINS</typ1:operator> " + 
			"                      <typ1:value>" + itemType + "</typ1:value> " + 
			"                  </typ1:item> ";
		}
		
		
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/scm/productModel/items/itemServiceV2/types/\" xmlns:typ1=\"http://xmlns.oracle.com/adf/svc/types/\"> " + 
				"   <soapenv:Header/> " + 
				"   <soapenv:Body> " + 
				"      <typ:findItem> " + 
				"         <typ:findCriteria> " + 
				"            <typ1:fetchStart>" + start + "</typ1:fetchStart> " + 
				"            <typ1:fetchSize>" + size + "</typ1:fetchSize> " + 
				"            <typ1:filter> " + 
				"               <typ1:conjunction>Or</typ1:conjunction> " + 
				"               <typ1:group> " + 
				"                  <typ1:item> " + 
				"                      <typ1:conjunction>And</typ1:conjunction> " + 
				"                      <typ1:attribute>" + dateType + "</typ1:attribute> " + 
				"                      <typ1:operator>AFTER</typ1:operator> " + 
				"                      <typ1:value>" + dateStart + "</typ1:value> " + 
				"                  </typ1:item> " + 
				"                  <typ1:item> " + 
				"                      <typ1:conjunction>And</typ1:conjunction> " + 
				"                      <typ1:attribute>" + dateType + "</typ1:attribute> " + 
				"                      <typ1:operator>ONORBEFORE</typ1:operator> " + 
				"                      <typ1:value>" + dateEnd + "</typ1:value> " + 
				"                  </typ1:item>" +
				"                  <typ1:item> " + 
				"                      <typ1:conjunction>And</typ1:conjunction> " + 
				"                      <typ1:attribute>OrganizationCode</typ1:attribute> " + 
				"                      <typ1:operator>=</typ1:operator> " + 
				"                      <typ1:value>" + entityList.get(0) + "</typ1:value> " + 
				"                  </typ1:item>" +
				itmTypeRequest +
				"               </typ1:group> " + 
				"               <typ1:group> " + 
				"                  <typ1:item> " + 
				"                      <typ1:conjunction>And</typ1:conjunction> " + 
				"                      <typ1:attribute>" + dateType + "</typ1:attribute> " + 
				"                      <typ1:operator>AFTER</typ1:operator> " + 
				"                      <typ1:value>" + dateStart + "</typ1:value> " + 
				"                  </typ1:item> " + 
				"                  <typ1:item> " + 
				"                      <typ1:conjunction>And</typ1:conjunction> " + 
				"                      <typ1:attribute>" + dateType + "</typ1:attribute> " + 
				"                      <typ1:operator>ONORBEFORE</typ1:operator> " + 
				"                      <typ1:value>" + dateEnd + "</typ1:value> " + 
				"                  </typ1:item>" +
				"                  <typ1:item> " + 
				"                      <typ1:conjunction>And</typ1:conjunction> " + 
				"                      <typ1:attribute>OrganizationCode</typ1:attribute> " + 
				"                      <typ1:operator>=</typ1:operator> " + 
				"                      <typ1:value>" + entityList.get(1) + "</typ1:value> " + 
				"                  </typ1:item>" +
				itmTypeRequest +
				"               </typ1:group> " +  
                "               </typ1:filter> " + 
				"               <typ1:findAttribute>ItemNumber</typ1:findAttribute>" +
				"               <typ1:findAttribute>UserItemTypeValue</typ1:findAttribute>" +
				"               <typ1:findAttribute>AssembleToOrderFlag</typ1:findAttribute>" +
				"               <typ1:findAttribute>ItemDescription</typ1:findAttribute>" +
				"               <typ1:findAttribute>LongDescription</typ1:findAttribute>" +
				"               <typ1:findAttribute>ItemDescription</typ1:findAttribute>" +
				"               <typ1:findAttribute>ItemClass</typ1:findAttribute>" +
				"               <typ1:findAttribute>StockEnabledFlag</typ1:findAttribute>" +
				"               <typ1:findAttribute>PrimaryUOMValue</typ1:findAttribute>" +
				"               <typ1:findAttribute>MarketPrice</typ1:findAttribute>" +
				"               <typ1:findAttribute>ListPrice</typ1:findAttribute>" +
				"               <typ1:findAttribute>BackOrderableFlag</typ1:findAttribute>" +
				"               <typ1:findAttribute>SerialStatusEnabledFlag</typ1:findAttribute>" +
				"               <typ1:findAttribute>BackOrderableFlag</typ1:findAttribute>" +
				"               <typ1:findAttribute>MaximumLoadWeight</typ1:findAttribute> " + 
				"               <typ1:findAttribute>InternalVolume</typ1:findAttribute> " + 
				"               <typ1:findAttribute>CreationDateTime</typ1:findAttribute> " + 
				"               <typ1:findAttribute>LastUpdateDateTime</typ1:findAttribute> " + 
				"               <typ1:findAttribute>conversion</typ1:findAttribute> " + 
				"               <typ1:findAttribute>ItemCategory</typ1:findAttribute> " + 
				"               <typ1:findAttribute>ItemDFF</typ1:findAttribute> " + 
				"               <typ1:childFindCriteria> " + 
				"                    <typ1:findAttribute>ItemCatalog</typ1:findAttribute> " + 
				"                    <typ1:findAttribute>CategoryName</typ1:findAttribute> " + 
				"                    <typ1:findAttribute>CreationDateTime</typ1:findAttribute> " + 
				"                    <typ1:findAttribute>LastUpdateDateTime</typ1:findAttribute> " + 
				"                    <typ1:childAttrName>ItemCategory</typ1:childAttrName> " + 
				"               </typ1:childFindCriteria> " + 
				"               <typ1:childFindCriteria> " + 
				"               	<typ1:findAttribute>fechaRecibo</typ1:findAttribute> " + 
				"               	<typ1:childAttrName>ItemDFF</typ1:childAttrName> " + 
				"               </typ1:childFindCriteria>" +
				"         </typ:findCriteria> " + 
				"      </typ:findItem> " + 
				"   </soapenv:Body> " + 
				"</soapenv:Envelope> ";
		return SOAPRequest;
	}
	
	public static String getItemCostSOAPXmlContent(String itemNumber){

		itemNumber = itemNumber.replace("\"", "");
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/scm/costing/itemCosts/itemCostServiceV2/types/\" xmlns:item=\"http://xmlns.oracle.com/apps/scm/costing/itemCosts/itemCostServiceV2/\"> " + 
				"   <soapenv:Header/> " + 
				"   <soapenv:Body> " + 
				"      <typ:retrieveItemCost> " + 
				"         <typ:costparams> " + 
				"               <item:ItemNumber>"+ itemNumber + "</item:ItemNumber> " + 
				"               <item:InventoryOrganizationCode>CDI001</item:InventoryOrganizationCode> " + 
				"               <item:CurrencyCode>MXN</item:CurrencyCode> " + 
				"            </typ:costparams> " + 
				"      </typ:retrieveItemCost> " + 
				"   </soapenv:Body> " + 
				"</soapenv:Envelope> ";
		return SOAPRequest;
	}
	
	public static String getLocationSOAPXmlContent(CustomerDTO c) {

		String SOAPRequest = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"> " + 
				"<soap:Body xmlns:ns1=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/locationService/applicationModule/types/\"> " + 
				"<ns1:createLocation> " + 
				"<ns1:location xmlns:ns2=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/locationService/\"> " + 
				"<ns2:Country>" + c.getCountry() + "</ns2:Country> " + 
				"<ns2:Address1>" + c.getAddress1() + "</ns2:Address1> " + 
				"<ns2:City>" + c.getCity() + "</ns2:City> " + 
				"<ns2:PostalCode>" + c.getPostalCode() + "</ns2:PostalCode> " + 
				"<ns2:State>"+ c.getState() +"</ns2:State> " + 
				"<ns2:CreatedByModule>AMS</ns2:CreatedByModule> " + 
				"</ns1:location> " + 
				"</ns1:createLocation> " + 
				"</soap:Body> " + 
				"</soap:Envelope> ";
		return SOAPRequest;
	}

	public static String getCustomerOrganizationSOAPXmlContent(CustomerDTO c, String locationId) {
		
		String SOAPRequest = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"> " + 
				"   <soap:Body xmlns:ns1=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/organizationService/applicationModule/types/\"> " + 
				"      <ns1:createOrganization> " + 
				"         <ns1:organizationParty xmlns:ns2=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/organizationService/\"> " + 
				"            <ns2:CreatedByModule>AMS</ns2:CreatedByModule> " + 
				"            <ns2:PartyUsageAssignment xmlns:ns3=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/partyService/\"> " + 
				"               <ns3:PartyUsageCode>CUSTOMER</ns3:PartyUsageCode> " + 
				"               <ns3:CreatedByModule>AMS</ns3:CreatedByModule> " + 
				"            </ns2:PartyUsageAssignment> " + 
				"            <ns2:PartyUsageAssignment xmlns:ns3=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/partyService/\"> " + 
				"               <ns3:PartyUsageCode>EXTERNAL_LEGAL_ENTITY</ns3:PartyUsageCode> " + 
				"               <ns3:CreatedByModule>AMS</ns3:CreatedByModule> " + 
				"            </ns2:PartyUsageAssignment> " + 
				"            <ns2:OrganizationProfile> " + 
				"               <ns2:OrganizationName>" + c.getName() + "</ns2:OrganizationName> " + 
				"               <ns2:JgzzFiscalCode>" + c.getRfc() +"</ns2:JgzzFiscalCode> " + 
				"               <ns2:CreatedByModule>AMS</ns2:CreatedByModule> " + 
				"            </ns2:OrganizationProfile> " + 
				"            <ns2:PartySite xmlns:ns28=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/partyService/\"> " + 
				"               <ns28:LocationId>" + locationId +"</ns28:LocationId> " + 
				"               <ns28:Mailstop>Empty</ns28:Mailstop> " + 
				"               <ns28:IdentifyingAddressFlag>true</ns28:IdentifyingAddressFlag> " + 
				"               <ns28:PartySiteName>" + c.getName() +"</ns28:PartySiteName> " + 
				"               <ns28:Addressee>SiteName Address</ns28:Addressee> " + 
				"               <ns28:CreatedByModule>AMS</ns28:CreatedByModule> " + 
				"               <ns28:Comments>Created by interface</ns28:Comments> " + 
				"               <ns28:CurrencyCode>USD</ns28:CurrencyCode> " + 
				"               <ns28:CorpCurrencyCode>USD</ns28:CorpCurrencyCode> " + 
				"               <ns28:CurcyConvRateType>Corporate</ns28:CurcyConvRateType> " + 
				"               <ns28:PartySiteUse> " + 
				"                  <ns28:Comments>Created by interface</ns28:Comments> " + 
				"                  <ns28:SiteUseType>BILL_TO</ns28:SiteUseType> " + 
				"                  <ns28:CreatedByModule>AMS</ns28:CreatedByModule> " + 
				"               </ns28:PartySiteUse> " + 
				"            </ns2:PartySite> " + 
				"            <ns2:Relationship xmlns:ns14=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/relationshipService/\"> " + 
				"                <ns14:SubjectType>ORGANIZATION</ns14:SubjectType> " + 
				"                <ns14:SubjectTableName>HZ_PARTIES</ns14:SubjectTableName> " + 
				"                <ns14:ObjectType>PERSON</ns14:ObjectType> " + 
				"                <ns14:ObjectId>" + AppConstants.DEFAULT_CONTACT_ID + "</ns14:ObjectId> " + 
				"                <ns14:ObjectTableName>HZ_PARTIES</ns14:ObjectTableName> " + 
				"                <ns14:RelationshipCode>EMPLOYER_OF</ns14:RelationshipCode> " + 
				"                <ns14:RelationshipType>EMPLOYMENT</ns14:RelationshipType> " + 
				"                <ns14:Comments>Created by interface</ns14:Comments> " + 
				"                <ns14:CurrencyCode>USD</ns14:CurrencyCode> " + 
				"                <ns14:CurcyConvRateType>Corporate</ns14:CurcyConvRateType> " + 
				"                <ns14:CorpCurrencyCode>USD</ns14:CorpCurrencyCode> " + 
				"                <ns14:CreatedByModule>AMS</ns14:CreatedByModule>  " + 
				"            </ns2:Relationship> " + 
				"         </ns1:organizationParty> " + 
				"      </ns1:createOrganization> " + 
				"   </soap:Body> " + 
				"</soap:Envelope> ";
		return SOAPRequest;
	}
	
	public static String getCustomerAccountSOAPXmlContent(CustomerDTO c, String partyId, String partySiteId, String partyNumber, String setId) {

		String SOAPRequest = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"> " + 
				"   <soap:Body xmlns:ns1=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/customerAccountService/applicationModule/types/\"> " + 
				"      <ns1:createCustomerAccount> " + 
				"         <ns1:customerAccount xmlns:ns2=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/customerAccountService/\"> " + 
				"            <ns2:PartyId>" + partyId +"</ns2:PartyId> " + 
				"            <ns2:AccountNumber>" + partyNumber + "AC" + "</ns2:AccountNumber> " + 
				"            <ns2:Status>A</ns2:Status> " + 
				"            <ns2:CustomerType>R</ns2:CustomerType> " +
				//"            <ns2:CustomerClassCode>PRIVATE</ns2:CustomerClassCode> " +
				"            <ns2:CustomerClassCode>Private Class</ns2:CustomerClassCode> " +
				"            <ns2:AccountName>" + c.getName() +"</ns2:AccountName> " + 
				"            <ns2:CreatedByModule>AMS</ns2:CreatedByModule> " + 
				"            <ns2:CustomerAccountSite> " + 
				"               <ns2:PartySiteId>" + partySiteId +"</ns2:PartySiteId> " + 
				"               <ns2:CreatedByModule>AMS</ns2:CreatedByModule> " + 
				"               <ns2:SetId>" + setId +"</ns2:SetId> " + 
				"               <ns2:CustomerAccountSiteUse> " + 
				"                  <ns2:SiteUseCode>BILL_TO</ns2:SiteUseCode> " + 
				"                  <ns2:CreatedByModule>AMS</ns2:CreatedByModule> " + 
				"               </ns2:CustomerAccountSiteUse> " + 
				"            </ns2:CustomerAccountSite> " + 
				"         </ns1:customerAccount> " + 
				"      </ns1:createCustomerAccount> " + 
				"   </soap:Body> " + 
				"</soap:Envelope> ";
		return SOAPRequest;
	}
	
	public static String getCustomerProfileServiceSOAPXmlContent(String partyId, String customerAccountId) {

		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/financials/receivables/customers/customerProfileService/types/\" xmlns:cus=\"http://xmlns.oracle.com/apps/financials/receivables/customers/customerProfileService/\" xmlns:cus1=\"http://xmlns.oracle.com/apps/financials/receivables/customerSetup/customerProfiles/model/flex/CustomerProfileDff/\" xmlns:cus2=\"http://xmlns.oracle.com/apps/financials/receivables/customerSetup/customerProfiles/model/flex/CustomerProfileGdf/\"> " + 
				"   <soapenv:Header/> " + 
				"   <soapenv:Body> " + 
				"      <typ:createCustomerProfile> " + 
				"         <typ:customerProfile> " + 
				"            <cus:PartyId>" + partyId +"</cus:PartyId> " + 
				"            <cus:CustomerAccountId>" + customerAccountId +"</cus:CustomerAccountId> " + 
				"        </typ:customerProfile> " + 
				"      </typ:createCustomerProfile> " + 
				"   </soapenv:Body> " + 
				"</soapenv:Envelope> ";
		return SOAPRequest;
	}
	
	public static String getSessionIdSOAPXmlContent() {
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:v7=\"urn://oracle.bi.webservices/v7\"> " + 
				"   <soapenv:Header/> " + 
				"   <soapenv:Body> " + 
				"      <v7:logon> " + 
				"         <v7:name>hrocha@smartech.com.mx</v7:name> " + 
				"         <v7:password>mexico2018</v7:password> " + 
				"      </v7:logon> " + 
				"   </soapenv:Body> " + 
				"</soapenv:Envelope>";
		return SOAPRequest;
	}
	
	public static String logOffSessionIdSOAPXmlContent(String sessionId) {
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:v7=\"urn://oracle.bi.webservices/v7\"> " + 
				"   <soapenv:Header/> " + 
				"   <soapenv:Body> " + 
				"      <v7:logoff> " + 
				"         <v7:sessionID>" + sessionId +"</v7:sessionID> " + 
				"      </v7:logoff> " + 
				"   </soapenv:Body> " + 
				"</soapenv:Envelope>";
		return SOAPRequest;
	}
	
	
	@SuppressWarnings("rawtypes")
	public static String getItemCostSOAPXmlContent(Map<String, InventoryTransactionDTO> invMap) {
		
		if(invMap.size() > 0) {
			String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/scm/costing/itemCosts/itemCostServiceV2/types/\" xmlns:item=\"http://xmlns.oracle.com/apps/scm/costing/itemCosts/itemCostServiceV2/\"> " + 
					"   <soapenv:Header/> " + 
					"   <soapenv:Body> " + 
					"      <typ:retrieveItemCost> ";
					
			Iterator entries = invMap.entrySet().iterator();
			while (entries.hasNext()) {
			    Map.Entry entry = (Map.Entry) entries.next();
			    InventoryTransactionDTO o = (InventoryTransactionDTO)entry.getValue();
			    SOAPRequest = SOAPRequest + " <typ:costparams> " + 
						"  <item:ItemNumber>" + o.getItemName() +"</item:ItemNumber> " + 
						"  <item:InventoryOrganizationCode>" + o.getInventoryOrganizationCode() + "</item:InventoryOrganizationCode> " + 
						"  <item:CurrencyCode>MXN</item:CurrencyCode> " +
						" </typ:costparams> "; 
			}

					SOAPRequest = SOAPRequest + "</typ:retrieveItemCost> " + 
					"   </soapenv:Body> " + 
					"</soapenv:Envelope>";
			return SOAPRequest;
		}else {
			return null;
		}
		
	}
	
	@SuppressWarnings("unused")
	public static String getInventoryTransactionTransferSOAPXmlContent(String sessionId, String sucursal, String dateFrom) {
		
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:v7=\"urn://oracle.bi.webservices/v7\"> " + 
				"   <soapenv:Header/> " + 
				"   <soapenv:Body> " + 
				"      <v7:executeSQLQuery> " + 
				"         <v7:sql><![CDATA[SELECT " + 
				"   \"Inventory - Inventory Transactions Real Time\".\"- Main\".\"Item Name\" s_1, " + 
				"   \"Inventory - Inventory Transactions Real Time\".\"- Main\".\"Item Description\" s_2, " + 
				"   \"Inventory - Inventory Transactions Real Time\".\"- Main\".\"Item Primary Unit of Measure\" s_3, " + 
				"   \"Inventory - Inventory Transactions Real Time\".\"Inventory Organization\".\"Inventory Organization Code\" s_4, " + 
				"   \"Inventory - Inventory Transactions Real Time\".\"Inventory Organization\".\"Inventory Organization Name\" s_5, " + 
				"   \"Inventory - Inventory Transactions Real Time\".\"Inventory Transactions Details\".\"Transaction Date\" s_8, " + 
				"   \"Inventory - Inventory Transactions Real Time\".\"Locator\".\"Locator Inventory Location\" s_11, " + 
				"   \"Inventory - Inventory Transactions Real Time\".\"Lot\".\"Creation Date\" s_12, " + 
				"   \"Inventory - Inventory Transactions Real Time\".\"Lot\".\"Expiration Date\" s_13, " + 
				"   \"Inventory - Inventory Transactions Real Time\".\"Lot\".\"Lot Number\" s_14, " + 
				"   \"Inventory - Inventory Transactions Real Time\".\"Subinventory\".\"Subinventory Name\" s_15, " + 
				"   \"Inventory - Inventory Transactions Real Time\".\"Transaction Type\".\"Transaction Type\" s_16, " + 
				"   \"Inventory - Inventory Transactions Real Time\".\"Inventory Transactions\".\"Primary Quantity\" s_19,"
				+ "   \"Inventory - Inventory Balance Real Time\".\"Category\".\"Category Name\" s_20 " + 
				"FROM \"Inventory - Inventory Transactions Real Time\" " + 
				"WHERE " + 
				"((\"Inventory Organization\".\"Inventory Organization Code\" = '" + sucursal + "')  " + 
				"  AND (\"Inventory Transactions Details\".\"Transaction Date\" >= timestamp '" + dateFrom +"')   " + 
				//"  AND (\"Inventory Transactions Details\".\"Transaction Date\" <= timestamp '" + toDate +"') " + 
				"  AND (\"Transaction Type\".\"Transaction Type\" IN ('in', 'Intransit Receipt', 'Intransit Shipment', 'Direct Organization Transfer', 'Subinventory Transfer', "
				+ " 'Purchase Order Receipt', 'Miscellaneous issue', 'Miscellaneous Receipt', 'Transfer to Owned', 'Cycle Count Adjustment', "
				+ " 'Purchase Order Receipt Adjustment', 'Return to Supplier','Physical Inventory Adjustment'))) " +
				//"  AND ((\"Catalog\".\"Catalog Name\" = 'MONEDA'))) " + 
				" ORDER BY 6 ASC " + 
				" FETCH FIRST 75001 ROWS ONLY]]></v7:sql> " + 
				"         <v7:outputFormat>XML</v7:outputFormat> " + 
				"         <v7:executionOptions> " + 
				"            <v7:async></v7:async> " + 
				"            <v7:maxRowsPerPage></v7:maxRowsPerPage> " + 
				"            <v7:refresh></v7:refresh> " + 
				"            <v7:presentationInfo></v7:presentationInfo> " + 
				"            <v7:type></v7:type> " + 
				"         </v7:executionOptions> " + 
				"         <v7:sessionID>" + sessionId + "</v7:sessionID> " + 
				"      </v7:executeSQLQuery> " + 
				"   </soapenv:Body> " + 
				"</soapenv:Envelope>";
		int i = 0;
		return SOAPRequest;
	}
	
	
	public static String findOrganizationByFCSOAPXmlContent(String request) {
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/organizationService/applicationModule/types/\" xmlns:typ1=\"http://xmlns.oracle.com/adf/svc/types/\">" + 
				"   <soapenv:Header/>" + 
				"   <soapenv:Body>" + 
				"      <typ:findOrganization>" + 
				"         <typ:findCriteria>" + 
				"            <typ1:fetchStart>0</typ1:fetchStart>" + 
				"            <typ1:fetchSize>-1</typ1:fetchSize>" + 
				"            <typ1:filter>" + 
				"               <typ1:group>" + 
				"                  <typ1:item>" + 
				"                     <typ1:attribute>JgzzFiscalCode</typ1:attribute>" + 
				"                     <typ1:operator>=</typ1:operator>" + 
				"                     <typ1:value>" + request + "</typ1:value>" + 
				"                  </typ1:item>" + 
				"               </typ1:group>" + 
				"            </typ1:filter>" + 
				"            <typ1:findAttribute>PartyName</typ1:findAttribute>" + 
				"         </typ:findCriteria>" + 
				"      </typ:findOrganization>" + 
				"   </soapenv:Body>" + 
				"</soapenv:Envelope>";
		return SOAPRequest;
	}
	
	
	public static String findOrganizationByFiscalCodeSOAPXmlContent(String request) {
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/organizationService/applicationModule/types/\" xmlns:typ1=\"http://xmlns.oracle.com/adf/svc/types/\"> " + 
				"   <soapenv:Header/> " + 
				"   <soapenv:Body> " + 
				"      <typ:findOrganization> " + 
				"         <typ:findCriteria> " + 
				"            <typ1:fetchStart>0</typ1:fetchStart> " + 
				"            <typ1:fetchSize>-1</typ1:fetchSize> " + 
				"            <typ1:filter> " + 
				"               <typ1:group> " + 
				"                  <typ1:item> " + 
				"                     <typ1:attribute>JgzzFiscalCode</typ1:attribute> " + 
				"                     <typ1:operator>=</typ1:operator> " + 
				"                     <typ1:value>" + request +"</typ1:value> " + 
				"                  </typ1:item> " + 
				"               </typ1:group> " + 
				"            </typ1:filter> " + 
				"			<typ1:findAttribute>PartyId</typ1:findAttribute> " + 
				"			<typ1:findAttribute>PartySite</typ1:findAttribute> " + 
				"			<typ1:childFindCriteria> " + 
				"			      <typ1:childAttrName>PartySite</typ1:childAttrName> " + 
				"			      <typ1:findAttribute>LocationId</typ1:findAttribute> " + 
				"			      <typ1:findAttribute>PartyId</typ1:findAttribute> " + 
				"			</typ1:childFindCriteria> " + 
				"         </typ:findCriteria> " + 
				"      </typ:findOrganization> " + 
				"   </soapenv:Body> " + 
				"</soapenv:Envelope>";
		return SOAPRequest;
	}
	
	public static String findPurchaseAverageCostSOAPXmlContent(String request, String sessionId) {
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:v7=\"urn://oracle.bi.webservices/v7\"> " + 
				"   <soapenv:Header/> " + 
				"   <soapenv:Body> " + 
				"      <v7:executeSQLQuery> " + 
				"         <v7:sql><![CDATA[SELECT saw_0, saw_1, saw_2, sum(saw_3), sum(saw_4) " + 
				" FROM ((SELECT \"Inventory Organization\".\"Inventory Organization Code\" saw_0,  " + 
				" \"- Main\".\"Item Name\" saw_1,  " + 
				" \"Lot\".\"Lot Number\" saw_2,  " + 
				" \"Current On-Hand Inventory\".\"Available Quantity\" saw_3,  " + 
				" \"Item Subinventory\".\"Processing Number of Days\" saw_4 " + 
				" FROM \"Inventory - Inventory Balance Real Time\" " + 
				" WHERE (\"Catalog\".\"Catalog Code\" = 'CATALOGO_GENERAL')  " + 
				" AND (\"- Main\".\"Item Name\" = '" + request + "') " + 
				" AND (\"Inventory Organization\".\"Inventory Organization Code\" = 'CDI001') " +
				" AND ( \"Current On-Hand Inventory\".\"Available Quantity\" > 0))  " + 
				" UNION (SELECT \"Inventory Organization\".\"Inventory Organization Code\" saw_0,  " + 
				"               \"-  Main\".\"Item Name\" saw_1,  " + 
				"               \"Cost Transaction Details\".\"Lot Number\" saw_2,  " + 
				"               \"Work Order\".\"Quantity Scrapped\" saw_3,  " + 
				"               \"- Purchase Order\".\"Price\" saw_4 " + 
				"                FROM \"Costing - Cost Accounting Real Time\" " + 
				"                WHERE (\"-  Main\".\"Item Name\" = '" + request + "')" +
			    "       AND (\"Inventory Organization\".\"Inventory Organization Code\" = 'CDI001'))) t1 " + 
				" GROUP BY saw_0, saw_1, saw_2 " + 
				" HAVING (sum(saw_3) > 0)  " + 
				" ORDER BY saw_0, saw_1, saw_2]]></v7:sql> " + 
				"         <v7:outputFormat>XML</v7:outputFormat> " + 
				"         <v7:executionOptions> " + 
				"            <v7:async/> " + 
				"            <v7:maxRowsPerPage/> " + 
				"            <v7:refresh/> " + 
				"            <v7:presentationInfo/> " + 
				"            <v7:type/> " + 
				"         </v7:executionOptions> " + 
				"         <v7:sessionID>" + sessionId + "</v7:sessionID> " + 
				"      </v7:executeSQLQuery> " + 
				"   </soapenv:Body> " + 
				"</soapenv:Envelope>";
		return SOAPRequest;
	}
	
	public static String findAccountByAccountXmlContent(String request) {
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/customerAccountService/applicationModule/types/\" xmlns:typ1=\"http://xmlns.oracle.com/adf/svc/types/\"> " + 
				"   <soapenv:Header/> " + 
				"   <soapenv:Body> " + 
				"      <typ:findCustomerAccount> " + 
				"         <typ:findCriteria> " + 
				"            <typ1:fetchStart>0</typ1:fetchStart> " + 
				"            <typ1:fetchSize>-1</typ1:fetchSize> " + 
				"            <typ1:filter> " + 
				"               <typ1:group> " + 
				"                  <typ1:item> " + 
				"                     <typ1:attribute>AccountNumber</typ1:attribute> " + 
				"                     <typ1:operator>=</typ1:operator> " + 
				"                     <typ1:value>" + request +"</typ1:value> " + 
				"                  </typ1:item> " + 
				"               </typ1:group> " + 
				"            </typ1:filter> " + 
				"            <typ1:findAttribute>PartyId</typ1:findAttribute> " + 
				"         </typ:findCriteria> " + 
				"      </typ:findCustomerAccount> " + 
				"   </soapenv:Body> " + 
				"</soapenv:Envelope>";
		return SOAPRequest;
	}
	
	public static String findLocationByPartyIdXmlContent(String request) {
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/organizationService/applicationModule/types/\" xmlns:typ1=\"http://xmlns.oracle.com/adf/svc/types/\">" + 
				"   <soapenv:Header/>" + 
				"   <soapenv:Body>" + 
				"      <typ:findOrganization>" + 
				"		<typ:findCriteria>" + 
				"	       <typ1:fetchStart>0</typ1:fetchStart>" + 
				"	       <typ1:fetchSize>-1</typ1:fetchSize>" + 
				"	       <typ1:filter>" + 
				"	          <typ1:group>" + 
				"	             <typ1:item>" + 
				"	                <typ1:attribute>PartyId</typ1:attribute>" + 
				"	                <typ1:operator>=</typ1:operator>" + 
				"	                <typ1:value>" + request +"</typ1:value>" + 
				"	             </typ1:item>" + 
				"	          </typ1:group>" + 
				"	       </typ1:filter>" + 
				"	       <typ1:findAttribute>IdenAddrLocationId</typ1:findAttribute>" + 
				"	    </typ:findCriteria>" + 
				"      </typ:findOrganization>" + 
				"   </soapenv:Body>" + 
				"</soapenv:Envelope>";
		return SOAPRequest;
	}
	
	public static String updateLocationUpdateSOAPXmlContent(String request, CustomerDTO c) {
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/locationService/applicationModule/types/\" xmlns:loc=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/locationService/\" xmlns:par=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/partyService/\" xmlns:sour=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/sourceSystemRef/\" xmlns:loc1=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/location/\"> " + 
				"   <soapenv:Header/> " + 
				"   <soapenv:Body> " + 
				"      <typ:updateLocation> " + 
				"         <typ:location> " + 
				"            <loc:LocationId>" + request + "</loc:LocationId> " + 
				"            <loc:Address1>" + c.getAddress1() + "</loc:Address1> " + 
				"            <loc:City>" + c.getCity() + "</loc:City> " + 
				"            <loc:PostalCode>" + c.getPostalCode() + "</loc:PostalCode> " + 
				"            <loc:State>" + c.getState() + "</loc:State> " +  
				"            <loc:Country>" + c.getCountry() + "</loc:Country> " +  
				"         </typ:location> " + 
				"      </typ:updateLocation> " + 
				"   </soapenv:Body> " + 
				"</soapenv:Envelope>";
		return SOAPRequest;
	}
	
	public static String updateCustomerProfileSOAPXmlContent(String request, CustomerDTO c) {
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/organizationService/applicationModule/types/\" xmlns:org=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/organizationService/\" xmlns:par=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/partyService/\" xmlns:sour=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/sourceSystemRef/\" xmlns:con=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/contactPointService/\" xmlns:con1=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/contactPoint/\" xmlns:org1=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/organization/\" xmlns:par1=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/partySite/\" xmlns:rel=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/relationshipService/\" xmlns:org2=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/orgContact/\" xmlns:rel1=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/relationship/\">" + 
				"   <soapenv:Header/>" + 
				"   <soapenv:Body>" + 
				"      <typ:updateOrganization>" + 
				"         <typ:organizationParty>" + 
				"            <org:PartyId>" + request + "</org:PartyId>" + 
				"            <org:PartyType>ORGANIZATION</org:PartyType>" + 
				"            <org:OrganizationProfile>" + 
				"               <org:OrganizationName>" + c.getName() + "</org:OrganizationName>" + 
                "               <org:JgzzFiscalCode>" + c.getRfc() + "</org:JgzzFiscalCode>" + 
				"            </org:OrganizationProfile>" + 
				"         </typ:organizationParty>" + 
				"      </typ:updateOrganization>" + 
				"   </soapenv:Body>" + 
				"</soapenv:Envelope>";
		return SOAPRequest;
	}
	
	
	public static String findPartySiteXmlContent(String request) {
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/organizationService/applicationModule/types/\" xmlns:typ1=\"http://xmlns.oracle.com/adf/svc/types/\"> " + 
				"   <soapenv:Header/> " + 
				"   <soapenv:Body> " + 
				"      <typ:findOrganization> " + 
				"		<typ:findCriteria> " + 
				"	       <typ1:fetchStart>0</typ1:fetchStart> " + 
				"	       <typ1:fetchSize>-1</typ1:fetchSize> " + 
				"	       <typ1:filter> " + 
				"	          <typ1:group> " + 
				"	             <typ1:item> " + 
				"	                <typ1:attribute>PartyId</typ1:attribute> " + 
				"	                <typ1:operator>=</typ1:operator> " + 
				"	                <typ1:value>" + request + "</typ1:value> " + 
				"	             </typ1:item> " + 
				"	          </typ1:group> " + 
				"	       </typ1:filter> " + 
				"	       <typ1:findAttribute>PartyId</typ1:findAttribute>  " + 
				"		  <typ1:findAttribute>PartySite</typ1:findAttribute> " + 
				"		  <typ1:childFindCriteria> " + 
				"		 	 <typ1:childAttrName>PartySite</typ1:childAttrName> " + 
				"		  	<typ1:findAttribute>PartySiteNumber</typ1:findAttribute>  " + 
				"		  </typ1:childFindCriteria>  " + 
				"	    </typ:findCriteria> " + 
				"      </typ:findOrganization> " + 
				"   </soapenv:Body> " + 
				"</soapenv:Envelope>";
		return SOAPRequest;
	}
	
	public static String findAccountNumberXmlContent(String request) {
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/customerAccountService/applicationModule/types/\" xmlns:typ1=\"http://xmlns.oracle.com/adf/svc/types/\"> " + 
				"   <soapenv:Header/> " + 
				"   <soapenv:Body> " + 
				"      <typ:findCustomerAccount> " + 
				"         <typ:findCriteria> " + 
				"            <typ1:fetchStart>0</typ1:fetchStart> " + 
				"            <typ1:fetchSize>-1</typ1:fetchSize> " + 
				"            <typ1:filter> " + 
				"               <typ1:group> " + 
				"                  <typ1:item> " + 
				"                     <typ1:attribute>PartyId</typ1:attribute> " + 
				"                     <typ1:operator>=</typ1:operator> " + 
				"                     <typ1:value>" + request +"</typ1:value> " + 
				"                  </typ1:item> " + 
				"               </typ1:group> " + 
				"            </typ1:filter> " + 
				"            <typ1:findAttribute>AccountNumber</typ1:findAttribute>  " + 
				"         </typ:findCriteria> " + 
				"      </typ:findCustomerAccount> " + 
				"   </soapenv:Body> " + 
				"</soapenv:Envelope>";
		return SOAPRequest;
	}

	
	public static String findPriceListJsonContent(String entity) {	
		String jsonUrl = "https://ejbh-test.fa.us2.oraclecloud.com/fscmRestApi/resources/11.13.18.05/priceLists?";
		jsonUrl = jsonUrl + "onlyData=true&fields=PriceListName,PriceListId,Status,StatusCode,BusinessUnit,CalculationMethodCode,StartDate,EndDate,CreationDate,LastUpdateDate&totalResults=false&q=StatusCode='APPROVED'";
		jsonUrl = jsonUrl + ";BusinessUnit='" + entity + "'";
		return jsonUrl;
	}
	
	public static String findAllItemPriceListJsonContent(String listId, String start, String limit, String dateStart, String dateEnd, String dateType) {	
		String jsonUrl = "https://ejbh-test.fa.us2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/priceLists/";
		jsonUrl = jsonUrl + listId;
		jsonUrl = jsonUrl + "/child/items?onlyData=true&offset=" + "0";
		jsonUrl = jsonUrl + "&limit=" + 1000;
		jsonUrl = jsonUrl + "&expand=charges&totalResults=false";
		return jsonUrl;
	}
	
	public static String findNewItemPriceListJsonContent(String listId, String start, String limit, String dateStart, String dateEnd, String dateType) {	
		String jsonUrl = "https://ejbh-test.fa.us2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/priceLists/";
		jsonUrl = jsonUrl + listId;
		jsonUrl = jsonUrl + "/child/items?onlyData=true&offset=" + "0";
		jsonUrl = jsonUrl + "&limit=" + 1000;
		jsonUrl = jsonUrl + "&expand=charges&totalResults=false";
		jsonUrl = jsonUrl + "&q=" + "CreationDate" + ">=" +  dateStart;
		return jsonUrl;
	}
	
public static String getARBulkFileImportSOAPXmlContent(String content, String fileName) {
		
		// Modificar los datos de los parámetros de acuerdo al ambiente:
		
		
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/types/\" xmlns:erp=\"http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/\"> " + 
				"   <soapenv:Header/> " + 
				"   <soapenv:Body> " + 
				"      <typ:importBulkData> " + 
				"         <typ:document> " + 
				"            <erp:Content>" + content +"</erp:Content> " + 
				"            <erp:FileName>" + fileName +"</erp:FileName> " + 
				"            <erp:ContentType>zip</erp:ContentType> " + 
				"         </typ:document> " + 
				"         <typ:jobDetails> " + 
				"            <erp:JobName>/oracle/apps/ess/financials/receivables/transactions/autoInvoices,AutoInvoiceImportEss</erp:JobName> " + 
				"            <erp:ParameterList>300000001600035,POS_Kauffman,2019-10-07,#NULL,#NULL,#NULL,#NULL,#NULL,#NULL,#NULL,#NULL,#NULL,#NULL,#NULL,#NULL,#NULL,#NULL,#NULL,#NULL,#NULL,#NULL,#NULL,Y,#NULL</erp:ParameterList> " + 
				"         </typ:jobDetails> " + 
				"         <typ:notificationCode>00</typ:notificationCode> " + 
				"         <typ:callbackURL>#NULL</typ:callbackURL> " + 
				"      </typ:importBulkData> " + 
				"   </soapenv:Body> " + 
				"</soapenv:Envelope>";
		return SOAPRequest;
	}
	
	public static String getInvBulkFileImportSOAPXmlContent(String content, String fileName) {
		
		// Modificar los datos de los parámetros de acuerdo al ambiente:
		
		
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/types/\" xmlns:erp=\"http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/\"> " + 
				"   <soapenv:Header/> " + 
				"   <soapenv:Body> " + 
				"      <typ:importBulkData> " + 
				"         <typ:document> " + 
				"            <erp:Content>" + content +"</erp:Content> " + 
				"            <erp:FileName>" + fileName +"</erp:FileName> " + 
				"            <erp:ContentType>zip</erp:ContentType> " + 
				"         </typ:document> " + 
				"         <typ:jobDetails> " + 
				"            <erp:JobName>/oracle/apps/ess/scm/inventory/materialTransactions/txnManager,SingleTMEssJob</erp:JobName> " + 
				"            <erp:ParameterList>#NULL</erp:ParameterList> " + 
				"         </typ:jobDetails> " + 
				"         <typ:notificationCode>00</typ:notificationCode> " + 
				"         <typ:callbackURL>#NULL</typ:callbackURL> " + 
				"      </typ:importBulkData> " + 
				"   </soapenv:Body> " + 
				"</soapenv:Envelope>";
		return SOAPRequest;
	}
	
	public static String getGLBulkFileImportSOAPXmlContent(String content, String fileName) {
		
		// Modificar los datos de los parámetros de acuerdo al ambiente:
		
		
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/types/\" xmlns:erp=\"http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/\"> " + 
				"   <soapenv:Header/> " + 
				"   <soapenv:Body> " + 
				"      <typ:importBulkData> " + 
				"         <typ:document> " + 
				"            <erp:Content>" + content +"</erp:Content> " + 
				"            <erp:FileName>" + fileName +"</erp:FileName> " + 
				"            <erp:ContentType>zip</erp:ContentType> " + 
				"         </typ:document> " + 
				"         <typ:jobDetails> " + 
				"            <erp:JobName>/oracle/apps/ess/financials/generalLedger/programs/common,JournalImportLauncher</erp:JobName> " + 
				"            <erp:ParameterList>300000001507130,Balance Transfer,300000001507121,ALL,N,N,N</erp:ParameterList> " + 
				"         </typ:jobDetails> " + 
				"         <typ:notificationCode>00</typ:notificationCode> " + 
				"         <typ:callbackURL>#NULL</typ:callbackURL> " + 
				"      </typ:importBulkData> " + 
				"   </soapenv:Body> " + 
				"</soapenv:Envelope>";
		return SOAPRequest;
	}

	
	public static String getARLockboxSOAPXMLContentStep1(String content, String fileName, String docName) {
		
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/types/\" xmlns:erp=\"http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/\">" + 
				"   <soapenv:Header/>" + 
				"   <soapenv:Body>" + 
				"      <typ:uploadFileToUcm>" + 
				"         <typ:document>" + 
				"            <erp:Content>" + content + "</erp:Content>" + 
				"            <erp:FileName>" + fileName +"</erp:FileName>" + 
				"            <erp:ContentType>zip</erp:ContentType>" +  
				"            <erp:DocumentSecurityGroup>FAFusionImportExport</erp:DocumentSecurityGroup>" + 
				"            <erp:DocumentAccount>fin$/receivables$/import$</erp:DocumentAccount>" + 
				"            <erp:DocumentName>"+ docName +"</erp:DocumentName>" + 
				" 		</typ:document>" + 
				"      </typ:uploadFileToUcm>" + 
				"   </soapenv:Body>" + 
				"</soapenv:Envelope>";
		
		return SOAPRequest;
	}
	
	public static String getARLockboxSOAPXMLContentStep2(String step1Id) {
		
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/types/\">" + 
				"   <soapenv:Header/>" + 
				"   <soapenv:Body>" + 
				"      <typ:submitESSJobRequest>" + 
				"         <typ:jobPackageName>oracle/apps/ess/financials/commonModules/shared/common/interfaceLoader</typ:jobPackageName>" + 
				"         <typ:jobDefinitionName>InterfaceLoaderController</typ:jobDefinitionName>" + 
				"         <typ:paramList>3</typ:paramList>" + 
				"         <typ:paramList>" + step1Id + "</typ:paramList>" + 
				"	    <typ:paramList>N</typ:paramList>" + 
				"	    <typ:paramList>N</typ:paramList>" + 
				"	    <typ:paramList>#NULL</typ:paramList>" + 
				"      </typ:submitESSJobRequest>" + 
				"   </soapenv:Body>" + 
				"</soapenv:Envelope>";
		
		return SOAPRequest;
	}
	
	public static String getARLockboxSOAPXMLContentStep3(String StepId2, String title, String date) {
		
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/types/\">" + 
				"   <soapenv:Header/>" + 
				"   <soapenv:Body>" + 
				"      <typ:submitESSJobRequest>" + 
					"      <typ:jobPackageName>/oracle/apps/ess/financials/receivables/receipts/lockboxes</typ:jobPackageName>" + 
					"      <typ:jobDefinitionName>ProcessLockboxesMasterEss</typ:jobDefinitionName>" + 
					"      <typ:paramList>Y</typ:paramList>" + 
					"      <typ:paramList>" + StepId2 + "</typ:paramList>" + 
					"	   <typ:paramList>" + title + "</typ:paramList>" + 
					"	   <typ:paramList>N</typ:paramList>" + 
					"	   <typ:paramList>#NULL</typ:paramList>" + 
					"	   <typ:paramList>#NULL</typ:paramList>" + 
					"      <typ:paramList>300000002578146</typ:paramList>" + 
					"	   <typ:paramList>Y</typ:paramList>" + 
					"	   <typ:paramList>300000002578054</typ:paramList>" + 
					"	   <typ:paramList>" + date + "</typ:paramList>" + 
					"      <typ:paramList>A</typ:paramList>" + 
					"      <typ:paramList>N</typ:paramList>" + 
					"	   <typ:paramList>N</typ:paramList>" + 
					"	   <typ:paramList>N</typ:paramList>" + 
					"	   <typ:paramList>N</typ:paramList>  " + 
					"	   <typ:paramList>300000001600035</typ:paramList>" + 
					"	   <typ:paramList>0</typ:paramList>" + 
				"      </typ:submitESSJobRequest>" + 
				"   </soapenv:Body>" + 
				"</soapenv:Envelope>";
		
		return SOAPRequest;
	}
	
}
