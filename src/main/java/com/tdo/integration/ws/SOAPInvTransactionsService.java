package com.tdo.integration.ws;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.berdico.webservices.services.file.IntegrationRestServiceFile;
import com.tdo.integration.dto.InvTransactionDTO;
import com.tdo.integration.dto.InventoryTransactionCost;
import com.tdo.integration.dto.InventoryTransactionHeader;
import com.tdo.integration.dto.InventoryTransactionLots;
import com.tdo.integration.dto.InventoryTransactionSerial;
import com.tdo.integration.dto.InventoryTransactionV1;
import com.tdo.integration.util.AppConstants;

@Service("SOAPInvTransactionsService")
public class SOAPInvTransactionsService {

	@Autowired
	IntegrationRestServiceFile IntegrationRestServiceFile;
	
	public String createInvTransaction(List<InvTransactionDTO> invTrans) {
		
		String resp = null;
		
		List<InventoryTransactionHeader> invTraHe = new ArrayList<InventoryTransactionHeader>();
		List<InventoryTransactionLots> invTraLot = new ArrayList<InventoryTransactionLots>();
		List<InventoryTransactionCost> intTraCost = new ArrayList<InventoryTransactionCost>();
		List<InventoryTransactionSerial> intTraSer = new ArrayList<InventoryTransactionSerial>();
		
		if(invTrans != null) {
			if(!invTrans.isEmpty()) {
				
				int ser = 0, lot = 0, cost = 0, line = 0;
				
				for(InvTransactionDTO c: invTrans) {
					InventoryTransactionHeader h1 = new InventoryTransactionHeader();
					
					for(int d = 0; d < c.getLines().size(); d++) {
						
						line++;
						
						h1.setORGANIZATION_NAME(c.getInventoryOrganization());
						h1.setPROCESS_FLAG(AppConstants.INV_PROCESSFLAG);
						h1.setITEM_NUMBER(c.getLines().get(d).getItemSKU());
						
						if(c.getLines().get(d).getSerialLots().contentEquals("S")) {
							ser++;
							String sv = AppConstants.INV_SER + String.valueOf(ser);
							h1.setINV_LOTSERIAL_INTERFACE_NUM(sv);
							InventoryTransactionSerial s1 = new InventoryTransactionSerial();
							s1.setINV_SERIAL_INTERFACE_NUM(sv);
							s1.setFM_SERIAL_NUMBER(c.getLines().get(d).getSerials().getSerialNumber());
							s1.setTO_SERIAL_NUMBER(c.getLines().get(d).getSerials().getSerialNumber());
							s1.setEND("END");
							
							intTraSer.add(s1);
							
						}else if(c.getLines().get(d).getSerialLots().contentEquals("L")) {
							lot++;
							String lv = AppConstants.INV_LOT + String.valueOf(lot);
							h1.setINV_LOTSERIAL_INTERFACE_NUM(lv);
							InventoryTransactionLots l1 = new InventoryTransactionLots();
							l1.setLOT_INTERFACE_NBR(lv);
							l1.setLOT_NUMBER(c.getLines().get(d).getLots().getLotNumber());
							l1.setTRANSACTION_QUANTITY(String.valueOf(c.getLines().get(d).getLots().getLotQuantity()));
							l1.setPRIMARY_QUANTITY(String.valueOf(c.getLines().get(d).getLots().getLotQuantity()));
							l1.setEND("END");
							
							invTraLot.add(l1);
						}
					
						h1.setSUBINVENTORY_CODE(c.getLines().get(d).getSubinventory());
						h1.setTRANSACTION_QUANTITY(String.valueOf(c.getLines().get(d).getQuantity()));
						h1.setTRANSACTION_UNIT_OF_MEASURE(c.getLines().get(d).getUOM());
						String date = c.getTransactionDate();//c.getTransactionDate().substring(0, 4) + "/" + c.getTransactionDate().substring(5,7) + "/" + c.getTransactionDate().substring(8, 10) + " " + c.getTransactionDate().substring(11, 18); 
						h1.setTRANSACTION_DATE(date);
						h1.setTRANSACTION_TYPE_NAME(c.getTransactionType());
						
						if(c.getTransactionType().contentEquals("Devolución POS")) {
							cost++;
							h1.setTRANSACTION_COST_IDENTIFIER(String.valueOf(cost));
							h1.setUSE_CURRENT_COST("Y");
							InventoryTransactionCost cv = new InventoryTransactionCost();
							cv.setTRANSACTION_COST_IDENTIFIER(String.valueOf(cost));
							cv.setCOST_COMPONENT_CODE(AppConstants.INV_COST_COMPONENT_CODE);
							//Colocar costo------------------------------------------------------
							cv.setCOST("100");
							cv.setEND("END");
							
							intTraCost.add(cv);
							
						}else if(c.getTransactionType().contentEquals("Venta POS")) {
							h1.setUSE_CURRENT_COST("N");
						}
						
						h1.setSOURCE_CODE(c.getVentaIdPOS());
						h1.setSOURCE_HEADER_ID(AppConstants.INV_SOURCE_HEADER_ID);
						h1.setSOURCE_LINE_ID(String.valueOf(line));
						h1.setTRANSACTION_SOURCE_NAME(c.getVentaIdPOS());
						h1.setTRANSACTION_MODE(AppConstants.INV_TRANSACTION_MODE);
						h1.setLOCK_FLAG(AppConstants.INV_LOCK_FLAG);
						h1.setTRANSACTION_REFERENCE(c.getLines().get(d).getLineId());
						h1.setDST_SEGMENT1(AppConstants.getDSTSEGMENT(c.getInventoryOrganization(), 1));
						h1.setDST_SEGMENT2(AppConstants.getDSTSEGMENT(c.getInventoryOrganization(), 2));
						h1.setDST_SEGMENT3(AppConstants.getDSTSEGMENT(c.getInventoryOrganization(), 3));
						h1.setDST_SEGMENT4(AppConstants.getDSTSEGMENT(c.getInventoryOrganization(), 4));
						h1.setDST_SEGMENT5(AppConstants.getDSTSEGMENT(c.getInventoryOrganization(), 5));
						h1.setDST_SEGMENT6(AppConstants.getDSTSEGMENT(c.getInventoryOrganization(), 6));
						h1.setDST_SEGMENT7(AppConstants.getDSTSEGMENT(c.getInventoryOrganization(), 7));
						h1.setEND("END");
						
						invTraHe.add(h1);
					}					
					
				}
			}
			
			InventoryTransactionV1 invTrans1 = new InventoryTransactionV1();
			invTrans1.setInventoryTransactionHeader(invTraHe);
			invTrans1.setInventoryTransactionLots(invTraLot);
			invTrans1.setInventoryTransactionSerial(intTraSer);
			invTrans1.setInventoryTransactionCost(intTraCost);
			
			resp = IntegrationRestServiceFile.processInventoryFile(invTrans1);
			return resp;
		}
		return resp;
		
	}
	
	private void sendInvTransaction(InvTransactionDTO sales) {
		
		
		//Convertimos a CSV dependiendo del tipo: Ventas, Inventarios o Pagos
		//Invocamos WS de creación de ventas e inventarios (pagos is aplica)
		// Verificamos el resultado de la invocación
		
	}
	
}
