<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Smartech Integraciones</title>

<script>
var myVar = setInterval(myTimer, 2000);

function myTimer() {
	window.open('http://50.21.190.40:8080/eai/mobile?', "_self");
}

function stopTimer() {
	clearInterval(myVar);
}
</script>

</head>
<body>
<button onclick="stopTimer()">Detener lectura</button><br /><br />
Registro : <br />
    <c:forEach items="${results}" var="result">
    ${result} <br />
</c:forEach>
</body>
</html>